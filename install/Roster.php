<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Roster extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'list') {
      return $this->ListUsers();
    }
    if ($us_action === 'showRoster') {
      return $this->ShowRoster();
    }
    if ($us_action === 'previousDate') {
      return $this->ShowRoster('previous');
    }
    if ($us_action === 'nextDate') {
      return $this->ShowRoster('next');
    }
    if ($us_action === 'submit') {
      return $this->SaveUser();
    }
    if ($us_action === 'remove') {
      return $this->RemoveUser();
    }
    if ($us_action === 'add-role') {
      return $this->AddRole();
    }
    if ($us_action === 'remove-role') {
      return $this->RemoveRole();
    }
    if ($us_action === 'edit-roles') {
      return $this->EditRoles();
    }
    if ($us_action === 'search') {
      return $this->Search();
    }
  }

  public function CanAdd($page) {
    // Need admin privileges to add the roster module.
    if (!$this->user->canEditSite) return false;
    // Can only have one roster module on a page.
    return !$this->AlreadyOnPage('roster', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $edit = '';
    $search = '';
    if ($this->user->canEditPage) {
      $edit = '<button class="roster-edit-roles">Edit Roles</button>';
      $search = '<div class="roster-search">Search the roster:' .
          '<div class="form-spacing">' .
            '<label for="roster-start-input">Start:</label>' .
            '<input id="roster-start-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="roster-end-input">End:</label>' .
            '<input id="roster-end-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input id="roster-export" type="checkbox">' .
            '<label for="roster-export">Download search results</label>' .
          '</div>' .
          '<button class="roster-search-submit">submit</button>' .
          '<span class="roster-search-info"></span>' .
        '</div>';
    }

    $detail = new Detail($this->user, $this->owner);
    $user_details = $detail->User();
    $first = $user_details['first'];
    $last = $user_details['last'];
    // If the user hasn't filled out their details just display their username.
    if ($first === '') {
      $first = $this->user->name;      
    }
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day contains commas, assume this is multiple weekdays and use
    // the first day listed.
    if (strpos($co_op_day, ',') !== false) {
      $co_op_day = strstr($co_op_day, ',', true);
    }
    $next_co_op = strtotime($co_op_day);
    if ($next_co_op && $next_co_op < time()) {
      if (strtotime('next ' . $co_op_day)) {
        $next_co_op = strtotime('next ' . $co_op_day);
      }
    }
    $future_dates = $this->FutureDates($first, $last);
    $hide_future_dates = $future_dates === '' ? ' hidden' : '';
    // Only show navigation buttons if the group meet on a weekly basis.
    $navigation_buttons = '';
    $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                 'Friday', 'Saturday'];
    if (in_array($co_op_day, $weekdays)) {
      $navigation_buttons .= '<button id="roster-show-previous">previous' .
        '</button><button id="roster-show-next">next</button>';
    }

    return 'Hello ' . $first . ',' .
      '<div class="roster-list' . $hide_future_dates . '">' . $future_dates .
      '</div>' .
      '<form id="roster-form">' .
        'Please add dates when you can volunteer:' .
        '<div class="form-spacing">' .
          '<label for="roster-username-input">Username:</label>' .
          '<input id="roster-username-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-firstname-input">First Name:</label>' .
          '<input id="roster-firstname-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-lastname-input">Last Name:</label>' .
          '<input id="roster-lastname-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-phone-input">Phone:</label>' .
          '<input id="roster-phone-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-date-input">Date:</label>' .
          '<input id="roster-date-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-role-input">Role:</label>' .
          '<select id="roster-role-select">' . $this->Roles(true) . '</select>'.
          $edit .
        '</div>' .
        '<div class="form-spacing" id="roster-role-description"></div>' .
        '<button class="submit">submit</button>' .
        '<button class="remove">remove</button>' .
      '</form>' .
      'Roster for <span class="roster-next-date">' . date('j M Y', $next_co_op).
        '</span>:' . $navigation_buttons .
      '<div class="roster-show-date">' .
        $this->ShowDate($next_co_op) .
      '</div>' .
      $search .
      '<div class="roster-role-dialog hidden"></div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.roster.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.roster.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS roster (' .
      'user VARCHAR(50) NOT NULL,' .
      'first VARCHAR(50),' .
      'last VARCHAR(50),' .
      'phone VARCHAR(50),' .
      'role VARCHAR(50),' .
      'timestamp INT(10) UNSIGNED NOT NULL' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Roster->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS roster_role (' .
      'system_group VARCHAR(50) NOT NULL,' .
      'role VARCHAR(50) NOT NULL,' .
      'description TEXT,' .
      'display TINYINT(1),' .
      'requires INT UNSIGNED,' .
      'PRIMARY KEY(system_group, role)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Roster->Install 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $description = ['roster-reminder' => 'The string \'true\' or \'false\' ' .
                      'to turn reminders on or off.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#roster-form","background-color","#eeeeee"',
                   '"","#roster-form","border","1px solid #aaaaaa"',
                   '"","#roster-form","border-radius","2px"',
                   '"","#roster-form","padding","5px"',
                   '"","#roster-form","margin","10px"',
                   '"","#roster-form label","width","6em"',
                   '"","#roster-form .submit","float","right"',
                   '"",".roster-list","background-color","#eeeeee"',
                   '"",".roster-list","border","1px solid #aaaaaa"',
                   '"",".roster-list","border-radius","2px"',
                   '"",".roster-list","padding","5px"',
                   '"",".roster-list","margin","10px"',
                   '"",".roster-date","padding","5px"',
                   '"",".roster-show-date","background-color","#eeeeee"',
                   '"",".roster-show-date","border","1px solid #aaaaaa"',
                   '"",".roster-show-date","border-radius","2px"',
                   '"",".roster-show-date","padding","5px"',
                   '"",".roster-show-date","margin","10px"',
                   '"",".roster-remove-role","top","0"',
                   '"",".roster-role-group","padding","0.2em"',
                   '"","#roster-description","width","390px"',
                   '"","#roster-description","height","80px"',
                   '"",".roster-edit-roles","margin-left","10px"',
                   '"","#roster-show-previous","margin-left","10px"',
                   '"","#roster-show-next","margin-left","10px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.roster.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function AllRoles() {
    $all_roles = [];
    $mysqli = connect_db();
    // First add the roster roles specified in admin user permission groups. 
    $query = 'SELECT role, description, visitor FROM roster_role INNER JOIN ' .
      'group_names ON role = name WHERE ' .
      'system_group = "' . $this->user->group . '" AND ' .
      'user = "admin" ORDER BY role';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster_role = $mysqli_result->fetch_assoc()) {
        $role = $roster_role["role"];
        if (!isset($all_roles[$role])) {
          $all_roles[$role] = ['description' => $roster_role['description'],
                               'members' => [$roster_role['visitor']]];
        }
        else {
          $all_roles[$role]['members'][] = $roster_role['visitor'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->AllRoles 1: ' . $mysqli->error);
    }
    // Then add normal roster roles. 
    $query = 'SELECT roster.role, description, user FROM roster LEFT JOIN ' .
      'roster_role ON roster.role = roster_role.role WHERE timestamp > ' .
      strtotime('-4 weeks') . ' AND system_group = "' . $this->user->group.'" '.
      'ORDER BY role, user, timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster = $mysqli_result->fetch_assoc()) {
        $role = $roster['role'];
        if (!isset($all_roles[$role])) {
          $all_roles[$role] = ['description' => $roster['description'],
                               'members' => [$roster['user']]];
        }
        else {
          // The above query doesn't provide unique usernames per role, so
          // make sure a user is not added twice here.
          if (!in_array($roster['user'], $all_roles[$role]['members'])) {
            $all_roles[$role]['members'][] = $roster['user'];
          }
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->AllRoles 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $all_roles;
  }

  public function FirstName() {
    $first = '';
    $mysqli = connect_db();
    $query = 'SELECT first FROM roster WHERE user = "' . $this->user->name.'" '.
      'AND timestamp > ' . strtotime('-24 hours') . ' AND timestamp < ' .
      strtotime('24 hours');
    if ($mysqli_result = $mysqli->query($query)) {
      if ($roster = $mysqli_result->fetch_assoc()) {
        $first = $roster['first'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->FirstName: ' . $mysqli->error);
    }
    $mysqli->close();
    return $first;
  }

  public function Description($user = '') {
    if ($user === '') {
      $user = $this->user->name;
    }
    $timestamp = 0;
    $role = '';
    $description = '';
    $mysqli = connect_db();
    $query = 'SELECT MAX(timestamp) as timestamp, role FROM roster WHERE ' .
      'timestamp > ' . strtotime('-4 weeks') . ' AND user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($roster = $mysqli_result->fetch_assoc()) {
        $role = $roster['role'];
        $timestamp = (int)$roster['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->Description 1: ' . $mysqli->error);
    }

    if ($timestamp === 0) {
      if ($this->Substitute('roster-reminder') === 'true') {
        // If the user hasn't volunteered for a role recently, also check that
        // they're not a member of a working group.
        $query = 'SELECT visitor FROM group_names INNER JOIN roster_role ON ' .
          'name = role WHERE system_group = "' . $this->user->group . '" AND ' .
          'visitor = "' . $user . '" AND user = "admin"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($mysqli_result->num_rows === 0) {
            $description = 'You haven\'t volunteered recently.<br>' .
              'Please consider putting your name on the roster soon.';
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Roster->Description 2: ' . $mysqli->error);
        }
      }
    }
    else if ($timestamp < strtotime('+1 week') && $timestamp > time()) {
      $description = 'Thanks for volunteering for ' . $role . ' this week! :-)';
    }
    else if ($timestamp < time() && $timestamp > strtotime('-1 week')) {
      $description = 'Thanks for volunteering for ' . $role . ' last week! :-)';
    }
    else if ($timestamp < strtotime('-1 week')) {
      $description = 'Thanks for volunteering for ' . $role . ' recently! :-)';
    }
    else {
      $description = 'Thanks for volunteering for ' . $role . ' soon! :-)';
    }
    $mysqli->close();
    return $description;
  }

  public function Reminders() {
    $reminders = [];
    $ignore = [];
    $recent = strtotime('-4 weeks');
    $mysqli = connect_db();
    // First look up members of working groups that can be ignored for
    // reminders, this is done first as the second query below looks up a
    // timestamp for everyone in the group, whereas this will be a subset.
    $query = 'SELECT DISTINCT visitor FROM group_names INNER JOIN ' .
      'roster_role ON name = role WHERE ' .
      'system_group = "' . $this->user->group . '" AND user = "admin"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($group_names = $mysqli_result->fetch_assoc()) {
        $ignore[] = $group_names['visitor'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->Reminders 1: ' . $mysqli->error);
    }
    // This query provides a list of everyone in the group and their most
    // recent timestamp from the roster table (which could be in the future).
    $query = 'SELECT users.user, MAX(timestamp) AS timestamp FROM users ' .
      'LEFT JOIN roster ON users.user = roster.user WHERE users.user NOT ' .
      'LIKE "buyer\_%" AND users.system_group = "' . $this->user->group . '" ' .
      'GROUP BY users.user';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster = $mysqli_result->fetch_assoc()) {
        if (!in_array($roster['user'], $ignore) &&
            (int)$roster['timestamp'] < $recent) {
          $reminders[] = $roster['user'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->Reminders 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $reminders;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddRole() {
    if (!$this->user->canEditPage) {
      return ['error' => 'You don\'t have permission to add roles.'];
    }

    // Allow html in the description.
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $purifier = new HTMLPurifier($config);
    $us_description = $purifier->purify($_POST['description']);
    $requires = (int)$_POST['requires'];
    $display = (int)$_POST['display'];

    $mysqli = connect_db();
    $role = $mysqli->escape_string(htmlspecialchars($_POST['role']));
    $description = $mysqli->escape_string($us_description);

    // When a role is not being displayed, it means the user wants to use
    // a permission group under the 'admin' account to manage members.
    // This means the current user needs to be added to the group with
    // permission to edit the group. Note that the current user could add a
    // role with the same name as an existing group and thereby get extra
    // permissions... need to be careful with group_permission and also
    // disallow some group names here that are already in use elsewhere.
    if ($display === 0) {
      if (in_array($role, ['admin', 'backup', 'private'])) {
        $mysqli->close();
        return ['error' => 'Name not allowed for working group.'];
      }

      $query = 'INSERT INTO group_names VALUES ("admin", "' . $role . '", ' .
        '"' . $this->user->name .'", 1) ON DUPLICATE KEY UPDATE edit_group = 1';
      if (!$mysqli->query($query)) {
        $this->Log('Roster->AddRole 1: ' . $mysqli->error);
      }
    }
    $query = 'INSERT INTO roster_role VALUES ("' . $this->user->group . '", ' .
      '"' . $role . '", "' . $description . '", ' . $display . ', ' .
      $requires . ') ON DUPLICATE KEY UPDATE ' .
      'description = "' . $description . '", display = ' . $display . ', ' .
      'requires = ' . $requires;
    if (!$mysqli->query($query)) {
      $this->Log('Roster->AddRole 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['content' => $this->Roles(), 'options' => $this->Roles(true)];
  }

  private function EditRoles() {
    if (!$this->user->canEditPage) {
      return ['error' => 'You don\'t have permission to edit roles.'];
    }

    $content = '<form id="roster-role-form">' .
        '<div class="form-spacing">' .
          '<label for="roster-add-role">Role:</label>' .
          '<input id="roster-add-role" type="text" maxlength="50"> ' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-requires">Number required:</label>' .
          '<input id="roster-requires" type="text" maxlength="3">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-description">Description:</label><br>' .
          '<textarea id="roster-description"></textarea>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="roster-display">Display in menu:</label>' .
          '<input id="roster-display" type="checkbox" checked="checked"> ' .
          '<button class="roster-add-role-button">submit</button>' .
        '</div>' .
        '<div class="roster-list-roles">' .
          $this->Roles() .
        '</div>' .
      '</form>';
    return ['content' => $content, 'description' => $this->RoleDescription()];
  }

  private function FutureDates($first, $last) {
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT first, last, role, timestamp FROM roster WHERE ' .
      'user = "' . $this->user->name . '" AND ' .
      'timestamp > ' . strtotime('-24 hours') . ' ORDER BY timestamp';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster = $mysqli_result->fetch_assoc()) {
        // users can change the name in the roster as it may be a group account,
        // so display the name when it doesn't match the default details.
        $content .= '<div class="roster-date">';
        if ($first !== $roster['first'] || $last !== $roster['last']) {
          $content .= $roster['first'] . ' ' . $roster['last'] . ': ';
        }
        $content .= $roster['role'] . ' on ' .
          date('j M Y', $roster['timestamp']) . '</div>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->FutureDates: ' . $mysqli->error);
    }
    $mysqli->close();
    if ($content !== '') {
      $content = 'Thanks for volunteering, here are the dates you have chosen:'.
        $content;
    }
    return $content;
  }

  private function ListUsers() {
    // content is set to an empty array here to reset in javascript.
    $result = ['content' => [], 'users' => [], 'roles' => []];

    $mysqli = connect_db();
    // Autocomplete users (ignore point of sale users).
    $query = 'SELECT user FROM users WHERE user NOT LIKE "buyer\_%" AND ' .
      'system_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $result['users'][] = $users['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->ListUsers 1: ' . $mysqli->error);
    }

    // Get user details for autocomplete.
    $detail = new Detail($this->user, $this->owner);
    $result['details'] = $detail->AllUsers();

    $query = 'SELECT role, description FROM roster_role WHERE display = 1 AND '.
      'system_group = "' . $this->user->group . '" ORDER BY role';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster_role = $mysqli_result->fetch_assoc()) {
        $role = htmlspecialchars_decode($roster_role['role']);
        $result['roles'][$role] = $roster_role['description'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->ListUsers 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function RemoveRole() {
    if (!$this->user->canEditPage) {
      return ['error' => 'You don\'t have permission to remove roles.'];
    }

    $mysqli = connect_db();
    $role = $mysqli->escape_string(htmlspecialchars($_POST['role']));
    $query = 'DELETE FROM roster_role WHERE ' .
      'system_group = "' . $this->user->group . '" AND role = "' . $role . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Roster->RemoveRole: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['content' => $this->Roles(), 'options' => $this->Roles(true)];
  }

  private function RemoveUser() {
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    $timestamp = (int)$_POST['timestamp'] / 1000;

    if ($username === '') {
      $mysqli->close();
      return ['error' => 'No username given'];
    }
    if ($timestamp === 0) {
      $mysqli->close();
      return ['error' => 'No date given'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($username)) {
      $mysqli->close();
      return ['error' => 'User not found'];
    }

    $role = $mysqli->escape_string(htmlspecialchars($_POST['role']));
    // Users can volunteer multiple times, but assume they only want to remove
    // one entry when removing them.
    $query = 'DELETE FROM roster WHERE user = "' . $username . '" AND ' .
      'role = "' . $role . '" AND timestamp = ' . $timestamp . ' LIMIT 1';
    if (!$mysqli->query($query)) {
      $this->Log('Roster->RemoveUser: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['content' => $this->ShowDate($timestamp)];
  }

  private function RoleDescription() {
    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT role, description, display, requires FROM roster_role ' .
      'WHERE system_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster_role = $mysqli_result->fetch_assoc()) {
        $result[$roster_role['role']] =
          ['description' => $roster_role['description'],
           'requires' => (int)$roster_role['requires'],
           'display' => (int)$roster_role['display']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->RoleDescription: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function Roles($options = false) {
    $text = '';
    $group_info_written = false;
    // If displaying options then only look up roles with display set.
    $display_query = '';
    if ($options) {
      $display_query = ' AND display = 1';
    }
    $mysqli = connect_db();
    $query = 'SELECT role, display FROM roster_role WHERE ' .
      'system_group = "' . $this->user->group . '"' . $display_query .
      ' ORDER BY display DESC, role';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster_role = $mysqli_result->fetch_assoc()) {
        if ($options) {
          $text .= '<option value="' . $roster_role['role'] . '">' .
            $roster_role['role'] . '</option>';
        }
        else {
          if ($roster_role['display'] === '0' && !$group_info_written) {
            $text .= '<hr><div class="roster-group-info">Below is a list of ' .
              'working groups that members can join. They are not displayed ' .
              'in the role options, instead you can add members to each group '.
              'via the Account menu -> "Preferences..." -> "Groups" tab.</div>';
            $group_info_written = true;
          }
          $text .= '<div class="roster-role-group">' .
              '<button class="roster-remove-role">remove</button> ' .
              '<span class="roster-role">' . $roster_role['role'] . '</span>' .
            '</div>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->Roles: ' . $mysqli->error);
    }
    $mysqli->close();
    return $text;
  }

  private function SaveUser() {
    $mysqli = connect_db();
    // First check if a volunteer is required for this role on this day.
    $role = $mysqli->escape_string(htmlspecialchars($_POST['role']));
    $timestamp = (int)$_POST['timestamp'] / 1000;
    $username = $mysqli->escape_string($_POST['username']);
    $requires = 0;
    $query = 'SELECT requires FROM roster_role WHERE role = "' . $role . '" ' .
      'AND system_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($roster_role = $mysqli_result->fetch_assoc()) {
        $requires = (int)$roster_role['requires'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->SaveUser 1: ' . $mysqli->error);
    }
    $query = 'SELECT roster.user FROM roster LEFT JOIN users ON ' .
      'roster.user = users.user WHERE role = "' . $role . '" AND ' .
      'users.system_group = "' . $this->user->group . '" AND ' .
      'timestamp >= ' . $timestamp . ' AND ' .
      'timestamp < ' . ($timestamp + 24 * 3600);
    if ($mysqli_result = $mysqli->query($query)) {
      $requires -= $mysqli_result->num_rows;
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->SaveUser 2' . $mysqli->error);
    }
    if ($requires <= 0) {
      $mysqli->close();
      return ['error' => 'No positions available on this date.'];
    }

    $first = $mysqli->escape_string(htmlspecialchars($_POST['first']));
    $last = $mysqli->escape_string(htmlspecialchars($_POST['last']));
    $phone = $mysqli->escape_string(htmlspecialchars($_POST['phone']));

    if ($username === '') {
      $mysqli->close();
      return ['error' => 'No username given'];
    }
    if ($timestamp === 0) {
      $mysqli->close();
      return ['error' => 'No date given'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($username)) {
      $mysqli->close();
      return ['error' => 'User not found'];
    }

    $query = 'INSERT INTO roster VALUES ("' . $username . '", ' .
      '"' . $first . '", "' . $last . '", "' . $phone . '", "' . $role . '", ' .
      $timestamp . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Roster->SaveUser 3: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['content' => $this->ShowDate($timestamp)];
  }

  private function Search() {
    $mysqli = connect_db();
    $start = (int)$_POST['start'] / 1000;
    $end = (int)$_POST['end'] / 1000;
    $export = (bool)$_POST['exportData'];

    $data = [];
    $timestamp_query = '';
    if ($start !== 0) {
      $timestamp_query .= ' AND timestamp >= ' . $start;
    }
    if ($end !== 0) {
      $timestamp_query .= ' AND timestamp <= ' . $end;
    }
    $query = 'SELECT roster.user, first, last, phone, role, timestamp FROM ' .
      'roster LEFT JOIN users ON roster.user = users.user WHERE ' .
      'users.system_group = "' . $this->user->group . '"' . $timestamp_query .
      ' ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster = $mysqli_result->fetch_assoc()) {
        $data['content'][] = ['date' => $roster['timestamp'] * 1000,
                              'username' => $roster['user'],
                              'first' => $roster['first'],
                              'last' => $roster['last'],
                              'phone' => $roster['phone'],
                              'role' => $roster['role']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->Search: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($export && count($data['content']) > 0) {
      $date = date('Y-m-d');
      $filename = 'roster-' . $date . '.csv';
      $this->CreateCSV($filename, $data['content']);
      $data['filename'] = $filename;
    }
    return $data;
  }

  private function ShowDate($timestamp) {
    if (!$timestamp) return 'No date set.';

    $mysqli = connect_db();
    $role_list = [];
    $query = 'SELECT role, requires FROM roster_role where display = 1 AND ' .
      'system_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster_role = $mysqli_result->fetch_assoc()) {
        $role_list[$roster_role['role']] = (int)$roster_role['requires'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->ShowDate 1: ' . $mysqli->error);
    }
    $volunteers = [];
    $query = 'SELECT roster.user, first, last, phone, role FROM roster ' .
      'LEFT JOIN users ON roster.user = users.user WHERE ' .
      'users.system_group = "' . $this->user->group . '" AND ' .
      'timestamp >= ' . $timestamp . ' AND ' .
      'timestamp < ' . ($timestamp + 24 * 3600);
    if ($mysqli_result = $mysqli->query($query)) {
      while ($roster = $mysqli_result->fetch_assoc()) {
        $role = $roster['role'];
        if (!isset($volunteers[$role])) {
          $volunteers[$role] = [];
        }
        $volunteers[$role][] = ['user' => $roster['user'],
                                'first' => $roster['first'],
                                'last' => $roster['last'],
                                'phone' => $roster['phone']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Roster->ShowDate 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $content = '';
    foreach ($role_list as $role => $requires) {
      if ($requires === 0) continue;

      $count = 0;
      $content .= '<div class="roster-role-name">' . $role . '</div>';
      if (isset($volunteers[$role])) {
        $count = count($volunteers[$role]);
        for ($i = 0; $i < $count; $i++) {
          $fullname = $volunteers[$role][$i]['first'];
          if ($fullname !== '') {
            $fullname .= ' ' . $volunteers[$role][$i]['last'];
          }
          if ($fullname === '') {
            $fullname = $volunteers[$role][$i]['user'];
          }
          $phone = $volunteers[$role][$i]['phone'];
          if ($phone !== '') {
            $phone = '(' . $phone . ')';
          }
          $content .= '<div class="roster-volunteer">' . $fullname . ' ' .
            $phone . '</div>';
        }
      }
      $vacancy = $requires - $count;
      if ($vacancy === 0) {
        $content .= '<div class="roster-volunteer">' .
          '<i>All positions for ' . $role . ' are full.</i></div>';
      }
      else if ($vacancy === 1) {
        $content .= '<div class="roster-volunteer">' .
          'We need <b>1</b> more volunteer for ' . $role . '</div>';
      }
      else if ($vacancy > 1) {
        $content .= '<div class="roster-volunteer">' .
            'We need <b>' . $vacancy . '</b> more volunteers for ' . $role .
          '</div>';
      }
    }
    if ($content === '') {
      $content = '<i>No roster available.</i>';
    }
    return $content;
  }

  private function ShowRoster($navigate = '') {
    $mysqli = connect_db();
    $timestamp = $mysqli->escape_string($_POST['timestamp']);
    $mysqli->close();

    if ($navigate === 'previous' || $navigate === 'next') {
      $timestamp = strtotime($timestamp);
    }
    else {
      // If previous or next were not provided, the timestamp was created in
      // javascript, therefore in milliseconds.
      $timestamp /= 1000;
    }
    
    if (!$timestamp) {
      return ['error' => 'No date given'];
    }

    if ($navigate === 'previous') {
      $timestamp -= 7 * 86400;
    }
    else if ($navigate === 'next') {
      $timestamp += 7 * 86400;
    }

    // TODO: This date format is used by jqueryui's datepicker, which should
    //       be set via a config option.
    return ['date' => date('j M Y', $timestamp),
            'content' => $this->ShowDate($timestamp)];
  }

}
