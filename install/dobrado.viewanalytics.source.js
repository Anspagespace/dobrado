/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.viewanalytics) {
  dobrado.viewanalytics = {};
}
(function() {

  'use strict';

  var analytics = {};
  var totalGraphId = '';
  var refererGrid = null;
  var refererGridId = '';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.viewanalytics').length === 0) {
      return;
    }

    $('#viewanalytics-total-start').datepicker({
      dateFormat: dobrado.dateFormat }).val('');
    $('#viewanalytics-total-end').datepicker({
      dateFormat: dobrado.dateFormat }).val('');
    $('#viewanalytics-total-button').button().click(view);

    if ($('.graph').length === 1) {
      var graphId = '#' + $('.graph').attr('id');
      $(graphId + ' .graph-area').each(function(index) {
        if (index === 0) {
          totalGraphId = $(this).attr('id');
          $('#' + totalGraphId).parent().hide();
        }
      });
    }
    if ($('.grid').length === 1) {
      refererGridId = '#' + $('.grid').attr('id');
      var columns = [{ id : "user", name: "User", field: "user",
                       width: 100, sortable: true },
                     { id : "page", name: "Page", field: "page",
                       width: 150, sortable: true },
                     { id : "referer", name: "Referer", field: "referer",
                       width: 400, sortable: true,
                       formatter: refererFormatter },
                     { id : "counter", name: "#", field: "counter",
                       width: 50, sortable: true }];
      var options = { autoHeight: true, forceFitColumns: true };
      refererGrid = dobrado.grid.instance(refererGridId, [], columns, options);
      refererGrid.onSort.subscribe(function (e, args) {
        analytics.referers.sort(function(row1, row2) {
          var field = args.sortCol.field;
          var sign = args.sortAsc ? 1 : -1;
          var value1 = row1[field];
          var value2 = row2[field];
          if (value1 === value2) {
            return 0;
          }
          if (value1 > value2) {
            return sign;
          }
          else {
            return sign * -1;
          }
        });
        refererGrid.invalidate();
      });
    }
    // Need to wait for the graph module to configure itself.
    setTimeout(view, 500);
  });

  function refererFormatter(row, cell, value, columnDef, dataContext) {
    if (value === '') return '';
    return '<a href="' + value + '">' + value + '</a>';
  }

  function view() {
    var start = parseInt($.datepicker.formatDate('@',
      $('#viewanalytics-total-start').datepicker('getDate')), 10);
    var end = parseInt($.datepicker.formatDate('@',
      $('#viewanalytics-total-end').datepicker('getDate')), 10);

    dobrado.log('Loading data.', 'info');
    $.post('/php/request.php', { request: 'viewanalytics',
                                 start: start,
                                 end: end,
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'viewanalytics')) {
          return;
        }
        analytics = JSON.parse(response);
        if (totalGraphId) {
          $('#' + totalGraphId).html('').parent().hide();
          $('#' + totalGraphId).parent().appendTo('.viewanalytics-total-graph');
          if (analytics.data) {
            $('#' + totalGraphId).parent().show();
            dobrado.graph.loadData(totalGraphId, analytics.data,
                                   analytics.series);
          }
        }
        if (refererGrid) {
          refererGrid.setData(analytics.referers);
          refererGrid.updateRowCount();
          refererGrid.render();
        }
      });
    return false;
  }

}());
