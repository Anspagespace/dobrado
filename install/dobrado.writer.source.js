/*global dobrado: true, CKEDITOR: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.writer) {
  dobrado.writer = {};
}
(function() {

  'use strict';

  // Set to zero to copy int type used by extended editor to assign values.
  var sendToTwitter = 0;
  var webactionType = '';
  var webactionUrl = '';
  var defaultText = 'Write a new post...';

  $(function() {
    // The writer module can be used to display the feed title, but javascript
    // is only required when #writer-content is shown on the page for input.
    if ($('#writer-content').length === 0) {
      return;
    }

    $('#writer-content').click(function() {
      // Extended editor isn't modal so don't allow editor changes when open.
      if (!$('.extended').dialog('isOpen')) {
        dobrado.writer.showEditor();
      }
    });
    $('.writer-post').button({ disabled: true }).click(function() {
      dobrado.log('Creating post...', 'info');
      dobrado.createModule('writer', 'post', 'post');
    });
    $('#writer-author').val(dobrado.readCookie('user'));
    $('#writer-title').val('');
    $('#writer-tag-input').val('');
    $('#writer-tags').checkboxradio({ icon: false }).click(toggleTags);
    $('.writer .menu').menu();
    $('#writer-photo-browse').button().click(function() {
      dobrado.createModule('browser', 'browser', 'writer');
    });
    // Writer module has a button to toggle reader module's settings, so show
    // that when on the page as it can toggle writer designate actions too.
    $('.writer-edit-settings').button({
      icon: 'ui-icon-signal-diag', showLabel: false
    }).click(function() {
      if ($('.reader-settings').length === 0) {
        $('.writer .designate').toggle();
      }
      else {
        $('.reader-settings').toggle();
      }
    });
    $('#writer-designate-action').val('post');
    $('#writer-designate-action').selectmenu({ change: showDesignate });
    $('.writer .submit-designate').button().click(designate);
    $('.writer .submit-feed-title').button().click(feedTitle);
    $('.writer-remove-title').button({
      icon: 'ui-icon-closethick', showLabel: false
    }).click(removeTitle);
    $('.writer-remove-author').button({
      icon: 'ui-icon-closethick', showLabel: false
    }).click(removeAuthor);
    $('.writer-remove-twitter').button({
      icon: 'ui-icon-closethick', showLabel: false
    }).click(removeTwitter);
    $('.writer-remove-photo').button({
      icon: 'ui-icon-closethick', showLabel: false
    }).click(removePhoto);
    $('#writer-remove-action').button({
      icon: 'ui-icon-closethick', showLabel: false
    }).click(removeActionExplicit);
    if (dobrado.reader) {
      $('#writer-follow').button().click(dobrado.reader.addFeed);
    }
    // If there are currently no posts on the page, add a post-comment wrapper
    // div under the writer module so that new posts are placed correctly.
    if ($('.post-comment').length === 0) {
      $('<div></div>').addClass('post-comment').appendTo('.writer');
    }
    webactionType = $('#writer-action-type').html();
    if (webactionType === 'like' || webactionType === 'reply') {
      $('#writer-content').val('');
    }
    webactionUrl = $('#writer-action-url').attr('href');
    if (webactionType === 'share') {
      shareFullText();
    }
    $('.writer-options-add').button({
      icon: 'ui-icon-plus', showLabel: false }).click(add);
    $('.writer-menu-wrapper .menu li').each(function() {
      $(this).click(showOptions);
    });
    hideOptions();
    dobrado.writer.showEditor();
    $('.writer-options').show();
  });

  function hideMenu() {
    $('.writer-menu-wrapper').hide();
  }

  function add() {
    dobrado.hideOtherMenu('writer-menu-wrapper');
    dobrado.toggleMenu('writer-menu-wrapper');
    $('body').one('click', hideMenu);
    return false;
  }

  function showDesignate(event, ui) {
    dobrado.log('Loading page for displaying ' + ui.item.value + '.', 'info');
    $.post('/php/request.php',
           { request: 'writer', mode: 'showDesignate', action: ui.item.value,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'writer showDesignate')) {
          return;
        }
        var designate = JSON.parse(response);
        $('#writer-designate-input').val(designate.page);
      });
    return false;
  }

  function designate() {
    var action = $('#writer-designate-action').val();
    dobrado.log('Updating page for displaying ' + action + '.', 'info');
    $.post('/php/request.php',
           { request: 'writer', mode: 'designate', action: action,
             designate: $('#writer-designate-input').val(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'writer designate')) {
          return;
        }
      });
    return false;
  }

  function feedTitle() {
    dobrado.log('Updating feed title.', 'info');
    $.post('/php/request.php',
           { request: 'writer', mode: 'feedTitle',
             title: $('#writer-feed-title-input').val(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'writer feedTitle')) {
          return;
        }
      });
    return false;
  }

  function showOptions(event) {
    event.preventDefault();
    var className = $(this).attr('id');
    $('.' + className).show();
    if (className === 'writer-options-twitter') {
      sendToTwitter = 1;
    }
    else if (className === 'writer-options-photo') {
      $('#writer-photo-browse').click();
    }
  }

  function hideOptions() {
    var writerOptions = ['title', 'photo', 'author', 'twitter'];
    $(writerOptions).each(function(i, item) {
      var value = item === 'author' ? dobrado.readCookie('user') : '';
      $('#writer-' + item).val(value);
      $('.writer-options-' + item).hide();
    });
  }

  function toggleTags() {
    $('#writer-tag-input').toggle();
  }

  function removeTitle() {
    $('#writer-title').val('');
    $('.writer-options-title').hide();
  }

  function removePhoto() {
    $('#writer-photo-selected').html('');
    $('.writer-options-photo').hide();
  }

  function removeAuthor() {
    // Reset the author to the current username.
    $('#writer-author').val(dobrado.readCookie('user'));
    $('.writer-options-author').hide();
  }

  function removeTwitter() {
    sendToTwitter = 0;
    $('.writer-options-twitter').hide();
  }

  function removeActionExplicit() {
    dobrado.log('Removing action...', 'info');
    dobrado.writer.removeAction(true);
  }

  function quoteContent() {
    // When the user clicks in the editor, the content gets quoted and the
    // webactionType is unset, as this is now a normal post. The original url
    // needs to be added to the body of the post here too.
    webactionType = '';
    $('#writer-author').val(dobrado.readCookie('user'));
    var data = dobrado.editor.getData();
    dobrado.editor.setData('<br><br><cite class="h-cite u-quotation-of">' +
                           data + '<br><a class="u-url" href="' +
                           webactionUrl + '">' + webactionUrl + '</a></cite>');
  }

  function shareFullText() {
    dobrado.log('Loading content...', 'info');
    $.post('/php/request.php',
           { request: 'writer', mode: 'shareFullText', share: webactionUrl,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'writer shareFullText')) {
          return;
        }
        var share = JSON.parse(response);
        if (dobrado.editor) {
          dobrado.editor.setData(share.content);
          dobrado.editor.once('focus', quoteContent);
        }
        else {
          $('#writer-content').val(share.content);
        }
        $('#writer-author').val(share.author);
        if (share.title !== '') {
          $('#writer-title').val(share.title);
          $('.writer-options-title').show();
        }
        if (share.category !== '') {
          $('#writer-tag-input').val(share.category);
          if (!$('#writer-tags').is(':checked')) {
            $('#writer-tags').click();
          }
        }
      });
  }

  dobrado.writer.action = function(action, url, sendTo) {
    webactionType = action;
    webactionUrl = url;
    if (sendTo === 'twitter') {
      sendToTwitter = 1;
    }
    $.post('/php/request.php',
           { request: 'writer', mode: 'showWebaction', action: action,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'writer action')) {
          return;
        }
      });
  };

  dobrado.writer.removeAction = function(cancelled) {
    $.post('/php/request.php',
           { request: 'writer', mode: 'removeWebaction',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'writer removeAction')) {
          return;
        }
        var writer = JSON.parse(response);
        if (location.href !== writer.url) {
          history.pushState({}, '', writer.url);
        }
        if (dobrado.editor) {
          dobrado.editor.setData('');
        }
        else {
          $('#writer-content').val('');
        }
        $('#writer-tag-input').val('');
        if ($('#writer-tags').is(':checked')) {
          $('#writer-tags').click();
        }
        $('#writer-webaction-info').html('');
        $('#writer-author').val(dobrado.readCookie('user'));
        // The reader module calls dobrado.writer.action which calls this
        // function, so tell the reader to update the action when done.
        if (dobrado.reader && !cancelled) {
          dobrado.reader.updateAction(webactionUrl);
        }
        webactionType = '';
        webactionUrl = '';
        removeTitle();
        removeTwitter();
        removePhoto();
      });
  };

  dobrado.writer.select = function(filename) {
    $('#writer-photo-selected').append('<img src="' + filename + '">');
  };

  dobrado.writer.showEditor = function() {

    function clearContent() {
      if (dobrado.editor.getData() === defaultText) {
        dobrado.editor.setData('');
      }
    }

    if (dobrado.editMode) {
      dobrado.closeEditor();
    }
    $('#writer-content').parents('div').each(function() {
      if (/^dobrado-/.test($(this).attr('id'))) {
        dobrado.current = '#' + $(this).attr('id');
        return false;
      }
    });
    $(dobrado.current).data('label', 'writer');
    var editorCssProperties =
      ['background', 'background-attachment', 'background-color',
       'background-image', 'background-position', 'background-repeat',
       'background-size', 'color', 'direction', 'empty-cells', 'font',
       'font-family', 'font-size', 'font-style', 'font-variant', 'font-weight',
       'letter-spacing', 'line-height', 'list-style', 'list-style-image',
       'list-style-position', 'list-style-type', 'opacity', 'outline',
       'outline-color', 'outline-style', 'outline-width', 'size', 'text-align',
       'text-decoration', 'text-indent', 'text-transform', 'white-space',
       'word-spacing'];

    var css = '.photo-hidden img { display: none; } .cke_editable { ';
    var current = $(dobrado.current);
    for (var i = 0; i < editorCssProperties.length; i++) {
      var property = editorCssProperties[i];
      var value = current.css(property);
      if (value) {
        css += property + ': ' + value + '; ';
      }
    }
    css += 'margin: 10px; }';
    CKEDITOR.addCss(css);
    // Force the textarea to refresh (the browser leaves old content displayed)
    var content = $('#writer-content').text();
    $('#writer-content').val(content);
    dobrado.editor = CKEDITOR.replace('writer-content',
      { allowedContent: true,
        autoGrow_minHeight: $('#writer-content').height(),
        autoGrow_onStartup: true,
        disableNativeSpellChecker: false,
        enterMode: CKEDITOR.ENTER_BR,
        extraPlugins: 'extended',
        filebrowserBrowseUrl: '/php/browse.php',
        on: { focus: clearContent },
        removePlugins:
          'elementspath,tableselection,tabletools,contextmenu,liststyle',
        resize_enabled: false,
        toolbar: [[ 'Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'Link',
                    'Unlink', '-', 'EmojiPanel', 'Image', 'Extended' ]]
      });
    $('.writer-post').button('option', 'disabled', false);
  };

  dobrado.writer.newModuleCallback = function(id, context) {
    var data = dobrado.editor ?
      dobrado.editor.getData() : $('#writer-content').val();
    var enclosure = [];
    $('#writer-photo-selected > img').each(function() {
      enclosure.push($(this).attr('src'));
    });
    var content = { data: data, dataOnly: false,
                    title: $('#writer-title').val(),
                    author: $('#writer-author').val(),
                    category: $('#writer-tag-input').val(),
                    enclosure: enclosure, twitter: sendToTwitter,
                    webactionType: webactionType, webactionUrl: webactionUrl };
    if (context === 'post') {
      $.post('/php/content.php',
             { id: id, label: 'post', content: JSON.stringify(content),
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'writer callback')) {
            return;
          }
          var formatted = JSON.parse(response);
          // If there's no content initially then the post is hidden,
          // so show it now if content is returned.
          if (formatted.html) {
            $(id).html(formatted.html);
            $(id).show();
          }
          if (dobrado.editMode) {
            var element = $(id + ' .dobrado-editable').get(0);
            if (element) {
              dobrado.inlineEditor(element);
            }
          }
          if (dobrado.editor) {
            dobrado.editor.setData(defaultText);
          }
          else {
            $('#writer-content').val(defaultText);
          }
          hideOptions();
          dobrado.writer.removeAction(false);
          $.post('/php/request.php',
                 { request: 'post', action: 'updated',
                   url: location.href, token: dobrado.token },
            function(response) {
              if (dobrado.checkResponseError(response, 'writer post update')) {
                return;
              }
              var post = JSON.parse(response);
              if (post.status) {
                // Need to wait for the 'Creating post' notification to be
                // removed before displaying the post status notification.
                setTimeout(function() {
                  dobrado.log(post.status, 'info');
                  setTimeout(function() {
                    $('.control .info').hide();
                  }, 10000);
                }, 2000);
              }
            });
        });
    }
    else if (context === 'extended') {
      // Need to set dobrado.current because the extended editor uses core's
      // closeEditor function. (We've reached this point after clicking the
      // extended editor button in the minimal ckeditor in the writer module,
      // which created a new post module, and are now opening the extended
      // editor on behalf of the new post module).
      dobrado.current = id;
      // Also since this is only opening the extended editor, don't use the
      // notification system to say that a new post has been created (yet).
      $.post('/php/request.php',
             { id: id, label: 'post', content: JSON.stringify(content),
               request: 'extended', mode: 'box', notify: false,
               url: location.href, token: dobrado.token },
             dobrado.extended.editor);
    }
  };

  dobrado.writer.reset = function() {
    if ($('.writer').length !== 0) {
      $('#writer-content').val(defaultText);
      $('.writer-post').button('option', 'disabled', true);
      hideOptions();
      dobrado.writer.removeAction(true);
    }
  };

}());
