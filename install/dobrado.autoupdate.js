// @source: /js/source/dobrado.autoupdate.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.autoupdate){dobrado.autoupdate={};}
(function(){'use strict';var details={};var dependencies=[];var grid=null;var updates=[];var installing=false;$(function(){if($('.autoupdate').length===0){return;}
$('#autoupdate-tabs').tabs();$('#autoupdate-build-module').click(buildModule);$('#autoupdate-build-core').click(buildCore);$('#autoupdate-build-submit').button().click(build);$('#autoupdate-install-tab .module-install').button().click(install);$('#autoupdate-remove').button().click(remove);dobrado.log('Loading updates...','info');$.post('/php/request.php',{request:'autoupdate',action:'list',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'autoupdate list')){return;}
updates=JSON.parse(response);if($('.grid').length!==0){gridSetup();}
else{alert('Please add a grid module to this page.');}
let names=[];$.each(updates,function(index,module){if(module.label==='core'){return true;}
if($.inArray(module.label,names)===-1){names.push(module.label);details[module.label]={display:module.display==='1',title:module.title};}});$('#autoupdate-build-label').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:names,select:displayDetails});$('#autoupdate-build-label').change(displayDetails);});});function build(){var module=$('#autoupdate-build-label').val();var core=$('#autoupdate-build-files').val();if(module===''&&core===''){alert('Please provide a module name or a list of core files.');return false;}
if(module!==''&&core!==''){alert('Module release and core file updates should be done separately.');return false;}
dobrado.log('Building update...','info');$.post('/php/request.php',{request:'autoupdate',action:'build',module:module,core:core,css:$('#autoupdate-build-css').val(),title:$('#autoupdate-build-title').val(),description:$('#autoupdate-build-description').val(),display:$('#autoupdate-build-display:checked').length,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'autoupdate build')){return;}
var autoupdate=JSON.parse(response);$('#autoupdate-build-result').html(autoupdate.result);$('#autoupdate-build-title').val('');$('#autoupdate-build-description').val('');$('#autoupdate-build-display').prop('checked',false);setTimeout(function(){$('#autoupdate-build-result').html('');},5000);});return false;}
function buildModule(){$('#autoupdate-build-files').val('');$('#autoupdate-build-module-wrapper').show();$('#autoupdate-build-core-wrapper').hide();}
function buildCore(){$('#autoupdate-build-label').val('');$('#autoupdate-build-css').val('');$('#autoupdate-build-title').val('');$('#autoupdate-build-module-wrapper').hide();$('#autoupdate-build-core-wrapper').show();}
function displayDetails(event,ui){var label=$('#autoupdate-build-label').val();if(ui){label=ui.item.value;}
if(details&&details[label]){let module=details[label];$('#autoupdate-build-display').prop('checked',module.display);$('#autoupdate-build-title').val(module.title);}}
function gridSetup(){var gridId='#'+$('.grid').attr('id');var columns=[{id:'timestamp',name:'Date',field:'timestamp',width:120,sortable:true,formatter:Slick.Formatters.Timestamp},{id:'title',name:'Title',field:'title',width:190,sortable:true},{id:'description',name:'Description',field:'description',width:190,sortable:true},{id:'version',name:'Version',field:'version',width:100,sortable:true},{id:'installed',name:'Installed',field:'installed',width:100,sortable:true}];var options={autoHeight:true,forceFitColumns:true};$('#autoupdate-log-tab').show();$(gridId).appendTo($('#autoupdate-log-tab'));grid=dobrado.grid.instance(gridId,updates,columns,options);grid.setSelectionModel(new Slick.RowSelectionModel());$('#autoupdate-log-tab').hide();}
function install(){var info=null;var label='';if(installing){return false;}
installing=true;if(dependencies.length===0){info=$(this).parents('.module-info');label=info.find('.module-label-name').text();}
else{label=dependencies[0];dependencies.splice(0,1);}
dobrado.log('Installing module...','info');$.post('/php/request.php',{request:'autoupdate',action:'install',label:label,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'autoupdate install')){installing=false;return;}
$.post('/php/request.php',{request:'autoupdate',action:'finishInstall',label:label,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'finishInstall')){installing=false;return;}
var check=JSON.parse(response);if(info){info.children('.module-install-info').html('<b>This module is '+'now installed.</b>');}
if(dependencies.length===0&&check.dependencies){dependencies=check.dependencies;}
else{$.each(check.dependencies,function(index,item){if($.inArray(item,dependencies)===-1){dependencies.push(item);}});}
installing=false;if(dependencies.length!==0){let message='The '+label+' module also requires the '+
dependencies[0]+' module, install it now?';if(confirm(message)){install();}
else{dependencies=[];}}});});}
function remove(){var selected=grid?grid.getSelectedRows():[];if(selected.length!==1){alert('Please select a row from the grid.');return;}
let removeUpdate=updates[selected[0]];dobrado.log('Removing update.','info');$.post('/php/request.php',{request:'autoupdate',action:'remove',label:removeUpdate.label,version:removeUpdate.version,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'autoupdate remove')){return;}
updates=JSON.parse(response);grid.setData(updates);grid.updateRowCount();grid.render();grid.setSelectedRows([]);});}}());