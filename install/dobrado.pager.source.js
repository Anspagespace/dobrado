/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.pager) {
  dobrado.pager = {};
}
(function() {

  'use strict';

  // Group private variables and module member variables here.

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.pager').length === 0) {
      return;
    }

    $('.pager .edit-limit').button({ icon: 'ui-icon-pencil',
      showLabel: false }).click(function() { $('.pager-limit').toggle(); });
    $('.pager .save').button().click(save);
  });

  function save() {
    $.post('/php/request.php', { request: 'pager',
                                 limit: $('#pager-limit-input').val(),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'pager save')) {
          return;
        }
        location.reload();
      });
    return false;
  }

})();
