<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Comment extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage && !$this->Published($this->user->page)) {
      return ['error' => 'You don\'t have permission to view new comments.'];
    }

    $id = 0;
    $mysqli = connect_db();
    $query = 'SELECT box_id FROM comment WHERE user = "' . $this->owner . '" ' .
      'AND permalink LIKE "' . $this->user->page . '#%" ORDER BY timestamp ' .
      'DESC LIMIT 1';
    if ($result = $mysqli->query($query)) {
      if ($comment = $result->fetch_assoc()) {
        $id = (int)$comment['box_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Comment->Callback: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['id' => 'dobrado-' . $id, 'content' => $this->Content($id)];
  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT author, author_photo, author_url, url, description, ' .
      'timestamp FROM comment WHERE user = "' . $this->owner . '" AND ' .
      'box_id = ' . $id;
    if ($result = $mysqli->query($query)) {
      if ($comment = $result->fetch_assoc()) {
        $author = '';
        $author_name = $comment['author'];
        $author_photo = $comment['author_photo'];
        $author_url = $comment['author_url'];
        if ($author_url !== '') {
          $author = '<span class="h-card u-author">';
          if ($author_photo !== '') {
            $author .= '<a href="' . $author_url . '">' .
              $author_photo . '</a>';
          }
          if ($author_name !== '') {
            $author .= '<a class="p-name u-url" href="' . $author_url . '">' .
              $author_name . '</a>';
          }
          $author .= '</span>';
        }
        else {
          $author = $author_photo . $author_name;
        }
        $url = '#' . $id;
        $type = 'h-entry';
        if ($comment['url'] !== '') {
          $url = $comment['url'];
          $type = 'h-cite';
        }
        $timestamp = (int)$comment['timestamp'];
        $formatted_date = date('j F Y, g:i a', $timestamp);
        $atom_date = date(DATE_ATOM, $timestamp);
        $content = '<div class="comment-wrapper u-comment ' . $type . '">' .
          '<div class="comment-content e-content p-name">' .
            $comment['description'] . '</div>' .
          '<div class="comment-name">' . $author .
            '<time class="comment-time dt-published" ' .
              'datetime="' . $atom_date . '">' .
              ' on <a class="u-url" name="' . $id . '" href="' . $url . '">' .
              $formatted_date . '</a>' .
            '</time>' .
          '</div></div>';
      }
      $result->close();
    }
    else {
      $this->Log('Comment->Content: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $this->Notify($id, 'comment', 'comment', $new_page,
                  $this->Published($new_page));
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'Modified' && $count === 2) {
        $id = $p[0];
        $us_content = $p[1];
        return $this->Modified($id, $us_content);
      }
      return;
    }
    if ($fn === 'MatchUrl') {
      return $this->MatchUrl($p);
    }
  }

  public function Group() {
    return 'post-comment';
  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS comment (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'url VARCHAR(200),' .
      'title VARCHAR(180),' .
      'description TEXT,' .
      'author VARCHAR(50),' .
      'author_photo VARCHAR(200),' .
      'author_url VARCHAR(200),' .
      'category VARCHAR(200),' .
      'enclosure TEXT,' .
      'permalink VARCHAR(200),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Comment->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    // Add style rules to the site_style table.
    $site_style = ['"",".comment-name .thumb","width","20px"',
                   '"",".comment-name .thumb","border-radius","2px"',
                   '"",".comment.highlight > .comment-wrapper",' .
                     '"border-color","orange"',
                   '"",".comment-wrapper","transition","border-color 1s"',
                   '"",".comment-wrapper","border","2px solid #888888"',
                   '"",".comment-wrapper","border-radius","3px"',
                   '"",".comment-wrapper","margin","5px"',
                   '"",".comment-wrapper","padding","10px"'];
    $this->AddSiteStyle($site_style);
    $this->AddTemplate(['"comment-notifications", "", "true"']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {
    if (isset($id)) {
      $this->Notify($id, 'comment', 'comment', $this->user->page, $update);
    }
    else {
      // When $id is not set the page has been made private. Remove users from
      // any comment notifications groups if they don't have access permission.
      $this->RemoveNotification('comment', $this->user->page);
    }
  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $this->RemoveNotify($id);
      $query = 'DELETE FROM comment WHERE user = "' . $this->owner . '" AND ' .
        'box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Comment->Remove 1: ' . $mysqli->error);
      }
    }
    else {
      // If a specific id is not set remove all comment data for the user.
      $query = 'DELETE FROM comment WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Comment->Remove 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    $mysqli = connect_db();
    $url = '';
    $description = '';
    $permalink = '';
    // Note that the caller passes all data in $us_content through HTMLPurifier
    // so escaping html is not required here.
    $us_author = isset($us_content['author']) ? $us_content['author'] : '';
    $us_author_photo = isset($us_content['author_photo']) ?
      $us_content['author_photo'] : '';
    $us_author_url = isset($us_content['author_url']) ?
      $us_content['author_url'] : '';
    if (isset($us_content['url'])) {
      $url = $mysqli->escape_string($us_content['url']);
      if ($url !== '' && !preg_match('/^https?:\/\//i', $url)) {
        $url = 'http://' . $url;
      }
    }
    // When a user is logged in, they are not asked to fill out their details.
    if ($us_author === '' && $this->user->loggedIn) {
      $us_author = $this->user->name;
      $detail = new Module($this->user, $this->owner, 'detail');
      if ($detail->IsInstalled()) {
        $user_detail = $detail->Factory('User');
        if ($user_detail['first'] !== '') {
          $us_author = $user_detail['first'];
        }
        if ($user_detail['last'] !== '') {
          if ($us_author !== '') $us_author .= ' ';
          $us_author .= $user_detail['last'];
        }
        $us_author_photo = $user_detail['thumbnail'];
      }
      $us_author_url = $this->user->name === 'admin' ?
        '/' : '/' . $this->user->name;
    }
    $author = $mysqli->escape_string($us_author);
    $author_photo = $mysqli->escape_string($us_author_photo);
    $author_url = $mysqli->escape_string($us_author_url);

    if (isset($us_content['description'])) {
      $description = $mysqli->escape_string($us_content['description']);
    }
    if (isset($us_content['permalink'])) {
      $permalink = $mysqli->escape_string($us_content['permalink']);
    }
    $query = 'INSERT INTO comment VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $url . '", "", "' . $description . '", "' . $author . '", '.
      '"' . $author_photo . '", "' . $author_url . '", "", "", ' .
      '"' . $permalink . '", ' . time() . ') ON DUPLICATE KEY UPDATE ' .
      'author = "' . $author . '", author_photo = "' . $author_photo . '", ' .
      'author_url = "' . $author_url . '", url = "' . $url . '", ' .
      'description = "' . $description . '", permalink = "' . $permalink . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Comment->SetContent 1: ' . $mysqli->error);
    }

    $page = $this->user->page;
    $published = $this->Published($page);
    $this->Notify($id, 'comment', 'comment', $page, $published);
    if ($this->Substitute('comment-notifications') === 'true') {
      $category = isset($us_content['category']) ? $us_content['category'] :
        'comment';
      // Also create individual notifications for users that request them.
      // Note that the page name is also passed in as the description here.
      $this->Notification('comment', [$us_author, $us_author_photo,
                          $us_author_url], $page, $category, $page, $permalink);
      if ($this->user->loggedIn) {
        // Add this user so they receive follow up comments as notifications.
        $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
          '"comment-notifications-' . $page . '", "' . $this->user->name .'", '.
          '0) ON DUPLICATE KEY UPDATE edit_group = 0';
        if (!$mysqli->query($query)) {
          $this->Log('Comment->SetContent 2: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();

    // If page is published, let subscribers know this feed has been updated.
    if ($published === 1) {
      $reader = new Module($this->user, $this->owner, 'reader');
      if ($reader->IsInstalled()) {
        $url = $this->user->config->Secure() ? 'https://' : 'http://';
        $url .= $this->user->config->ServerName();
        $path = 'rss/index.php?page=' . $page . '&action=comment';
        $path = $this->owner === 'admin' ? $path : $this->owner . '/' . $path;
        $reader->Factory('UpdateFeed', $url . '/' . $path);
        // Also update the microformats comments feed and provide this comment
        // as the payload for the update.
        $reader->Factory('UpdateFeed',
                         [$url . $this->Url('', $page), $this->Content($id)]);
      }
    }
  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

  // Private functions below here ////////////////////////////////////////////

  private function MatchUrl($us_url) {
    $id = 0;
    $mysqli = connect_db();
    $url = $mysqli->escape_string($us_url);
    $query = 'SELECT box_id FROM comment WHERE url = "' . $url . '" AND ' .
      'user = "' . $this->owner . '" AND permalink LIKE ' .
      '"' . $this->user->page . '%"';
    if ($result = $mysqli->query($query)) {
      if ($comment = $result->fetch_assoc()) {
        $id = (int)$comment['box_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Comment->MatchUrl: ' . $mysqli->error);
    }
    $mysqli->close();
    return $id;
  }

  private function Modified($id, $us_description) {
    $modified = false;
    $mysqli = connect_db();
    $query = 'SELECT description FROM comment WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id;
    if ($result = $mysqli->query($query)) {
      if ($comment = $result->fetch_assoc()) {
        $modified = $comment['description'] !== $us_description;
      }
      $result->close();
    }
    else {
      $this->Log('Comment->Modified: ' . $mysqli->error);
    }
    $mysqli->close();
    return $modified;
  }

}
