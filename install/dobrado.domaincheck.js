// @source: /js/source/dobrado.domaincheck.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.domaincheck){dobrado.domaincheck={};}
(function(){'use strict';$(function(){if($('.domaincheck').length===0){return;}
$('#domaincheck-button').button().click(check);});function check(){function submit(){var email=$('#domaincheck-email').val();if(email===''){return false;}
$.post('/php/request.php',{request:'domaincheck',action:'submit',email:email,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'domaincheck submit')){return;}
let check=JSON.parse(response);$('#domaincheck-email-info').html(check.info);});return false;}
var domain=$('#domaincheck-input').val();if(domain===''){return false;}
$('#domaincheck-info').html('checking...');$.post('/php/request.php',{request:'domaincheck',action:'lookup',domain:domain,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'domaincheck lookup')){return;}
let check=JSON.parse(response);$('#domaincheck-info').html(check.info);$('#domaincheck-email-form').hide();if(check.email){$('#domaincheck-email-form').show();$('#domaincheck-email-submit').button().click(submit);}});return false;}}());