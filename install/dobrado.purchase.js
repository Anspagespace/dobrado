// @source: /js/source/dobrado.purchase.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.purchase){dobrado.purchase={};}
(function(){'use strict';var purchase=null;var purchaseGrid=null;var purchaseGridId='';var allAvailableGrid=null;var allAvailableGridId='';var allAvailableData=[];var allUserGrid=null;var allUserGridId='';var allUserData=[];var allUserChanges={};var currentCategory='';var currentProduct=null;var currentWeight='';var startTime=new Date().getTime();var tomorrow=0;var yesterday=0;var oneDay=86400000;var stockUpdate=0;var orderMode=false;var sparkID=null;var sparkAddress=null;var updateQuantity=false;var viewPurchases=true;var oldQuantity=0;var currentUser='';$(function(){if($('.purchase').length===0){return;}
if(dobrado.localStorage){if(localStorage.purchase){purchase=JSON.parse(localStorage.purchase);}}
else{$('.purchase .warning').show();}
var currentPage=$('#page-select').val();window.addEventListener('beforeunload',function(event){if(purchase&&purchase.processed&&purchase.processed.length!==0){$('.control .info').hide();$('#page-select').val(currentPage).selectmenu('refresh');var message='Please submit your order before leaving the page.';event.returnValue=message;return message;}});$('#purchase-details-form').dialog({show:true,autoOpen:false,modal:true,width:400,position:{my:'top',at:'top+50',of:window},title:'Add User Details',create:dobrado.fixedDialog});$('#purchase-details-form .submit').button().click(saveDetails);$('#purchase-form .view-all').button().click(viewAll);$('#purchase-categories > input').checkboxradio({icon:false});$('#purchase-categories').controlgroup().click(viewCategory);$('#purchase-available-orders').change(changeOrder);$('.purchase .save').button({disabled:true}).click(save);dobrado.account.preventLogout=false;$('#purchase-next-week-input').click(nextWeek);$('#purchase-form .remove').button({disabled:true}).click(remove);$('#purchase-form .add').button({disabled:true}).click(add);$('#purchase-form').keypress(addFromEnter);$('#purchase-name-input').val('').change(function(){setTimeout(showUser,10);});$('#purchase-product-input').val('').change(function(){setTimeout(showProduct,10);});$('#purchase-price-input').val('');$('.purchase-quantity-info').html('');$('#purchase-quantity-input').val('');$('#purchase-quantity-input').spinner({disabled:true,min:0,spin:setQuantity,change:setQuantity});$('.purchase-save-order-dismiss').button().click(function(){$('.purchase-save-order-confirm').dialog('close');});if($('.grid').length!==0){gridSetup();}
if($('#purchase-connect').length===1){$('#purchase-update-quantity').checkboxradio({icon:false}).click(toggleUpdate);$('#purchase-connect-settings').button({icon:'ui-icon-gear',showLabel:false}).click(connectSettings);$('#purchase-connect').controlgroup();}
if(!purchase||(purchase.processed&&purchase.processed.length===0&&purchaseDataStored())){loadProducts();}
else{sync();}});function decimalString(value){return(value+0.0001).toFixed(2);}
function purchaseDataStored(){if(purchase.data){for(var user in purchase.data){if(purchase.data[user].length!==0){return true;}}}
return false;}
function sync(){dobrado.log('Requesting server time...','info');$.post('/php/request.php',{request:'purchase',action:'time',timestamp:purchase.date,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase sync')){return;}
var current=JSON.parse(response);if(!current.save){loadProducts();}
else if(purchase.date<current.update||purchase.date<current.time-oneDay||purchase.group!==current.group||purchase.username!==current.username){if(purchase.processed&&purchase.processed.length!==0){stockUpdate=current.update;yesterday=current.time-oneDay;save();}
else{loadProducts();}}
else{tomorrow=current.time+oneDay;purchase.date=current.time;if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
$('.purchase-date-info').html(purchase.dateInfo);var source=[];$.each(purchase.users,function(i,user){if(purchase.details[user]&&!purchase.details[user].supplierOnly){source.push(user);}});$('#purchase-name-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:source,select:showUser});updateProducts();if(purchase.processed&&purchase.processed.length!==0){$('.purchase .message .stored').html('You have data stored.');$('.purchase .save').button('option','disabled',false);dobrado.account.preventLogout=true;alert('The product list needs to be updated.\n'+'Please submit your purchases and then reload the page.');}}});}
function loadProducts(){function dateInfo(){purchase.countdown-=60;if(purchase.countdown>60){var hours=Math.floor(purchase.countdown/3600);var minutes=Math.floor((purchase.countdown-(hours*3600))/60);var countdown='';if(hours===1){countdown+='1 hour ';}
else if(hours>1){countdown+=hours+' hours ';}
if(minutes===1){countdown+=minutes+' minute';}
else{countdown+=minutes+' minutes';}
$('.purchase-countdown').html(countdown);if(purchase.countdown<120){alert('Warning: This order is closing. '+'You need to submit your order now.');}}
else{if(dobrado.localStorage){localStorage.purchase='';}
purchase=null;location.reload();}}
dobrado.log('Loading products...','info');$.post('/php/request.php',{request:'purchase',action:'list',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase loadProducts')){return;}
purchase=JSON.parse(response);tomorrow=purchase.date+oneDay;var source=[];$.each(purchase.users,function(i,user){if(purchase.details[user]&&!purchase.details[user].supplierOnly){source.push(user);}});$('#purchase-name-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:source,select:showUser});updateProducts();if(purchase.username!==''){$('#purchase-name-input').val(purchase.username);showUser();purchase.username='';orderMode=true;}
if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
if(purchase.unavailable){$('.purchase-unavailable').html(purchase.unavailableText).dialog({classes:{'ui-dialog':'no-close'},show:true,modal:true,closeOnEscape:false,width:400,position:{my:'top',at:'top+50',of:window},title:'Ordering Unavailable',create:dobrado.fixedDialog});}
else{$('.purchase-date-info').html(purchase.dateInfo);if(purchase.countdown){setInterval(dateInfo,60000);}}});}
function gridSetup(){$('.grid').each(function(index){if(index===0){purchaseGridId='#'+$(this).attr('id');}
if(index===1){allAvailableGridId='#'+$(this).attr('id');}
if(index===2){allUserGridId='#'+$(this).attr('id');}});var purchaseColumns=[{id:'product',name:'Product',field:'name',width:490,sortable:true}];purchaseColumns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});purchaseColumns.push({id:'price',name:'Price',field:'price',width:130,sortable:true,formatter:Slick.Formatters.Units});if(!dobrado.mobile){purchaseColumns.push({id:'total',name:'Total',field:'total',width:120,sortable:true,formatter:Slick.Formatters.Dollar});}
var purchaseOptions={autoHeight:true,editable:true,forceFitColumns:true};purchaseGrid=dobrado.grid.instance(purchaseGridId,[],purchaseColumns,purchaseOptions);purchaseGrid.setSelectionModel(new Slick.RowSelectionModel());purchaseGrid.onClick.subscribe(function(e,item){showPurchase(item.row);});purchaseGrid.onSelectedRowsChanged.subscribe(function(e,item){if(item.rows.length===1){showPurchase(item.rows[0]);}});purchaseGrid.onCellChange.subscribe(updateAvailablePurchaseData);purchaseGrid.onBeforeEditCell.subscribe(updateOldQuantity);purchaseGrid.onSort.subscribe(function(e,args){var user=$('#purchase-name-input').val();if(user===''||!purchase.data||!purchase.data[user]){return;}
purchase.data[user].sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field==='quantity'||field==='price'||field==='total'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});purchaseGrid.invalidate();});if($('.grid').length>=2){var allAvailableColumns=[{id:'product',name:'Product',field:'name',width:490,sortable:true}];allAvailableColumns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});allAvailableColumns.push({id:'price',name:'Price',field:'price',width:130,sortable:true,formatter:Slick.Formatters.Units});if(!dobrado.mobile){allAvailableColumns.push({id:'total',name:'Total',field:'total',width:120,sortable:true,formatter:Slick.Formatters.Dollar});}
var allAvailableOptions={autoHeight:true,editable:true,forceFitColumns:true};allAvailableGrid=dobrado.grid.instance(allAvailableGridId,[],allAvailableColumns,allAvailableOptions);allAvailableGrid.setSelectionModel(new Slick.RowSelectionModel());allAvailableGrid.onClick.subscribe(function(e,item){showAllAvailablePurchase(item.row);});allAvailableGrid.onSelectedRowsChanged.subscribe(function(e,item){if(item.rows.length===1){showAllAvailablePurchase(item.rows[0]);}});allAvailableGrid.onCellChange.subscribe(updateAvailablePurchaseData);allAvailableGrid.onBeforeEditCell.subscribe(updateOldQuantity);allAvailableGrid.onSort.subscribe(function(e,args){allAvailableData.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field==='quantity'||field==='price'||field==='total'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});allAvailableGrid.invalidate();});}
if($('.grid').length===3){var allUserColumns=[{id:'user',name:'Username',field:'user',width:300,sortable:true},{id:'quantity',name:'Qty',field:'quantity',width:160,sortable:true,editor:Slick.Editors.Float},{id:'price',name:'Price',field:'price',width:170,sortable:true,formatter:Slick.Formatters.Units},{id:'total',name:'Total',field:'total',width:170,sortable:true,formatter:Slick.Formatters.Dollar}];var allUserOptions={autoHeight:true,editable:true,forceFitColumns:true};allUserGrid=dobrado.grid.instance(allUserGridId,[],allUserColumns,allUserOptions);allUserGrid.setSelectionModel(new Slick.RowSelectionModel());allUserGrid.onCellChange.subscribe(updateUserPurchaseData);allUserGrid.onBeforeEditCell.subscribe(updateOldQuantity);allUserGrid.onSort.subscribe(function(e,args){allUserData.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field!=='user'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});allUserGrid.invalidate();});}
$('.grid').hide();}
function setFormControls(){if($('#purchase-name-input').val()!==''){$('#purchase-form .remove').button('option','disabled',false);$('#purchase-form .add').button('option','disabled',false);}
else{$('#purchase-quantity-input').spinner('disable');$('#purchase-form .remove').button('option','disabled',true);$('#purchase-form .add').button('option','disabled',true);}}
function changeOrder(){if(purchase&&purchase.processed&&purchase.processed.length!==0){alert('Please submit your current order before changing.');return false;}
$.post('/php/request.php',{request:'purchase',action:'change',group:$(this).val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase changeOrder')){return;}
location.reload();});}
function showUser(event,ui){var user=$('#purchase-name-input').val();if(ui){user=ui.item.value;}
if(user===currentUser){return;}
resetForm();$('#purchase-form .order').hide();$(purchaseGridId).hide();setFormControls();currentUser=user;if(user===''){return;}
if($.inArray(user,purchase.users)===-1){alert('If an account was just created for '+user+' please reload the page.');$('#purchase-name-input').val('');return;}
var price=purchase.buyerGroup[user];if(!viewPurchases){viewPurchases=true;$('#purchase-form .view-all').button('option','label','View all');$('#purchase-form .remove').show();$('#purchase-form .add').show();$('.purchase-quantity-info').parent().show();$('#purchase-price-input').parent().show();$('.purchase-all-available').hide();$('.purchase-all-users').hide();$(allAvailableGridId).hide();$(allUserGridId).hide();}
if($.inArray(user,purchase.nextWeek)===-1){$('#purchase-next-week-input').prop('checked',false);}
else{$('#purchase-next-week-input').prop('checked',true);}
var total=0;if(purchase.total&&purchase.total[user]){total=purchase.total[user];}
var quantity=1;if(purchase.data[user]){$.each(purchase.data[user],function(i,item){if(!item.unit){$.each(purchase.products,function(index,product){if(item.name===product.name){item.unit=product.unit;if(product.quantityAdjustment&&decimalString(product[price])!==item.price){item.basePrice=decimalString(product.price);item.price=decimalString(product[price]);item.server=false;if(orderMode){quantity=parseFloat(item.quantity)/product.quantityAdjustment;item.quantity=formatFloat(quantity);item.total=decimalString(quantity*product[price]);item.unit='each';}
else{quantity=parseFloat(item.quantity)*product.quantityAdjustment;item.quantity=formatFloat(quantity);item.total=decimalString(quantity*product[price]);item.unit=product.unit;}}
return false;}});}
if(item.date<tomorrow){total+=parseFloat(item.total);}});if(purchaseGrid&&purchase.data[user].length!==0){$(purchaseGridId).show();purchaseGrid.setData(purchase.data[user]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}}
else{purchase.data[user]=[];}
$('#purchase-form .total').html(decimalString(total));$('#purchase-form .order').show();if(purchase.roster){if($.inArray(user,purchase.roster)===-1){$('.roster-reminder').hide();$('.roster-volunteer').show();}
else{$('.roster-reminder').show();$('.roster-volunteer').hide();}}
var details=purchase.details[user];if(details&&details.first&&details.last){$('#purchase-form .order .name').html(details.first+' '+details.last);}
else if(details&&details.first){$('#purchase-form .order .name').html(details.first);}
else{$('#purchase-form .order .name').html(user);$('.purchase-details-user').html(user);$('#purchase-details-first-input').val('');$('#purchase-details-last-input').val('');$('#purchase-details-phone-input').val('');$('#purchase-details-info').html('');if(details){if(details.first){$('#purchase-details-first-input').val(dobrado.decode(details.first));}
if(details.last){$('#purchase-details-last-input').val(dobrado.decode(details.last));}
if(details.phone){$('#purchase-details-phone-input').val(dobrado.decode(details.phone));}}
if(!$('#purchase-details-form').dialog('isOpen')){$('#purchase-details-form').dialog('open');}}
$('.warning-level').hide();$('.info-level').hide();if(user in purchase.outstanding){if(purchase.outstanding[user]>=purchase.info){var outstanding=decimalString(purchase.outstanding[user]);var name=user;if(details.first){name=details.first;}
if(purchase.warning>0.01&&purchase.outstanding[user]>=purchase.warning){$('.warning-level .user').html(name);$('.warning-level .outstanding').html(outstanding);$('.warning-level').show();}
else if(purchase.info>0.01){$('.info-level .user').html(name);$('.info-level .outstanding').html(outstanding);$('.info-level').show();}}}
if(details.supplierOnly){alert('Please note that '+user+' is set as an external supplier.');}}
function showPurchase(row){var user=$('#purchase-name-input').val();if(user===''){return false;}
var data=purchase.data[user][row];if(!data){return false;}
$('#purchase-product-input').val(dobrado.decode(data.name));$('#purchase-quantity-input').val('');$('.purchase-quantity-info').html('');var price=data.quantity*data.price;$.each(purchase.products,function(index,item){if(item.name===data.name){currentProduct=item;if(currentProduct.unit==='variable'){$('#purchase-price-input').val(data.price).attr('readonly',false);$('#purchase-quantity-input').spinner('value','1');$('#purchase-quantity-input').spinner('option','disabled',true);}
else{$('#purchase-price-input').val('$'+decimalString(price)+' @ ($'+data.price+'/'+
currentProduct.unit+')');$('#purchase-price-input').attr('readonly',true);$('#purchase-quantity-input').spinner('enable');}
if(currentProduct.grower){$('#purchase-grower-info').text('Produced by: '+
currentProduct.grower);}
else{$('#purchase-grower-info').html('');}
$('#purchase-quota-info').html(showQuota('form'));return false;}});setFormControls();}
function showProduct(event,ui){var user=$('#purchase-name-input').val();var price='retail';if(user!==''){price=purchase.buyerGroup[user];}
var productFound=false;var product=$('#purchase-product-input').val();if(ui){product=ui.item.value;}
$('.purchase-quantity-info').html('');$.each(purchase.products,function(index,item){if(item.name===product){productFound=true;currentProduct=item;if(currentProduct.unit==='variable'){$('#purchase-quantity-input').val('1').spinner('disable');$('#purchase-price-input').val(decimalString(item[price]));$('#purchase-price-input').attr('readonly',false);}
else{$('#purchase-price-input').val('$'+decimalString(item[price])+'/'+item.unit);$('#purchase-price-input').attr('readonly',true);if(currentProduct.unit==='kg'&&currentWeight!==''){$('#purchase-quantity-input').val(currentWeight).spinner('enable');setQuantity();}
else{$('#purchase-quantity-input').val('').spinner('enable');}}
if(currentProduct.grower){$('#purchase-grower-info').text('Produced by: '+
currentProduct.grower);}
else{$('#purchase-grower-info').html('');}
if(viewPurchases){$('#purchase-quota-info').html(showQuota('form'));if(user!==''&&purchaseGrid){$.each(purchase.data[user],function(i,item){if(item.name===product){purchaseGrid.setSelectedRows([i]);purchaseGrid.scrollRowIntoView(i);return false;}});}}
else if(user===''){$('#purchase-quota-info').html(showQuota('all-users'));viewAllUsers();}
else if(allAvailableGrid){var id='';$('#purchase-categories > input').each(function(index){id=$(this).attr('id');if(item.category===$('#'+id).button('option','label')){return false;}});if(id===''){$('#purchase-categories > input').last().click();}
else{$('#'+id).click();}
viewCategory();$.each(allAvailableData,function(i,item){if(item.name===product){allAvailableGrid.setSelectedRows([i]);allAvailableGrid.scrollRowIntoView(i);return false;}});}
return false;}});if(!productFound){resetForm();}
setFormControls();if(viewPurchases){$('#purchase-quantity-input').focus();}
else{$('#purchase-product-input').focus();}}
function formatFloat(value){value=parseFloat(value);value=value.toFixed(3);if(value.indexOf('.')!==-1){for(var i=value.length-1;i>0;i--){if(value[i]==='0'){continue;}
if(value[i]==='.'){value=value.substring(0,i);}
else{value=value.substring(0,i+1);}
break;}}
return value;}
function soldOut(quantity){if(!currentProduct){return false;}
if(currentProduct.track&&currentProduct.quantity<0.001){alert('Could not add purchase: Product has sold out.');return true;}
if(currentProduct.track&&currentProduct.quantity<quantity){var available=formatFloat(currentProduct.quantity);var unit=currentProduct.unit;if(unit==='each'||unit==='variable'){unit='';}
alert('Could not add purchase: Only '+available+unit+' available.');return true;}
return false;}
function showQuota(input){var total=0;function setQuotaQuantity(i,item){if(item.name===currentProduct.name){total+=parseFloat(item.quantity);}}
if(!currentProduct){return'';}
if(!orderMode&&input!=='all-users'){return'';}
if(currentProduct.size===0){return'';}
for(var user in purchase.data){$.each(purchase.data[user],setQuotaQuantity);}
let soldOutText='';if(purchase.stockLimited&&currentProduct.track&&currentProduct.quantity<0.001){soldOutText=' <b>This product is sold out.</b>';if(input==='form'){$('#purchase-quantity-input').val('').spinner('disable');}}
let remainder=total%currentProduct.size;let count=(total-remainder)/currentProduct.size;if(remainder!==0||count===0){count++;}
count=count.toFixed(0);let percent=total/(currentProduct.size*count)*100;let color;if(percent<30){color='red';}
else if(percent<50){color='orange';}
else{color='green';}
let current=remainder;if(remainder===0&&total!==0){current=currentProduct.size;}
let unit=currentProduct.unit;if(unit==='each'||unit==='variable'){unit='';}
if(input==='grid'){if(total===0){return'';}
return'<span class="purchase-grid-quota" style="background-color:'+
color+'">box '+count+'</span>';}
return'Quota: <span id="purchase-quota-wrapper">'+'<span id="purchase-quota-bar" style="background-color:'+color+';'+'width:'+percent+'%"></span></span> '+'(Currently: '+formatFloat(current)+'/'+currentProduct.size+unit+' in box: <b>'+count+'</b>)'+soldOutText;}
function updateProducts(){var products=[];$.each(purchase.products,function(index,item){products.push(item.name);});$('#purchase-product-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:products,select:showProduct});}
function checkDecimal(quantity,unit){var text='';if(/\./.test(quantity)){if(unit==='each'){text='Note: This product is usually purchased in whole quantities.';}}
else{if(unit==='kg'||unit==='g'){text='Note: This product is purchased by weight.';}
else if(unit==='L'){text='Note: This product is purchased by volume.';}}
$('.purchase-quantity-info').html(text);}
function setQuantity(event,ui){if(!currentProduct){return;}
var user=$('#purchase-name-input').val();var price='retail';if(user!==''){price=purchase.buyerGroup[user];}
var quantity=0;if(ui&&'value'in ui){quantity=ui.value;}
else{quantity=parseFloat($('#purchase-quantity-input').val());if(!quantity||quantity<0){quantity=0;$('#purchase-quantity-input').val('0');}}
checkDecimal(quantity,currentProduct.unit);if(currentProduct.unit!=='variable'){var total=decimalString(quantity*currentProduct[price]);$('#purchase-price-input').val('$'+total+' @ ($'+
decimalString(currentProduct[price])+'/'+currentProduct.unit+')');}}
function remove(){var user=$('#purchase-name-input').val();var product=$('#purchase-product-input').val();removePurchase(user,product,purchase.date);return false;}
function removePurchase(user,product,currentTime){function removePurchaseFromGrid(user,position,total){$('#purchase-form .total').html(decimalString(total));purchase.data[user].splice(position,1);if(purchaseGrid&&purchase.data[user].length>0){purchaseGrid.setData(purchase.data[user]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}
else{$(purchaseGridId).hide();$.each(purchase.processed,function(i,item){if(item===user){purchase.processed.splice(i,1);}});}
if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
if(purchase.processed.length===0){$('.purchase .message .stored').html('');$('.purchase .save').button('option','disabled',true);$('#purchase-form .order .info').hide();dobrado.account.preventLogout=false;}}
var total=parseFloat($('#purchase-form .total').html());var timestamp=0;var position=0;if(purchase.data[user]){$.each(purchase.data[user],function(i,item){if(item.name===product){if(item.date>currentTime&&item.date-currentTime>oneDay){return true;}
if(item.date<currentTime&&currentTime-item.date>oneDay){return true;}
position=i;if(item.date<tomorrow){total-=item.total;}
if(item.server){timestamp=item.date;}
else{removePurchaseFromGrid(user,position,total);}
if(currentProduct){currentProduct.quantity+=parseFloat(item.quantity);if(currentProduct.quantityAdjustment){timestamp=item.date;}}
return false;}});}
resetForm();setFormControls();if(user===''){return false;}
if(timestamp!==0){dobrado.log('Removing item from order...','info');$.post('/php/request.php',{request:'purchase',buyer:user,name:product,timestamp:timestamp,action:'remove',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase remove')){return;}
removePurchaseFromGrid(user,position,total);});}
return false;}
function addFromEnter(event){if(event.which===13){event.preventDefault();add();}}
function add(){var user=$('#purchase-name-input').val();if(user===''||!currentProduct){return false;}
var product=currentProduct.name;if(product!==$('#purchase-product-input').val()){alert('Product not found.');resetForm();return false;}
var accumulative=true;var supplier=currentProduct.user;var grower=currentProduct.grower;var unit=currentProduct.unit;var priceLevel=purchase.buyerGroup[user];if(!priceLevel){priceLevel='retail';}
var price=currentProduct[priceLevel];var basePrice=currentProduct.price;if(unit==='variable'){price=parseFloat($('#purchase-price-input').val());if(!price){$('#purchase-price-input').val('');return false;}
basePrice=price;if(priceLevel==='wholesale'&&purchase.wholesalePercent){basePrice-=basePrice*purchase.wholesalePercent/100;}
else if(priceLevel==='retail'&&purchase.retailPercent){basePrice-=basePrice*purchase.retailPercent/100;}
accumulative=false;}
var quantity=parseFloat($('#purchase-quantity-input').val());if(!quantity||quantity<0){return false;}
oldQuantity=0;update(user,product,supplier,grower,unit,price,basePrice,quantity,purchase.date,accumulative,true);return false;}
function update(user,product,supplier,grower,unit,price,basePrice,quantity,time,accumulative,updateGrid){var elapsed=new Date().getTime()-startTime;if(elapsed>oneDay){alert('Please reload the page before continuing');return false;}
var purchaseTime=purchase.date+elapsed;if((time>purchaseTime&&time-purchaseTime>oneDay)||(time<purchaseTime&&purchaseTime-time>oneDay)){purchaseTime=time;}
var itemTotal=parseFloat(decimalString(quantity*price));var selectedRow=0;var newItem=true;var quantityUpdated=false;if(!purchase.data[user]){purchase.data[user]=[];}
$.each(purchase.data[user],function(i,item){if(item.name===product){if(item.date>purchaseTime&&item.date-purchaseTime>oneDay){return true;}
if(item.date<purchaseTime&&purchaseTime-item.date>oneDay){return true;}
newItem=false;if(quantity<oldQuantity){currentProduct.quantity+=oldQuantity-quantity;}
else if(purchase.stockLimited&&soldOut(quantity-oldQuantity)){if(currentProduct.quantity>0){quantity=parseFloat(oldQuantity)+
parseFloat(currentProduct.quantity);}
else{quantity=parseFloat(oldQuantity);}
currentProduct.quantity=0;quantityUpdated=true;itemTotal=parseFloat(decimalString(quantity*price));}
else{currentProduct.quantity-=quantity-oldQuantity;}
if(accumulative){itemTotal+=parseFloat(item.total);quantity+=parseFloat(item.quantity);}
purchase.data[user][i]={name:product,date:purchaseTime,supplier:supplier,grower:grower,unit:unit,quantity:formatFloat(quantity),price:decimalString(price),basePrice:decimalString(basePrice),total:decimalString(itemTotal),server:false};if(purchaseGrid&&updateGrid){$(purchaseGridId).show();purchaseGrid.setData(purchase.data[user]);if(!viewPurchases){$(purchaseGridId).hide();}
selectedRow=i;}
return false;}});if(newItem){if(purchase.stockLimited&&soldOut(quantity-oldQuantity)){if(currentProduct.quantity>0){quantity=currentProduct.quantity;}
else{quantity=0;}
currentProduct.quantity=0;quantityUpdated=true;itemTotal=parseFloat(decimalString(quantity*price));}
else{currentProduct.quantity-=quantity-oldQuantity;}
purchase.data[user].push({name:product,date:purchaseTime,supplier:supplier,grower:grower,unit:unit,quantity:formatFloat(quantity),price:decimalString(price),basePrice:decimalString(basePrice),total:decimalString(itemTotal),server:false});if(purchaseGrid&&updateGrid&&purchase.data[user].length===1){$(purchaseGridId).show();purchaseGrid.setData(purchase.data[user]);if(!viewPurchases){$(purchaseGridId).hide();}}
selectedRow=purchase.data[user].length-1;}
if(updateGrid){var userTotal=0;if(purchase.total&&purchase.total[user]){userTotal=purchase.total[user];}
$.each(purchase.data[user],function(i,item){if(item.date<tomorrow){userTotal+=parseFloat(item.total);}});$('#purchase-form .total').html(decimalString(userTotal));if(purchaseGrid&&viewPurchases){$(purchaseGridId).show();purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([selectedRow]);purchaseGrid.scrollRowIntoView(selectedRow);}
else if(quantityUpdated){viewAllAvailable(user);}
resetForm();}
if($.inArray(user,purchase.processed)===-1){purchase.processed.push(user);}
if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
$('.purchase .save').button('option','disabled',false);$('#purchase-form .order .info').show();dobrado.account.preventLogout=true;}
function save(){function saveToServer(){function saveData(){var data={};$.each(purchase.processed,function(i,user){if(user in purchase.data){$.each(purchase.data[user],function(i,item){if(!item.server){if(!data[user]){data[user]=[item];}
else{data[user].push(item);}}});}});return data;}
$.post('/php/request.php',{request:'purchase',data:JSON.stringify(saveData()),nextWeek:JSON.stringify(purchase.nextWeek),action:'save',timestamp:purchase.date,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase save')){return;}
$('.purchase .message .stored').html('');$('.purchase .save').button('option','disabled',true);$('.warning-level').hide();$('.info-level').hide();$('.roster-reminder').hide();$('.roster-volunteer').hide();$('#purchase-form .order .info').hide();$('#purchase-name-input').val('');currentUser='';$(purchaseGridId).hide();dobrado.account.preventLogout=false;resetForm();var save=JSON.parse(response);if(save.order){$('.purchase-save-order-confirm').dialog({show:true,modal:true,width:400,position:{my:'top',at:'top+50',of:window},title:'Order Saved',create:dobrado.fixedDialog});}
$.each(purchase.processed,function(i,user){if(user in purchase.data){$.each(purchase.data[user],function(i,item){item.server=true;});}});purchase.processed=[];if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
if(purchase.date<stockUpdate||purchase.date<yesterday||save.order){loadProducts();}});}
dobrado.log('Saving...','info');if(viewPurchases){if(purchaseGrid){purchaseGrid.gotoCell(0,0);}}
else{if(allAvailableGrid){allAvailableGrid.gotoCell(0,0);}
if(allUserGrid){allUserGrid.gotoCell(0,0);}}
setTimeout(saveToServer,200);return false;}
function saveDetails(){var user=$('#purchase-name-input').val();if(user===''){return;}
var first=$('#purchase-details-first-input').val();var last=$('#purchase-details-last-input').val();var phone=$('#purchase-details-phone-input').val();if(first===''){$('#purchase-details-info').html('Please enter a first name.');return false;}
dobrado.log('Saving user details...','info');$.post('/php/request.php',{request:'purchase',username:user,first:first,last:last,phone:phone,action:'saveDetails',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase saveDetails')){return;}
if(!purchase.details[user]){purchase.details[user]=[];}
purchase.details[user].first=first;purchase.details[user].last=last;purchase.details[user].phone=phone;if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
$('#purchase-form .order .name').html(first+' '+last);$('#purchase-details-form').dialog('close');});return false;}
function nextWeek(){var user=$('#purchase-name-input').val();if(user===''){return;}
if($(this).is(':checked')){purchase.nextWeek.push(user);}
else{$.each(purchase.nextWeek,function(i,item){if(item===user){purchase.nextWeek.splice(i,1);return false;}});}
if(dobrado.localStorage){localStorage.purchase=JSON.stringify(purchase);}
$('.purchase .save').button('option','disabled',false);dobrado.account.preventLogout=true;}
function resetForm(){if(viewPurchases){$('#purchase-product-input').focus();}
$('#purchase-product-input').val('');$('#purchase-grower-info').html('');$('#purchase-quota-info').html('');$('#purchase-price-input').val('');$('#purchase-quantity-input').val('');$('.purchase-quantity-info').html('');currentProduct=null;}
function viewAll(){var user=$('#purchase-name-input').val();var label='View all';if(!purchase){return false;}
if(viewPurchases){viewPurchases=false;label=orderMode?'View orders only':'View purchases only';$('#purchase-form .view-all').button('option','label',label);$('#purchase-form .remove').hide();$('#purchase-form .add').hide();$('.purchase-quantity-info').parent().hide();$('#purchase-price-input').parent().hide();if(user===''&&currentProduct){viewAllUsers();}
else{viewAllAvailable(user);}}
else{viewPurchases=true;$('#purchase-form .view-all').button('option','label',label);$('#purchase-form .remove').show();$('#purchase-form .add').show();$('.purchase-quantity-info').parent().show();$('#purchase-price-input').parent().show();$('.purchase-all-available').hide();$('.purchase-all-users').hide();if(allAvailableGrid){allAvailableGrid.gotoCell(0,0);$(allAvailableGridId).hide();}
if(allUserGrid){allUserGrid.gotoCell(0,0);$(allUserGridId).hide();}
resetForm();if(purchase.data[user]){if(purchaseGrid&&purchase.data[user].length!==0){$(purchaseGridId).show();purchaseGrid.setData(purchase.data[user]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}}}
return false;}
function viewAllUsers(){function setAllUsersQuantity(i,item){if(item.name===currentProduct.name){if(currentProduct.quantityAdjustment&&decimalString(currentProduct[price])!==item.price){item.basePrice=decimalString(currentProduct.price);item.price=decimalString(currentProduct[price]);if(orderMode){quantity=parseFloat(item.quantity)/currentProduct.quantityAdjustment;item.quantity=formatFloat(quantity);item.total=decimalString(quantity*currentProduct[price]);item.unit='each';}
else{quantity=parseFloat(item.quantity)*currentProduct.quantityAdjustment;item.quantity=formatFloat(quantity);item.total=decimalString(quantity*currentProduct[price]);item.unit=currentProduct.unit;}}
else{quantity=item.quantity;}
return false;}}
var price='retail';var quantity=0;if(purchaseGrid){purchaseGrid.gotoCell(0,0);$(purchaseGridId).hide();}
allUserData=[];for(var i=0;i<purchase.users.length;i++){var user=purchase.users[i];if(purchase.details[user]&&purchase.details[user].supplierOnly){continue;}
price=purchase.buyerGroup[user];quantity=0;if(purchase.data[user]){$.each(purchase.data[user],setAllUsersQuantity);}
var total=decimalString(quantity*currentProduct[price]);allUserData.push({user:user,date:purchase.date,quantity:quantity,unit:currentProduct.unit,name:currentProduct.name,supplier:currentProduct.user,grower:currentProduct.grower,price:decimalString(currentProduct[price]),basePrice:decimalString(currentProduct.price),total:total});}
var description='Showing quantity for all users for product: <b>'+
dobrado.encode(currentProduct.name)+'</b>';$('#purchase-quota-info').html(showQuota('all-users'));$('.purchase-all-users').html(description).show();$('.purchase-all-available').hide();$(allAvailableGridId).hide();if(allUserGrid){$(allUserGridId).show();allUserGrid.setData(allUserData);allUserGrid.updateRowCount();allUserChanges={};allUserGrid.removeCellCssStyles('grid-row-updated');allUserGrid.render();$(allUserGridId+' .slick-header-columns').children().eq(0).click();}}
function viewAllAvailable(user){var price='retail';if(user!==''){price=purchase.buyerGroup[user];}
if(purchaseGrid){purchaseGrid.gotoCell(0,0);$(purchaseGridId).hide();}
var categoryLastID=$('#purchase-categories > input').last().attr('id');var categoryLast=null;if(categoryLastID){categoryLast=dobrado.decode($('#'+categoryLastID).button('option','label'));}
var categoryID=$('#purchase-categories > input:checked').attr('id');var category='';if(categoryID){category=dobrado.decode($('#'+categoryID).button('option','label'));}
var current={};if(purchase.data[user]){$.each(purchase.data[user],function(index,item){if(!current[item.name]){current[item.name]=[];}
current[item.name].push({date:item.date,quantity:item.quantity});});}
allAvailableData=[];var total=0;$.each(purchase.products,function(productIndex,product){if(category===categoryLast){if($.inArray(product.category,purchase.categories)!==-1){return true;}}
else if(category!==''&&category!==product.category){return true;}
if(current[product.name]){$.each(current[product.name],function(currentIndex,currentItem){total=decimalString(currentItem.quantity*product[price]);allAvailableData.push({date:currentItem.date,name:product.name,supplier:product.user,grower:product.grower,quantity:currentItem.quantity,unit:product.unit,price:decimalString(product[price]),basePrice:decimalString(product.price),total:total});});}
else{allAvailableData.push({date:purchase.date,name:product.name,supplier:product.user,grower:product.grower,quantity:0,unit:product.unit,price:decimalString(product[price]),basePrice:decimalString(product.price),total:'0.00'});}});resetForm();$('.purchase-all-users').hide();$('.purchase-all-available').show();$(allUserGridId).hide();if(allAvailableGrid){$(allAvailableGridId).show();allAvailableGrid.setData(allAvailableData);allAvailableGrid.updateRowCount();allAvailableGrid.render();}}
function viewCategory(){if(allAvailableGrid){allAvailableGrid.gotoCell(0,0);allAvailableGrid.setSelectedRows([]);}
var categoryID=$('#purchase-categories > input:checked').attr('id');var category='';if(categoryID){category=dobrado.decode($('#'+categoryID).button('option','label'));}
if(currentCategory===category)return;currentCategory=category;viewAllAvailable($('#purchase-name-input').val());}
function showAllAvailablePurchase(row){if(!allAvailableData){return false;}
var data=allAvailableData[row];$('#purchase-product-input').val(dobrado.decode(data.name));$('#purchase-quantity-input').val('');$('.purchase-quantity-info').html('');var price=data.quantity*data.price;$.each(purchase.products,function(index,item){if(item.name===data.name){currentProduct=item;if(currentProduct.unit==='variable'){$('#purchase-price-input').val(data.price).attr('readonly',false);$('#purchase-quantity-input').spinner('value','1');$('#purchase-quantity-input').spinner('option','disabled',true);}
else{$('#purchase-price-input').val('$'+decimalString(price)+' @ ($'+data.price+'/'+
currentProduct.unit+')');$('#purchase-price-input').attr('readonly',true);$('#purchase-quantity-input').spinner('enable');}
if(currentProduct.grower){$('#purchase-grower-info').text('Produced by: '+
currentProduct.grower);}
else{$('#purchase-grower-info').html('');}
$('#purchase-quota-info').html(showQuota('all-available'));return false;}});setFormControls();}
function updateAvailablePurchaseData(e,args){var user=$('#purchase-name-input').val();if(user===''){return;}
var item=args.item;var price=parseFloat(item.price);var basePrice=parseFloat(item.basePrice);var updateProduct=null;$.each(purchase.products,function(index,product){if(product.name===item.name){updateProduct=product;return false;}});if(!updateProduct){item.quantity=oldQuantity;alert('Product not found.');return;}
var grower=updateProduct.grower;var unit=updateProduct.unit;var priceLevel=purchase.buyerGroup[user];if(!priceLevel){priceLevel='retail';}
if(unit==='variable'){if(item.quantity!==0){item.quantity=1;}
if(priceLevel==='wholesale'&&purchase.wholesalePercent){basePrice-=basePrice*purchase.wholesalePercent/100;}
else if(priceLevel==='retail'&&purchase.retailPercent){basePrice-=basePrice*purchase.retailPercent/100;}}
else{checkDecimal(item.quantity,unit);}
var newProduct=currentProduct;currentProduct=updateProduct;update(user,item.name,item.supplier,grower,unit,price,basePrice,item.quantity,item.date,false,true);args.item.total=decimalString(item.quantity*price);if(allAvailableGrid){allAvailableGrid.invalidate();}
currentProduct=updateProduct;$('#purchase-quota-info').html(showQuota('all-available'));currentProduct=newProduct;}
function updateUserPurchaseData(e,args){var item=args.item;var price=parseFloat(item.price);var basePrice=parseFloat(item.basePrice);var updateProduct=null;$.each(purchase.products,function(index,product){if(product.name===item.name){updateProduct=product;return false;}});if(!updateProduct){item.quantity=oldQuantity;alert('Product not found.');return;}
var grower=updateProduct.grower;var unit=updateProduct.unit;var priceLevel=purchase.buyerGroup[item.user];if(!priceLevel){priceLevel='retail';}
if(item.quantity>0){if(unit==='variable'){item.quantity=1;if(priceLevel==='wholesale'&&purchase.wholesalePercent){basePrice-=basePrice*purchase.wholesalePercent/100;}
else if(priceLevel==='retail'&&purchase.retailPercent){basePrice-=basePrice*purchase.retailPercent/100;}}
else{checkDecimal(item.quantity,unit);}
update(item.user,item.name,item.supplier,grower,unit,price,basePrice,item.quantity,item.date,false,false);}
else{removePurchase(item.user,item.name,item.date);}
args.item.total=decimalString(item.quantity*price);if(allUserGrid){allUserChanges[args.row]={user:'grid-row-updated',date:'grid-row-updated',quantity:'grid-row-updated',price:'grid-row-updated',total:'grid-row-updated'};allUserGrid.setCellCssStyles('grid-row-updated',allUserChanges);allUserGrid.invalidate();}
var newProduct=currentProduct;currentProduct=updateProduct;$('#purchase-quota-info').html(showQuota('all-users'));currentProduct=newProduct;}
function updateOldQuantity(e,args){if(args&&args.item){oldQuantity=args.item.quantity;}}
function toggleUpdate(){if($('#purchase-update-quantity').is(':checked')){if(sparkID){updateQuantity=true;checkQuantity();}
else{connectSettings();}}
else{updateQuantity=false;}}
function connectSettings(){var sparkOptions=[];function sparkConnect(){var name=$('#purchase-spark-list').val();sparkID=sparkOptions[name].id;sparkAddress=sparkOptions[name].ip;$('.purchase-connect-settings').dialog('close');if(!$('#purchase-update-quantity').is(':checked')){$('#purchase-update-quantity').prop('checked',true);$('#purchase-update-quantity').checkboxradio('refresh');}
if(!updateQuantity){updateQuantity=true;checkQuantity();}}
dobrado.log('Loading connect settings...','info');$.post('/php/request.php',{request:'purchase',action:'connectSettings',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'purchase connectSettings')){return;}
var content='';var connect=JSON.parse(response);if(connect.settings.length===0){content='<p>There are no scales available to connect to.</p><p>'+'Please talk to an administrator about updating your settings.</p>';$('#purchase-update-quantity').prop('checked',false);$('#purchase-update-quantity').checkboxradio('refresh');}
else if(connect.settings.length===1){var spark=connect.settings[0];if(sparkID===spark.id&&sparkAddress===spark.ip){content='<p>You are currently connected to <b>'+spark.name+'</b>';if(!updateQuantity){content+=', click the connect button to receive updates';}
content+='.<br><span class="purchase-spark-text">Address: '+
spark.ip+'<br>ID: </span>'+'<span class="purchase-spark-id">'+spark.id+'</span>';}
else{if(!sparkID){updateQuantity=true;if(!$('#purchase-update-quantity').is(':checked')){$('#purchase-update-quantity').prop('checked',true);$('#purchase-update-quantity').checkboxradio('refresh');}
sparkID=spark.id;sparkAddress=spark.ip;checkQuantity();}
else{sparkID=spark.id;sparkAddress=spark.ip;}
content='<p>You are now connected to <b>'+spark.name+'</b>';if(!updateQuantity){content+=', click the connect button to receive updates';}
content+='.<br><span class="purchase-spark-text">Address: '+
spark.ip+'<br>ID: </span>'+'<span class="purchase-spark-id">'+spark.id+'</span>';}}
else{var name='';var options='';$.each(connect.settings,function(index,item){sparkOptions[item.name]={id:item.id,ip:item.ip};if(sparkID===item.id){name=item.name;options+='<option selected="selected">'+item.name+'</option>';}
else{options+='<option>'+item.name+'</option>';}});if(name){content='<p>You are currently connected to <b>'+name+'</b>';if(!updateQuantity){content+=', click the connect button to receive updates';}
content+='.<br><span class="purchase-spark-text">Address: '+
sparkAddress+'<br>ID: </span>'+'<span class="purchase-spark-id">'+sparkID+'</span></p>'+'<p>You can select different scales to connect to:<br>';}
else{content='<p>Please choose the scales to connect to:<br><br>';}
content+='<label for="purchase-spark-list">Name:</label>'+'<select id="purchase-spark-list">'+options+'</select><br><br>'+'<button id="purchase-spark-connect">connect</button></p>';}
$('.purchase-connect-settings').html(content).dialog({show:true,width:400,position:{my:'top',at:'top+50',of:window},title:'Connect Scales',create:dobrado.fixedDialog});$('#purchase-spark-connect').button().click(sparkConnect);});return false;}
function checkQuantity(){function createCORSRequest(url){var xhr=new XMLHttpRequest();if('withCredentials'in xhr){xhr.open('GET',url,true);}
else if(typeof XDomainRequest!=='undefined'){xhr=new XDomainRequest();xhr.open('GET',url);}
else{xhr=null;}
return xhr;}
if(!updateQuantity||!sparkID||!sparkAddress){return;}
var xhr=createCORSRequest('http://'+sparkAddress);if(!xhr){console.log('CORS not supported.');return;}
xhr.onload=function(){var weight=xhr.responseText.match(/([0-9.]+)kg/)[0];if(currentProduct&&currentProduct.unit==='kg'){$('#purchase-quantity-input').val(weight);setQuantity();}
currentWeight=weight;checkQuantity();};xhr.onerror=function(){$('#purchase-quantity-input').val('');console.log('Error reading from spark.');setTimeout(checkQuantity,1000);};xhr.send();}
dobrado.purchase.settingsCallback=function(settings){function customGridColumns(){function supplierFormatter(row,cell,value,columnDef,dataContext){var display='';if(purchase.suppliers[value]){if(purchase.suppliers[value].first){display=purchase.suppliers[value].first;}
if(purchase.suppliers[value].last){display+=' '+purchase.suppliers[value].last;}}
if(display!=='')return display;return value;}
function combinedFormatter(row,cell,value,columnDef,dataContext){var image='';var price='';var lightboxPrefix=viewPurchases?'':'all ';if(dataContext.unit==='variable'){price='as marked';}
else{price='$'+dataContext.price+'/'+dataContext.unit;}
$.each(purchase.products,function(index,item){if(item.name===value){if(item.image){image='<a href="'+item.image+'" data-lightbox="'+
lightboxPrefix+value+'" data-title="'+value+'">'+'<img class="purchase-grid-image" src="'+item.image+'"></a>';}
else{image='<div class="purchase-grid-no-image"></div>';}
currentProduct=item;return false;}});let quota=showQuota('grid');currentProduct=null;let grower='';if(dataContext.grower!==''){grower='Produced by: '+dataContext.grower;}
return'<div class="purchase-grid-product-wrapper">'+image+'<span class="purchase-grid-price">'+price+'</span><span class="purchase-grid-product">'+value+'</span></div><div>'+quota+'<span class="purchase-grid-grower">'+
grower+'</span></div>';}
let purchaseColumns=[];if(settings.gridColumns==='combinedFormat'){$('#purchase-grower-info').hide();if(purchaseGrid){purchaseGrid.setOptions({rowHeight:70});}
purchaseColumns.push({id:'product',name:'Product',field:'name',width:500,sortable:true,formatter:combinedFormatter});}
else if(settings.gridColumns==='showSupplier'){purchaseColumns.push({id:'product',name:'Product',field:'name',width:290,sortable:true});if(!dobrado.mobile){purchaseColumns.push({id:'supplier',name:'Supplier',field:'supplier',width:200,sortable:true,formatter:supplierFormatter});}}
else{return;}
purchaseColumns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});if(settings.gridColumns==='showSupplier'){purchaseColumns.push({id:'price',name:'Price',field:'price',width:130,sortable:true,formatter:Slick.Formatters.Units});}
if(!dobrado.mobile){purchaseColumns.push({id:'total',name:'Total',field:'total',width:120,sortable:true,formatter:Slick.Formatters.Dollar});}
if(purchaseGrid){if($(purchaseGridId).is(':visible')){purchaseGrid.setColumns(purchaseColumns);purchaseGrid.render();}
else{$(purchaseGridId).show();purchaseGrid.setColumns(purchaseColumns);purchaseGrid.render();$(purchaseGridId).hide();}}
let allAvailableColumns=[];if(settings.gridColumns==='combinedFormat'){allAvailableGrid.setOptions({rowHeight:70});allAvailableColumns.push({id:'product',name:'Product',field:'name',width:500,sortable:true,formatter:combinedFormatter});}
else if(settings.gridColumns==='showSupplier'){allAvailableColumns.push({id:'product',name:'Product',field:'name',width:290,sortable:true});if(!dobrado.mobile){allAvailableColumns.push({id:'supplier',name:'Supplier',field:'supplier',width:200,sortable:true,formatter:supplierFormatter});}}
allAvailableColumns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});if(settings.gridColumns==='showSupplier'){allAvailableColumns.push({id:'price',name:'Price',field:'price',width:130,sortable:true,formatter:Slick.Formatters.Units});}
if(!dobrado.mobile){allAvailableColumns.push({id:'total',name:'Total',field:'total',width:120,sortable:true,formatter:Slick.Formatters.Dollar});}
if(allAvailableGrid){$(allAvailableGridId).show();allAvailableGrid.setColumns(allAvailableColumns);allAvailableGrid.render();$(allAvailableGridId).hide();}}
setTimeout(customGridColumns,1000);};}());