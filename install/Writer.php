<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Writer extends Base {

  public function Add($id) {
    // Micropub requests look for a page to write posts to, so make sure
    // there's a default when the writer module is added.
    $mysqli = connect_db();
    $query = 'INSERT INTO writer VALUES ("' . $this->owner . '", ' .
      '"' . $this->user->page . '", "post", "' . $this->user->page . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Add: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'Permission denied editing page.'];
    }

    $mysqli = connect_db();
    $mode = isset($_POST['mode']) ? $mysqli->escape_string($_POST['mode']) : '';
    $action = isset($_POST['action']) ?
      $mysqli->escape_string($_POST['action']) : '';
    $designate = isset($_POST['designate']) ?
      $mysqli->escape_string($_POST['designate']) : '';
    $mysqli->close();

    $key = $this->owner . '-' . $this->user->page;
    if ($mode === 'showWebaction') {
      if (!in_array($action, ['like', 'reply', 'share', 'post'])) {
        return ['error' => 'Action not supported.'];
      }

      // This is used by Designate when a new post module is added. Store owner
      // and page names so that multiple pages can be opened at the same time.
      if (isset($_SESSION['writer-webaction'])) {
        $_SESSION['writer-webaction'][$key] = $action;
      }
      else {
        $_SESSION['writer-webaction'] = [$key => $action];
      }
      return ['done' => true];
    }

    if ($mode === 'removeWebaction') {
      unset($_SESSION['writer-webaction'][$key]);
      $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
        'https://' : 'http://';
      return ['url' => $scheme . $_SERVER['SERVER_NAME'] . $this->Url()];
    }

    if ($mode === 'showDesignate') {
      if (!in_array($action, ['like', 'post', 'reply', 'share', 'payment'])) {
        return ['error' => 'Action not supported.'];
      }
      return ['page' => $this->Designate($action)];
    }

    if ($mode === 'designate') {
      if (!preg_match('/^[a-z0-9_-]{1,200}$/i', $designate)) {
        return ['error' => 'Page name has the wrong format.'];
      }
      if (!in_array($action, ['like', 'post', 'reply', 'share', 'payment'])) {
        return ['error' => 'Action not supported.'];
      }
      $this->Designate($action, $this->user->page, $designate);
      return ['done' => true];
    }

    if ($mode === 'shareFullText') {
      return $this->ShareFullText();
    }

    if ($mode === 'feedTitle') {
      return $this->UpdateFeedTitle();
    }
  }

  public function CanAdd($page) {
    // Can only have one writer module on a page.
    return !$this->AlreadyOnPage('writer', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    if (!$this->user->canViewPage) {
      return '<h1 class="feed-title p-name">' .
        htmlspecialchars($this->FeedTitle()) . '</h1>' . $this->FeedAuthor();
    }
    if (!$this->CanPublish()) return false;

    // Check for webaction parameters in the url.
    list($action, $us_url, $options) = $this->Webaction();
    $writer_settings = '';
    $designate_page = '';
    $webaction_info = '';
    $feed_title = '';
    // When there's a reader module on the page, a feed can't be shown so
    // there's no point setting a title for it.
    if (!$this->AlreadyOnPage('reader')) {
      $feed_title = '<div class="form-spacing">' .
          '<label for="writer-feed-title-input">Feed title</label>' .
          '<input id="writer-feed-title-input" type="text" maxlength="100" ' .
            'value="' . $this->FeedTitle() . '">' .
          '<button class="submit-feed-title">submit</button>' .
        '</div>';
    }
    // Allow the writer module to toggle the visibility of the reader module's
    // settings when on the page, this is just to simplify the UI.
    if ($this->user->canViewPage) {
      $writer_settings = '<button class="writer-edit-settings">' .
        'feed settings</button>';
      $webaction_info = $this->WebactionInfo($action, $us_url);
    }
    if ($this->user->canEditPage) {
      $designate_page = '<div class="hidden designate">' .
          $feed_title .
          '<div class="form-spacing">' .
            '<label for="writer-designate-input">Page for displaying </label>' .
            '<select id="writer-designate-action">' . $options . '</select>' .
            '<input id="writer-designate-input" type="text" maxlength="200" ' .
              'value="' . $this->Designate($action) . '">' .
            '<button class="submit-designate">submit</button>' .
          '</div>' .
        '</div>';
    }
    $author = isset($this->user->settings['micropub']['endpoint']) ? '' :
      '<li id="writer-options-author"><div>Add an author</div></li>';
    return '<div id="writer-webaction-info">' . $webaction_info . '</div>' .
      '<textarea id="writer-content" class="dobrado-editable">' .
        'Write a new post...</textarea>' .
      '<div class="writer-options">' .
        '<button class="writer-options-add" title="add">add</button>' .
        '<div class="writer-menu-wrapper menu-wrapper hidden">' .
          '<div class="arrow-border"></div>' .
          '<div class="arrow"></div>' .
          '<ul class="menu">' .
            '<li id="writer-options-title"><div>Add a title</div></li>' .
            '<li id="writer-options-photo"><div>Add a photo</div></li>' .
            $author .
            '<li id="writer-options-twitter"><div>Send to Twitter</div></li>' .
          '</ul>' .
        '</div>' .
        $writer_settings .
        '<button class="writer-post">Post</button>' .
        '<label for="writer-tags" title="tags">Tags</label>' .
        '<input type="checkbox" id="writer-tags" title="tags">' .
        '<input class="hidden" id="writer-tag-input">' .
      '</div>' .
      '<div class="writer-options-title hidden">' .
        '<button class="writer-remove-title">remove</button>' .
        '<label for="writer-title">Title: </label>' .
        '<input id="writer-title" type="text" maxlength="180">' .
      '</div>' .
      '<div class="writer-options-author hidden">' .
        '<button class="writer-remove-author">remove</button>' .
        '<label for="writer-author">Author: </label>' .
        '<input id="writer-author" value="' . $this->user->name . '" ' .
          'type="text" maxlength="50">' .
      '</div>' .
      '<div class="writer-options-twitter hidden">' .
        '<button class="writer-remove-twitter">remove</button>' .
        '<span class="writer-twitter">Send to Twitter</span>' .
      '</div>' .
      '<div class="writer-options-photo hidden">' .
        '<button class="writer-remove-photo">remove</button>' .
        '<span class="writer-photo">Add photos to display as an album:</span>' .
        '<button id="writer-photo-browse">browse</button>' .
        '<div id="writer-photo-selected"></div>' .
      '</div>' .
      $designate_page;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $mysqli = connect_db();
    $query = 'INSERT INTO writer_settings (user, page, title) SELECT ' .
      '"' . $this->owner . '", "' . $new_page . '", title FROM ' .
      'writer_settings WHERE user = "' . $old_owner . '" AND ' .
      'page = "' . $this->user->page . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Copy: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'Designate' && $count === 3) {
        $action = $p[0];
        $source = $p[1];
        $destination = $p[2];
        return $this->Designate($action, $source, $destination);
      }
      return;
    }
    if ($fn === 'Designate') return $this->Designate($p);
    if ($fn === 'AllDesignated') return $this->AllDesignated($p);
  }

  public function Group() {
    return 'reader-writer';
  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.writer.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.writer.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS writer (' .
      'user VARCHAR(50) NOT NULL,' .
      'page VARCHAR(200) NOT NULL,' .
      'action ENUM("like", "post", "reply", "share", "payment") NOT NULL,' .
      'designate VARCHAR(200),' .
      'PRIMARY KEY(user, page(50), action)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS writer_settings (' .
      'user VARCHAR(50) NOT NULL,' .
      'page VARCHAR(200) NOT NULL,' .
      'title VARCHAR(100),' .
      'PRIMARY KEY(user, page(50))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Install 2: ' . $mysqli->error);
    }
    $mysqli->close();

    // Add style rules to the site_style table.
    $site_style = ['"",".writer .feed-title","display","none"',
                   '"",".writer .feed-author","display","none"',
                   '"",".writer-options","display","none"',
                   '"",".writer-options","position","relative"',
                   '"",".writer-options","width","100%"',
                   '"",".writer-options","padding","10px 0"',
                   '"",".writer-options-title","padding","2px"',
                   '"",".writer-options-author","padding","2px"',
                   '"",".writer-options-twitter","padding","2px"',
                   '"",".writer-options-photo","padding","2px"',
                   '"",".writer-menu-wrapper","opacity",".8"',
                   '"",".writer-menu-wrapper","position","absolute"',
                   '"",".writer-menu-wrapper","left","-3px"',
                   '"",".writer-menu-wrapper","top","30px"',
                   '"",".writer-menu-wrapper","z-index","1"',
                   '"",".writer-edit-settings","float","right"',
                   '"",".writer-edit-settings","margin-left","10px"',
                   '"",".writer-post","float","right"',
                   '"","#writer-content","max-width","100%"',
                   '"","#writer-content","width","500px"',
                   '"","#writer-content","height","100px"',
                   '"",".writer-options label[for=writer-tags]",' .
                     '"margin-left","10px"',
                   '"",".writer-options label[for=writer-tags]","float","none"',
                   '"",".writer .designate","border-top","1px solid #aaaaaa"',
                   '"",".writer .designate","border-bottom",' .
                     '"1px solid #aaaaaa"',
                   '"",".writer .designate","padding","5px"',
                   '"",".writer .designate","margin-bottom","10px"',
                   '"",".writer .designate label","width","10em"',
                   '"",".writer .submit-designate","margin-left","5px"',
                   '"",".writer .submit-feed-title","margin-left","5px"',
                   '"","#writer-feed-title-input","width","394px"',
                   '"","#writer-designate-action-button","width","100px"',
                   '"","#writer-designate-input","width","200px"',
                   '"","#writer-designate-input","margin-left","5px"',
                   '"","#writer-webaction-info","padding-bottom","2px"',
                   '"","#writer-action-type","font-weight","bold"',
                   '"",".writer .info .ui-icon","display","inline-block"',
                   '"",".writer .info","padding","3px"',
                   '"",".writer .info a","text-decoration","none"',
                   '"",".writer .menu","padding","1px"',
                   '"",".writer .menu li","font-weight","normal"',
                   '"",".writer-remove-title","float","left"',
                   '"",".writer label[for=writer-title]","width","4em"',
                   '"",".writer label[for=writer-title]","float","none"',
                   '"",".writer label[for=writer-title]","text-align","right"',
                   '"",".writer label[for=writer-title]","margin-right","5px"',
                   '"",".writer label[for=writer-title]","display",' .
                     '"inline-block"',
                   '"",".writer label[for=writer-author]","width","4em"',
                   '"",".writer label[for=writer-author]","float","none"',
                   '"",".writer label[for=writer-author]","text-align","right"',
                   '"",".writer label[for=writer-author]","margin-right","5px"',
                   '"",".writer label[for=writer-author]","display",' .
                     '"inline-block"',
                   '"",".writer-twitter","margin-left","10px"',
                   '"",".writer-photo","margin","0 5px 0 10px"',
                   '"","#writer-follow","float","right"',
                   '"","#writer-photo-selected","margin","5px 5px 0 40px"',
                   '"","#writer-photo-selected","background-color","#eeeeee"',
                   '"","#writer-photo-selected","border","1px solid #aaaaaa"',
                   '"","#writer-photo-selected","border-radius","2px"',
                   '"","#writer-photo-selected img","margin","5px"',
                   '"","#writer-photo-selected img","height","50px"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['post']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM writer WHERE user = "' . $this->owner . '" ' .
        'AND page = "' . $this->user->page . '"';
    }
    else {
      $query = 'DELETE FROM writer WHERE user = "' . $this->owner . '"';
    }
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Remove 1: ' . $mysqli->error);
    }
    if (isset($id)) {
      $query = 'DELETE FROM writer_settings WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"';
    }
    else {
      $query = 'DELETE FROM writer_settings WHERE user = "' . $this->owner .'"';
    }
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Remove 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    $mysqli = connect_db();
    $query = 'ALTER TABLE writer DROP PRIMARY KEY';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Update 1: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE writer ADD PRIMARY KEY(user, page(50), action)';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Update 2: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE writer_settings DROP PRIMARY KEY';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Update 3: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE writer_settings ADD PRIMARY KEY(user, page(50))';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Update 4: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE writer MODIFY action ' .
      'ENUM("like", "post", "reply", "share", "payment") NOT NULL';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->Update 5: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function UpdateScript($path) {
    // Append dobrado.writer.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.writer.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function Designate($action = '', $source = '', $destination = '') {
    $mysqli = connect_db();
    // When called with only the action parameter, return the destination page
    // for this action and the users current page.
    if ($source === '' && $destination === '') {
      // If the action parameter is also empty, a new post module is being
      // added. Check if the writer-webaction session variable is set,
      // otherwise default action is post when not set.
      if ($action === '') {
        $key = $this->owner . '-' . $this->user->page;
        $action = isset($_SESSION['writer-webaction'][$key]) ?
          $_SESSION['writer-webaction'][$key] : 'post';
      }
      $query = 'SELECT designate, page FROM writer WHERE ' .
        'user = "' . $this->owner . '" AND action = "' . $action . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($writer = $mysqli_result->fetch_assoc()) {
          // Use any designated page for the action when currently on the index
          // page, as this helps posts made by Micropub. Otherwise look for a
          // value set for the current page.
          if ($this->user->page === 'index' ||
              $this->user->page === $writer['page']) {
            $destination = $writer['designate'];
            break;
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Writer->Designate 1: ' . $mysqli->error);
      }
    }
    else {
      // Otherwise setting the designated page for this action.
      $query = 'INSERT INTO writer VALUES ("' . $this->owner . '", ' .
        '"' . $source . '", "' . $action . '", "' . $destination . '") ' .
        'ON DUPLICATE KEY UPDATE designate = "' . $destination . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Writer->Designate 2: ' . $mysqli->error);
      }
      // When a page is designated as a feed, it needs to have a writer module
      // on it so that page.php doesn't mark it up as a permalink page.
      if ($this->CanAdd($destination)) {
        new_module($this->user, $this->owner, 'writer', $destination,
                   $this->Group(), $this->Placement());
      }
    }
    $mysqli->close();
    return $destination === '' ? $this->user->page : $destination;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AllDesignated($ignore) {
    $designated = [];
    $ignore_list = [];
    foreach (explode(',', $ignore) as $page) {
      $ignore_list[] = strtolower(trim($page));
    }
    $mysqli = connect_db();
    $query = 'SELECT designate FROM writer WHERE ' .
      'user = "' . $this->owner . '" AND action != "payment"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($writer = $mysqli_result->fetch_assoc()) {
        $check = strtolower($writer['designate']);
        if ($check !== '' &&
            !in_array($check, $designated) && !in_array($check, $ignore_list)) {
          $designated[] = $writer['designate'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Writer->AllDesignated: ' . $mysqli->error);
    }
    $mysqli->close();
    return $designated;
  }

  private function FeedAuthor() {
    $detail = new Module($this->user, $this->owner, 'detail');
    if (!$detail->IsInstalled()) return '';

    $user_detail = $detail->Factory('User', $this->owner);
    $name = '';
    if ($user_detail['first'] !== '') {
      $name = $user_detail['first'];
    }
    if ($user_detail['last'] !== '') {
      if ($name !== '') $name .= ' ';
      $name .= $user_detail['last'];
    }
    $url = $this->user->config->Secure() ? 'https://' : 'http://';
    $url .= $this->user->config->ServerName();
    if ($this->owner !== 'admin') $url .= '/' . $this->owner;

    $author = '';
    if ($user_detail['thumbnail'] !== '') {
      $author = '<a href="' . $url . '">' . $user_detail['thumbnail'] . '</a> ';
    }
    if ($name !== '') {
      $author .= '<a class="p-name u-url" href="' . $url . '">' . $name .'</a>';
    }
    return '<div class="h-card p-author feed-author">' . $author . '</div>';
  }

  private function FeedTitle() {
    $title = '';
    $mysqli = connect_db();
    $query = 'SELECT title FROM writer_settings WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($writer_settings = $mysqli_result->fetch_assoc()) {
        $title = $writer_settings['title'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Writer->FeedTitle: ' . $mysqli->error);
    }
    $mysqli->close();
    if ($title !== '') return $title;

    // Add a default if title is not set for this page.
    $detail = new Module($this->user, $this->owner, 'detail');
    if ($detail->IsInstalled()) {
      $user_detail = $detail->Factory('User', $this->owner);
      $name = '';
      if ($user_detail['first'] !== '') {
        $name = $user_detail['first'];
      }
      if ($user_detail['last'] !== '') {
        if ($name !== '') $name .= ' ';
        $name .= $user_detail['last'];
      }
      if ($name !== '') return $name . ': ' . $this->user->page;
    }

    return $this->owner . ': ' . $this->user->page;
  }

  private function ShareFullText($us_url = '') {
    if ($us_url === '') $us_url = $_POST['share'];
    $result = parse_hentry($us_url);
    if ($result['content'] !== '') {
      include 'library/HTMLPurifier.auto.php';
      $config = HTMLPurifier_Config::createDefault();
      $purifier = new HTMLPurifier($config);
      // Don't show a title if it's the same as the content.
      $title = $result['title'];
      if ($title !== '') {
        $check_title = trim(preg_replace('/&#x?[0-9A-F]+;|\s/i', '', $title));
        $check_content = trim(preg_replace('/&#x?[0-9A-F]+;|\s/i', '',
                                           strip_tags($result['content'])));
        $check_content = substr($check_content, 0, strlen($check_title));
        if ($check_title === $check_content) $title = '';
        else if (strlen($title) > 100) $title = substr($title, 0, 50) . '...';
        $title = $purifier->purify($title);
      }
      return ['content' => $purifier->purify($result['content']),
              'title' => $title,
              'author' => $purifier->purify($result['author-name']),
              'category' => $purifier->purify($result['category'])];
    }
    return ['error' => 'Could not load full text.'];
  }

  private function UpdateFeedTitle() {
    $mysqli = connect_db();
    $title = $mysqli->escape_string(htmlspecialchars($_POST['title']));
    $query = 'INSERT INTO writer_settings VALUES ("' . $this->owner . '", ' .
      '"' . $this->user->page . '", "' . $title . '") ' .
      'ON DUPLICATE KEY UPDATE title = "' . $title . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Writer->UpdateFeedTitle: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function Webaction() {
    $prefix = '';
    // Set the correct path to include when called from index.php.
    if (strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') !== false) {
      $prefix = $this->owner === 'admin' ? 'php/' : '../php/';
    }
    include $prefix . 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $us_url = '';
    $options = '';

    if (isset($_GET['like'])) {
      $action = 'like';
      $us_url = $purifier->purify($_GET['like']);
      $options = '<option value="like" selected="selected">likes</option>' .
        '<option value="post">posts</option>' .
        '<option value="reply">replies</option>' .
        '<option value="share">shares</option>' .
        '<option value="payment">payments</option>';
    }
    else if (isset($_GET['repost'])) {
      $action = 'share';
      $us_url = $purifier->purify($_GET['repost']);
      $options = '<option value="like">likes</option>' .
        '<option value="post">posts</option>' .
        '<option value="reply">replies</option>' .
        '<option value="share" selected="selected">shares</option>' .
        '<option value="payment">payments</option>';
    }
    else if (isset($_GET['reply'])) {
      $action = 'reply';
      $us_url = $purifier->purify($_GET['reply']);
      $options = '<option value="like">likes</option>' .
        '<option value="post">posts</option>' .
        '<option value="reply" selected="selected">replies</option>' .
        '<option value="share">shares</option>' .
        '<option value="payment">payments</option>';
    }
    else if (isset($_GET['payment'])) {
      $action = 'payment';
      $us_url = $purifier->purify($_GET['payment']);
      $options = '<option value="like">likes</option>' .
        '<option value="post">posts</option>' .
        '<option value="reply">replies</option>' .
        '<option value="share">shares</option>' .
        '<option value="payment" selected="selected">payments</option>';
    }
    else {
      if (isset($_GET['follow']) && $this->AlreadyOnPage('reader')) {
        $action = 'follow';
        $us_url = $purifier->purify($_GET['follow']);
      }
      else {
        $action = 'post';
      }
      $options = '<option value="like">likes</option>' .
        '<option value="post" selected="selected">posts</option>' .
        '<option value="reply">replies</option>' .
        '<option value="share">shares</option>' .
        '<option value="payment">payments</option>';
    }
    // This is used by Designate when a new post module is added.
    if ($action !== 'follow') {
      $key = $this->owner . '-' . $this->user->page;
      if (isset($_SESSION['writer-webaction'])) {
        $_SESSION['writer-webaction'][$key] = $action;
      }
      else {
        $_SESSION['writer-webaction'] = [$key => $action];
      }
    }
    return [$action, $us_url, $options];
  }

  private function WebactionInfo($action, $us_url) {
    $icon = '';
    $button = '';
    if ($action === 'like') {
      $icon = 'ui-icon-star';
    }
    else if ($action === 'reply') {
      $action = 'reply to';
      $icon = 'ui-icon-arrowreturnthick-1-w';
    }
    else if ($action === 'share') {
      $icon = 'ui-icon-refresh';
    }
    else if ($action === 'follow') {
      $icon = 'ui-icon-signal-diag';
      $button = ' <button id="writer-follow">ok</button>';
    }
    else {
      return '';
    }

    // Shorten the url so that it fits in the display area.
    $display = $us_url;
    if (preg_match('/^https?:\/\/(.*)$/', $display, $match)) {
      $display = $match[1];
    }
    if (strlen($display) > 63) $display = substr($display, 0, 60) . '...';
    return '<button id="writer-remove-action">remove</button>' .
      '<span class="info ui-state-highlight ui-corner-all">' .
        '<span class="ui-icon ' . $icon . '"></span>' .
        '<span id="writer-action-type">' . $action . '</span> ' .
        '<a href="' . $us_url . '" id="writer-action-url">' .
          $display . '</a>?' .
      '</span>' . $button;
  }

}
