<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Registration extends Base {

  public function Add($id) {
    $selector = '#dobrado-' . $id . ' .registration-form';
    $box_style =
      ['"","' . $selector . ' label","width","12em"',
       '"","' . $selector . ' .submit","margin-left","11em"',
       '"","' . $selector . '","background-color","#eeeeee"',
       '"","' . $selector . '","border","1px solid #aaaaaa"',
       '"","' . $selector . '","border-radius","2px"',
       '"","' . $selector . '","padding","5px"',
       '"","' . $selector . ' .edit-info","padding","5px"',
       '"","' . $selector . ' .edit-info","margin","5px"',
       '"","' . $selector . ' .edit-info","background-color","#ffffff"',
       '"","' . $selector . ' .edit-info","border","2px dashed #333333"',
       '"","' . $selector . ' .username-check","margin-left","12em"',
       '"","' . $selector . ' .password-check","margin-left","12em"',
       '"","' . $selector . ' .registration-info","margin-left","12em"'];
    $this->AddBoxStyle($box_style);

    $organiser = new Organiser($this->user, $this->owner);
    $organisations = $organiser->ShowOrganisations();
    $select = '';
    if (count($organisations) > 0) {
      $select = '<div class="form-spacing">' .
        '<label for="registration-organisation">Organisation:</label>' .
        '<select id="registration-organisation">';
      for ($i = 0; $i < count($organisations); $i++) {
        $select .= '<option value="' . $organisations[$i] . '">' .
          ucfirst($organisations[$i]) . '</option>';
      }
      $select .= '</select></div>';
      // Also create selects for the groups for each organisation.
      for ($i = 0; $i < count($organisations); $i++) {
        $groups = $organiser->Children($organisations[$i]);
        if (count($groups) > 0) {
          $select .= '<div class="form-spacing hidden">' .
            '<label for="' . $organisations[$i] . '-groups">Groups:</label>' .
            '<select id="' . $organisations[$i] . '-groups">';
          for ($j = 0; $j < count($groups); $j++) {
            $select .= '<option value="' . $groups[$j] . '">' .
              ucfirst($groups[$j]) . '</option>';
          }
          $select .= '</select></div>';
        }
      }
    }
    $content = '<form class="registration-form">' .
      '<div class="edit-info"><h4>Editing your registration form:</h4>' .
        'Multiple organisations can be displayed here by creating a select ' .
        'with id <b>registration-organisation</b>. To display the groups ' .
        'associated with each organisation, create a select for each with ' .
        'id\'s like <b>&lt;organisation&gt;-groups</b> and list the groups ' .
        'as options. Give them a class name of <b>hidden</b>, as they will ' .
        'be displayed when the value of <b>registration-organisation</b> ' .
        'changes. Note that the <i>values</i> of options in each select must ' .
        'match groups in the system, but it\'s ok to change their ' .
        'descriptive names.<br><i>(By default all organisations and groups ' .
        'are displayed... if you need a new group listed add it via the ' .
        'Organiser module.)</i>' .
      '</div>' .
      $select .
      '<div class="form-spacing">' .
        '<label for="registration-username">Username:</label>' .
        '<input id="registration-username" type="text" maxlength="50">' .
      '</div>' .
      '<div class="username-check hidden"></div>' .
      '<div class="form-spacing">' .
        '<label for="registration-password">Password:</label>' .
        '<input id="registration-password" type="password" maxlength="200">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="registration-password-confirm">Confirm Password:</label>' .
        '<input id="registration-password-confirm" type="password" ' .
          'maxlength="200">' .
      '</div>' .
      '<div class="password-check hidden"></div>' .
      '<div class="form-spacing">' .
        '<label for="registration-email">Email:</label>' .
        '<input id="registration-email" type="text" maxlength="100">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="registration-first">First Name:</label>' .
        '<input id="registration-first" type="text" maxlength="50">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="registration-last">Last Name:</label>' .
        '<input id="registration-last" type="text" maxlength="50">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="registration-phone">Phone Number:</label>' .
        '<input id="registration-phone" type="text" maxlength="50">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="registration-address">Address:</label>' .
        '<textarea id="registration-address"></textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="registration-description">Description:</label>' .
        '<textarea id="registration-description"></textarea>' .
      '</div>' .
      '<button class="submit">submit</button><br>' .
      '<span class="registration-info hidden">' .
        'ckeditor removes this span if empty</span>' .
      '</form>';
    $mysqli = connect_db();
    $this->Insert($id, $mysqli->escape_string($content));
    $mysqli->close();
  }

  public function Callback() {
    // 'mode' is used by the Extended module, which calls this function.
    $us_mode = isset($_POST['mode']) ? $_POST['mode'] : '';
    if ($us_mode === 'box') {
      $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
      return ['source' => $this->PlainContent($id), 'editor' => true];
    }
    if ($us_mode === 'check') {
      $mysqli = connect_db();
      $username = $mysqli->escape_string(strtolower($_POST['username']));
      $mysqli->close();
      $user = new User($username);
      list($available, $can_update) = username_available($user, $this->owner);
      if ($available === true) {
        return ['check' => 'This username is available.'];
      }
      if ($available === false) {
        return ['check' => 'There was a problem checking your username.'];
      }
      return ['check' => $available];
    }
    if ($us_mode === 'submit') {
      $mysqli = connect_db();
      $username = $mysqli->escape_string(strtolower($_POST['username']));
      $password = $mysqli->escape_string($_POST['password']);
      $email = $mysqli->escape_string(strtolower($_POST['email']));
      $organisation = isset($_POST['organisation']) ?
        $mysqli->escape_string(strtolower($_POST['organisation'])) : '';
      $group = isset($_POST['group']) ?
        $group = $mysqli->escape_string(strtolower($_POST['group'])) : '';
      if ($group === '') {
        // Need a default group if a moderator is used to confirm new accounts.
        $us_group = $this->Substitute('registration-default-group');
        $group = $mysqli->escape_string($us_group);
      }
      $mysqli->close();

      if ($organisation !== '') {
        $organiser = new Organiser($this->user, $this->owner);
        if (!in_array($group, $organiser->Children($organisation))) {
          return ['content' => 'This registration form is not configured ' .
                  'correctly.<br>Please contact an administrator.'];
        }
      }
      if ($email === '') {
        return ['content' => 'Please provide an email address.'];
      }

      $user = new User($username, $group);
      $status = new_user($user, $this->owner, 1, $email, 0, $password);
      if ($status === true) {
        $detail = new Module($this->user, $this->owner, 'detail');
        if ($detail->IsInstalled()) {
          $mysqli = connect_db();
          $first = isset($_POST['first']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['first'])) : '';
          $last = isset($_POST['last']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['last'])) : '';
          $phone = isset($_POST['phone']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['phone'])) : '';
          $address = isset($_POST['address']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['address'])) : '';
          $description = isset($_POST['description']) ?
           $mysqli->escape_string(htmlspecialchars($_POST['description'])) : '';
          $mysqli->close();
          $display = $description === '' ? 0 : 1;
          $detail->Factory('UpdateUser',
                           [$username, $first, $last, $phone, $address,
                            $description, $display, true]);
        }
        return ['content' => 'Thanks! An email has been sent to the ' .
                'address provided.'];
      }
      if ($status === false) {
        return ['content' => 'There was a problem creating your account.<br>' .
                'Please try again.'];
      }
      return ['content' => $status];
    }
  }

  public function CanAdd($page) {
    // Must have admin access to add the registration module.
    return $this->user->canEditSite;
  }

  public function CanEdit($id) {
    // Must have admin access to edit the registration module.
    return $this->user->canEditSite;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<div class="dobrado-editable">' . $this->PlainContent($id) .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $this->Insert($id, $this->PlainContent($old_id, $old_owner, true));
    $this->CopyStyle($id, $old_owner, $old_id);
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS registration (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'content TEXT,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Registration->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS registration_history (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'content TEXT,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'modified_by VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, box_id, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Registration->Install 2: ' . $mysqli->error);
    }

    $mysqli->close();

    // Append dobrado.registration.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.registration.js', true);

    $site_style = ['"","#registration-description","width","400px"',
                   '"","#registration-description","height","80px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM registration WHERE user = "' . $this->owner . '" ' .
        'AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Registration->Remove 1: ' . $mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM registration WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Registration->Remove 2: ' . $mysqli->error);
      }
      $query = 'DELETE FROM registration_history WHERE ' .
        'user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Registration->Remove 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    if ($us_content['data'] === $this->PlainContent($id)) return;

    $time = time();
    $mysqli = connect_db();
    $data = $mysqli->escape_string($us_content['data']);
    $query= 'UPDATE registration SET content = "' . $data . '", ' .
      'timestamp = ' . $time . ' WHERE user = "' . $this->owner . '" AND ' .
      'box_id = ' . $id;
    if (!$mysqli->query($query)) {
      $this->Log('Registration->SetContent 1: ' . $mysqli->error);
    }

    $query = 'INSERT INTO registration_history VALUES ' .
      '("' . $this->owner . '", ' . $id . ', "' . $data . '", ' .
      $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Registration->SetContent 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.registration.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.registration.js', true);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Insert($id, $content = '') {
    $time = time();
    $mysqli = connect_db();
    $query = 'INSERT INTO registration VALUES ' .
      '("' . $this->owner . '", ' . $id . ', "' . $content . '", ' . $time .')';
    if (!$mysqli->query($query)) {
      $this->Log('Registration->Insert 1: ' . $mysqli->error);
    }

    $query = 'INSERT INTO registration_history VALUES ' .
      '("' . $this->owner . '", ' . $id . ', "' . $content . '", ' .
      $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Registration->Insert 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function PlainContent($id, $user = '', $escape = false) {
    if ($user === '') {
      $user = $this->owner;
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT content FROM registration WHERE user = "' . $user . '" ' .
      'AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($registration = $mysqli_result->fetch_assoc()) {
        $content = $escape ? $mysqli->escape_string($registration['content']) :
          $registration['content'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Registration->PlainContent: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

}
