// @source: /js/source/dobrado.start.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.start){dobrado.start={};}
(function(){'use strict';$(function(){if($('.start > .content').length===0){return;}
$('.start > .content').dialog({show:true,width:600,height:350,position:{my:'top',at:'top+50',of:window},title:'Getting started',create:dobrado.fixedDialog,close:close});$.post('/php/request.php',{request:'start',action:'tooltip',url:location.href,token:dobrado.token},function(response){var tooltip=JSON.parse(response);if(tooltip){dobrado.tooltip(tooltip.selector,tooltip.content,tooltip.arrow);}});});function close(){var id=$('.start').attr('id');if(!id||$('#start-show-message:checked').length===1){return;}
$.post("/php/remove.php",{id:'#'+id,label:'start',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"start close")){return;}});}}());