<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Section extends Base {

  public function Add($id) {

  }

  public function Callback() {
    // mode is used by the Extended module, which calls this function.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
      return ['source' => $this->PlainContent($id), 'editor' => true,
              'custom' => '<form id="extended-custom-settings"></form>'];
    }

    // Custom tab should have buttons for "add above" and "add below",
    // remove section, and tag, which should expand to show all current tags
    // and allow editing.

    // - move sections to previous/next page (if not the top/bottom section,
    //   get the user to confirm that X section will be moved...)
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    // - A section will have a way to show tags via ckeditor.
    // - It will start with the most specific tags, and allow toggling of
    //   highlighted text that has also been tagged.
    // - Should also have a way to show less specific tags without opening
    //   the whole project, ie if first if there are grouped module tags and
    //   then page level tags. Finally project level tags. After that allow
    //   opening the project dialog. (Which could open in context,
    //   ie highlighting the current section on the current page in the current
    //   project...)
    // - should have options to expand to the extended editor
    // - also options to add new sections: below, above or split at cursor.
    // - if the text in a section changes need to check if there are any
    //   'quotes' in the parent project for this section.
    //   - need to be able to ask the project for them, (which just means their
    //     start and length).
    //   - if this substring in the old text is the same in the new text,
    //     can ignore this change.
    //   - if this substring has changed, ask the user to re-select the quote
    //     from the text. (if they choose not to, remove the old quote tags)
    return '<div class="dobrado-editable">'.$this->PlainContent($id).'</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    // Need to alert the parent project that the section is being copied.
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {
    return 'section-group';
  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS section ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'project_id INT UNSIGNED NOT NULL,'.
      'content TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'PRIMARY KEY(user, box_id)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Section->Install 1: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS section_history ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'project_id INT UNSIGNED NOT NULL,'.
      'content TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'modified_by VARCHAR(50) NOT NULL,'.
      'PRIMARY KEY(user, box_id, timestamp)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Section->Install 2: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS section_tag ('.
      'project_id INT UNSIGNED NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'content VARCHAR(200) NOT NULL,'.
      'quote_start INT UNSIGNED,'.
      'quote_length INT UNSIGNED,'.
      'PRIMARY KEY(project_id, box_id, content)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Section->Install 3: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM section WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Section->Remove 1: '.$mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM section WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Section->Remove 2: '.$mysqli->error);
      }
      $query = 'DELETE FROM section_history WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Section->Remove 3: '.$mysqli->error);
      }
    }
    $mysqli->close();

    // TODO: Need to alert the parent project that the section is being removed.
  }

  public function SetContent($id, $us_content) {
    if ($us_content['data'] === $this->PlainContent($id)) return;

    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $purifier = new HTMLPurifier($config);
    $us_data = $purifier->purify($us_content['data']);

    $time = time();
    $mysqli = connect_db();
    $data = $mysqli->escape_string($us_data);
    $project_id = 0;
    // A new project will provide the project_id when creating sections.
    if (isset($us_content['project_id'])) {
      $project_id = $mysqli->escape_string($us_content['project_id']);
    }
    else {
      $query = 'SELECT project_id FROM section WHERE user = "'.$this->owner.'"'.
        ' AND box_id = '.$id;
      if ($result = $mysqli->query($query)) {
        if ($section = $result->fetch_assoc()) {
          $project_id = (int)$section['project_id'];
        }
        $result->close();
      }
      else {
        $this->Log('Section->SetContent 1: '.$mysqli->error);
      }
    }
    if ($project_id !== 0) {
      $query= 'INSERT INTO section VALUES ("'.$this->owner.'", '.$id.', '.
        $project_id.', "'.$data.'", '.$time.') ON DUPLICATE KEY UPDATE '.
        'project_id = '.$project_id.', content = "'.$data.'", timestamp = '.
        $time;
      if (!$mysqli->query($query)) {
        $this->Log('Section->SetContent 2: '.$mysqli->error);
      }
      $query = 'INSERT INTO section_history VALUES ("'.$this->owner.'", '.$id.
        ', '.$project_id.', "'.$data.'", '.$time.', "'.$this->user->name.'")';
      if (!$mysqli->query($query)) {
        $this->Log('Section->SetContent 3: '.$mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript.
  }

  // Public functions that aren't part of interface here /////////////////////

  public function ListProject($id) {
    $section_list = [];
    $mysqli = connect_db();
    $query = 'SELECT box_id, content FROM section WHERE project_id = '.$id.
      ' ORDER BY box_id';
    if ($result = $mysqli->query($query)) {
      while ($section = $result->fetch_assoc()) {
        $section_list[] = ['id' => $section['box_id'],
                           'tags' => $this->Tags($id, $section['box_id']),
                           'content' => $section['content']];
      }
      $result->close();
    }
    else {
      $this->Log('Section->ListProject: '.$mysqli->error);
    }
    $mysqli->close();
    return $section_list;
  }

  public function SaveTag($content, $project_id, $us_selected_id) {
    $mysqli = connect_db();
    $query = '';
    if (count($us_selected_id) === 0) {
      // When no sections are selected the tag applies to all sections.
      $query = 'SELECT box_id FROM section WHERE project_id = '.$project_id;
      if ($result = $mysqli->query($query)) {
        $query = '';
        while ($section = $result->fetch_assoc()) {
          if ($query !== '') $query .= ', ';
          $query .= '('.$project_id.', '.$section['box_id'].', '.
            '"'.$content.'", 0, 0)';
        }
        $result->close();
      }
      else {
        $this->Log('Section->SaveTag 1: '.$mysqli->error);
      }
    }
    else {
      foreach($us_selected_id as $us_id) {
        // Remove the 'project-section-' prefix from the id.
        $id = (int)substr($us_id, 16);
        if ($query !== '') $query .= ', ';
        $query .= '('.$project_id.', '.$id.', "'.$content.'", 0, 0)';
      }
    }
    if ($query !== '' &&
        !$mysqli->query('INSERT INTO section_tag VALUES '.$query)) {
      $this->Log('Section->SaveTag 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  // Private functions below here ////////////////////////////////////////////

  private function PlainContent($id, $user = '', $escape = false) {
    if ($user === '') {
      $user = $this->owner;
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT content FROM section WHERE user = "'.$user.'" AND '.
      'box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($section = $result->fetch_assoc()) {
        $content = $escape ? $mysqli->escape_string($section['content']) :
          $section['content'];
      }
      $result->close();
    }
    else {
      $this->Log('Section->PlainContent: '.$mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function Tags($project_id, $section_id) {
    $tags = [];
    $mysqli = connect_db();
    $query = 'SELECT content, quote_start, quote_length FROM section_tag '.
      'WHERE project_id = '.$project_id.' AND box_id = '.$section_id;
    if ($result = $mysqli->query($query)) {
      while ($section_tag = $result->fetch_assoc()) {
        $tags[] = ['content' => $section_tag['content'],
                   'quote_start' => (int)$section_tag['quote_start'],
                   'quote_length' => (int)$section_tag['quote_length']];
      }
      $result->close();
    }
    else {
      $this->Log('Section->Tags: '.$mysqli->error);
    }
    $mysqli->close();
    return $tags;
  }

}
