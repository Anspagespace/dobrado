<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Hosting extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view hosting.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'list') return $this->ListDomains();
    if ($us_action === 'startSetup') return $this->StartSetup();
    if ($us_action === 'finishSetup') return $this->FinishSetup();
  }

  public function CanAdd($page) {
    return !$this->AlreadyOnPage('hosting', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<div id="hosting-info"></div>' .
      '<button class="setup-start hidden">Start</button>' .
      '<button class="setup-finish hidden">Continue</button>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.hosting.js');
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.hosting.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function FinishSetup() {
    $domain = $_POST['domain'];
    $email = '';
    $verification = '';
    $confirmed = NULL;
    foreach ($this->ListDomains() as $hosting) {
      if ($hosting['domain'] === $domain) {
        $email = $hosting['email'];
        $verification = $hosting['verification'];
        $confirmed = $hosting['confirmed'] === 1;
        break;
      }
    }
    if (!isset($confirmed)) return ['error' => 'Domain not listed.'];
    if ($confirmed) return ['error' => 'Hosting already set up.'];

    $lets_encrypt = '/etc/letsencrypt/live/' . $domain;
    $https_content = "\n" .
      '<VirtualHost *:443>' . "\n" .
      '  ServerName ' . $domain . "\n" .
      '  DocumentRoot /var/www/' . $domain . "\n" .
      '  RewriteEngine On' . "\n" .
      '  RewriteOptions Inherit' . "\n" .
      '  SSLEngine On' . "\n" .
      '  SSLCertificateFile ' . $lets_encrypt . '/cert.pem' . "\n" .
      '  SSLCertificateKeyFile ' . $lets_encrypt . '/privkey.pem' . "\n" .
      '  SSLCACertificateFile ' . $lets_encrypt . '/fullchain.pem' . "\n" .
      '</VirtualHost>' . "\n\n" .
      '<VirtualHost *:443>' . "\n" .
      '  ServerName www.' . $domain . "\n" .
      '  DocumentRoot /var/www/' . $domain . "\n" .
      '  RewriteEngine On' . "\n" .
      '  RewriteOptions Inherit' . "\n" .
      '  SSLEngine On' . "\n" .
      '  SSLCertificateFile ' . $lets_encrypt . '/cert.pem' . "\n" .
      '  SSLCertificateKeyFile ' . $lets_encrypt . '/privkey.pem' . "\n" .
      '  SSLCACertificateFile ' . $lets_encrypt . '/fullchain.pem' . "\n" .
      '</VirtualHost>' . "\n";
    file_put_contents('/etc/apache2/sites-available/default.conf',
                      $https_content, FILE_APPEND);

    $cron = $this->Substitute('hosting-cron');
    $content = '0 * * * * cd /var/www/' . $domain . '/php; php cron.php' . "\n";
    file_put_contents($cron, $content, FILE_APPEND);
    $output[] = 'Reloading Cron.';
    exec('crontab ' . $cron . ' 2>&1', $output);
    $output[] = 'Continue setup using the following link:';
    $link = 'https://' . $domain . '/deploy.php?email=' . urlencode($email) .
      '&verification=' . urlencode($verification);

    $domaincheck = new Module($this->user, $this->owner, 'domaincheck');
    if ($domaincheck->IsInstalled()) $domaincheck->Factory('Confirm', $domain);
    return ['output' => $this->FormatOutput($output) .
                        '<a href="' . $link . '">' . $link . '</a>'];
  }

  private function FormatOutput($output) {
    $result = '';
    foreach ($output as $content) {
      $result .= '<p>' . htmlspecialchars($content) . '</p>';
    }
    return $result;
  }

  private function ListDomains() {
    $domaincheck = new Module($this->user, $this->owner, 'domaincheck');
    if ($domaincheck->IsInstalled()) {
      return $domaincheck->Factory('ListDomains');
    }
    return ['error' => 'Domaincheck is not installed'];
  }

  private function StartSetup() {
    $domain = $_POST['domain'];
    $confirmed = NULL;
    foreach ($this->ListDomains() as $hosting) {
      if ($hosting['domain'] === $domain) {
        $confirmed = $hosting['confirmed'] === 1;
        break;
      }
    }
    if (!isset($confirmed)) return ['error' => 'Domain not listed.'];
    if ($confirmed) return ['error' => 'Hosting already set up.'];

    $dir = '/var/www/' . $domain;
    if (is_dir($dir)) return ['error' => 'Webserver directory already exists.'];
    if (!mkdir($dir)) return ['error' => 'Webserver directory not created.'];
    if (!file_exists('deploy.php')) {
      return ['error' => 'deploy.php not found in the current directory.'];
    }
    if (!copy('deploy.php', $dir . '/deploy.php')) {
      return ['error' => 'deploy.php was not copied to the websever ' .
                         'directory.'];
    }

    include 'functions/db_config.php';

    $output = ['Creating MySQL Database.'];
    exec('mysqladmin -h ' . $db_server . ' -u ' . $db_user .
           ' -p' . $db_password . ' create ' . $domain . ' 2>&1', $output);

    $http_content = "\n" .
      '<VirtualHost *:80>' . "\n" .
      '  ServerName ' . $domain . "\n" .
      '  DocumentRoot /var/www/' . $domain . "\n" .
      '  RewriteEngine On' . "\n" .
      '  RewriteOptions Inherit' . "\n" .
      '</VirtualHost>' . "\n\n" .
      '<VirtualHost *:80>' . "\n" .
      '  ServerName www.' . $domain . "\n" .
      '  DocumentRoot /var/www/' . $domain . "\n" .
      '  RewriteEngine On' . "\n" .
      '  RewriteOptions Inherit' . "\n" .
      '</VirtualHost>' . "\n";
    file_put_contents('/etc/apache2/sites-available/default.conf',
                      $http_content, FILE_APPEND);

    $email = $this->Substitute('hosting-email');
    $commands = 'sudo service apache2 reload' . "\n" .
      'sudo certbot certonly --apache --agree-tos --email ' . $email .
        ' -n -d ' . $domain . ' -d www.' . $domain . "\n" .
      'sudo service apache2 reload';
    $command_file = $this->Substitute('hosting-commands');
    if ($command_file === '') {
      $output[] = 'Commands file path not set. You need to run: ' . $commands;
    }
    else {
      $output[] = 'You need to run commands on the server to continue.';
      file_put_contents($command_file, $commands);
    }
    return ['output' => $this->FormatOutput($output)];
  }

}
