<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Summary extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'Permission denied viewing summary.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'view') {
      $start = (int)$_POST['start'] / 1000;
      $start = strtotime(date('F j Y 00:00:00', $start));
      $end = (int)$_POST['end'] / 1000;
      $end = strtotime(date('F j Y 23:59:59', $end));
      if ($start === 0 || $end === 0) {
        return ['error' => 'start and end dates required'];
      }

      $us_select = $_POST['select'];
      if ($us_select === 'purchase') {
        $purchase = new Purchase($this->user, $this->owner);
        return ['data' => $purchase->Data($start, $end, true),
                'surcharge' => $purchase->Surcharge($start, $end)];
      }
      if ($us_select === 'sold') {
        $purchase = new Purchase($this->user, $this->owner);
        return ['data' => $purchase->Sold($start, $end)];
      }
      if ($us_select === 'payment') {
        $payment = new Payment($this->user, $this->owner);
        return ['data' => $payment->Data($start, $end)];
      }
    }
    if ($us_action === 'change') {
      $mysqli = connect_db();
      $group = $mysqli->escape_string($_POST['group']);
      $mysqli->close();
      $invite = new Invite($this->user, $this->owner);
      if ($group === $this->user->group ||
          in_array($group, $invite->OpenBuyingGroups())) {
        $_SESSION['purchase-group'] = $group;
        $_SESSION['purchase-group-changed'] = true;
        return ['done' => true];
      }
      return ['error' => 'Purchase group is not available.'];
    }
  }

  public function CanAdd($page) {
    // Can only have one summary module on a page.
    return !$this->AlreadyOnPage('summary', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    if (!$this->user->canViewPage) return false;

    $detail = new Detail($this->user, $this->owner);
    $user_details = $detail->User();
    $name = '';
    if (isset($user_details['first'])) $name = $user_details['first'];
    // If the user hasn't filled out their details just display their username.
    if ($name === '') {
      $name = $this->user->name;      
    }

    $roster = new Roster($this->user, $this->owner);
    $volunteer_text = $roster->Description();

    $outstanding_text = '';
    if ($this->Substitute('summary-display-outstanding') === 'true') {
      $purchase = new Purchase($this->user, $this->owner);
      $purchase_total = $purchase->Total();

      $payment = new Payment($this->user, $this->owner);
      $payment_total = $payment->Total();
      $outstanding = $purchase_total - $payment_total;
      // Ignore small balances, up to ten cents (only for display purposes).
      if ($outstanding > 0.1) {
        $outstanding_text = 'You currently owe: $' .
          number_format($outstanding, 2, '.', '');
        $timestamp = $payment->MostRecent();
        if ($timestamp !== 0 && time() > $timestamp + 86400) {
          $outstanding_text .= $this->Substitute('summary-balance', '/!date/',
                                                 date('j F Y', $timestamp));
        }
      }
      else if ($outstanding < -0.1) {
        $outstanding_text = 'You are currently in credit by: $' .
          number_format($outstanding * -1, 2, '.', '');
      }
    }

    return '<p>Hello ' . $name . ',<br>' .
      $volunteer_text . '<br>' . $outstanding_text . '</p>' .
      '<p class="ordered-items">' . $this->Orders() . '</p>' .
      '<p class="total"></p>' .
      '<div class="view-previous">' .
        '<a href="#">View previous transactions.</a>' .
        '<form id="summary-form" class="hidden">' .
          '<div class="form-spacing">' .
            '<label for="summary-view-select">View:</label>' .
            '<select id="summary-view-select">' .
              '<option value="purchase">purchases</option>' .
              '<option value="payment">payments</option>' .
              '<option value="sold">sold</option>' .
            '</select>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="summary-start-input">Start:</label>' .
            '<input id="summary-start-input" type="text" maxlength="50" ' .
              'readonly="true">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="summary-end-input">End:</label>' .
            '<input id="summary-end-input" type="text" maxlength="50" ' .
              'readonly="true">' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    // Copy is called when a new user is created (assuming a Summary module
    // has been added to the group's default page). The Summary module depends
    // on both the Purchase and Payment modules, they need to be told about
    // the new user here.
    $purchase = new Purchase($this->user, $this->owner);
    $purchase->NewUser();
    $payment = new Payment($this->user, $this->owner);
    $payment->NewUser();
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.summary.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.summary.js', false);
    $site_style = ['"",".summary a","color","#222222"',
                   '"",".summary a","text-decoration","none"',
                   '"",".summary a:hover","color","#aaaaaa"',
                   '"","#summary-form","background-color","#eeeeee"',
                   '"","#summary-form","border","1px solid #aaaaaa"',
                   '"","#summary-form","border-radius","2px"',
                   '"","#summary-form","padding","5px"',
                   '"","#summary-form label","width","5em"'];
    $this->AddSiteStyle($site_style);

    $template = ['"summary-pre-order","","from !open !open-time until ' .
                   '!final !final-time."',
                 '"summary-display-outstanding","","true"',
                 '"summary-balance","","<br>(Payments were last ' .
                   'processed on !date)"',
                 '"summary-available-orders","","Select a group:"'];
    $this->AddTemplate($template);
    $description = ['summary-pre-order' => 'Descriptive text for the ' .
                      'summary page specifying when pre-orders are ' .
                      'finalised. Will replace !final with ' .
                      '\'pre-order-final\' substitution.',
                    'summary-display-outstanding' => 'The string true or ' .
                      'false, used for calculating and displaying any ' .
                      'outstanding amount on the summary page.',
                    'summary-balance' => 'Descriptive text to display after ' .
                      'a negative balance. Substitutes !date for when ' .
                      'payments were last processed.'];
    $this->AddTemplateDescription($description);

    // Check module dependenices.
    $dependencies = ['detail', 'invite', 'payment', 'purchase', 'roster'];
    return $this->Dependencies($dependencies);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.summary.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Orders() {
    // The order text for the default group is always returned, it's either the
    // only group available or added as the first option in the select.
    $order_text = $this->OrderText();
    // Find the open buying groups that this user has been invited to.
    $invite = new Invite($this->user, $this->owner);
    $groups = $invite->OpenBuyingGroups();
    $count = count($groups);
    if ($count === 0) {
      return $order_text;
    }

    // Store the users default group to reset at the end.
    $default_group = $this->user->group;
    // When there's more than one group, return a select as well as all the
    // order text for each of the groups. Also hide the order text except for
    // the currently selected purchase group.
    $hidden = ' hidden';
    $selected = '';
    $purchase_group = '';
    if (isset($_SESSION['purchase-group'])) {
      $purchase_group = $_SESSION['purchase-group'];
    }
    // If there is no selected purchase group show the user's default group.
    if ($purchase_group === $this->user->group || $purchase_group === '') {
      $selected = ' selected="selected"';
      $hidden = '';
    }
    $order_text = '<div class="summary-order summary-group-' .
      $this->user->group . $hidden . '">' . $order_text . '</div>';
    $select = '<label for="summary-available-orders">' .
        $this->Substitute('summary-available-orders') . '</label>' .
      '<select id="summary-available-orders">' .
        '<option value="' . $this->user->group . '"' . $selected . '>' .
          $this->Substitute('group-name') .
        '</option>';
    for ($i = 0; $i < $count; $i++) {
      $name = $groups[$i];
      $this->user->group = $name;
      $hidden = ' hidden';
      $selected = '';
      if ($purchase_group === $name) {
        $selected = ' selected="selected"';
        $hidden = '';
      }
      $select .= '<option value="' . $name . '"' . $selected . '>' .
        $this->Substitute('group-name') . '</option>';
      $order_text .= '<div class="summary-order summary-group-' . $name .
        $hidden . '">' . $this->OrderText() . '</div>';
    }
    $select .= '</select>';
    $this->user->group = $default_group;
    return $select . $order_text;
  }

  private function OrderText() {
    $order_text = $this->Substitute('group-description');
    $pre_order_available = $this->Substitute('pre-order') === 'true';
    $purchase_page = $this->Substitute('purchase-page');
    // If FancyUrl is true, starting the query string here, otherwise contd.
    $purchase_page .= $this->user->config->FancyUrl() ? '?' : '&';
    $purchase_page .= 'order=true';
    $pre_order_text =
      $this->Substitute('summary-pre-order',
                        ['/!open-day/', '/!open-time/', '/!final-day/',
                         '/!final-time/'],
                        [$this->Substitute('pre-order-open'),
                         $this->Substitute('pre-order-open-time'),
                         $this->Substitute('pre-order-final'),
                         $this->Substitute('pre-order-final-time')]);
    $purchase = new Purchase($this->user, $this->owner);
    // Don't tell people they have items ordered when purchasing is currently
    // available, as it gets confusing.
    if (!$purchase->PurchasingAvailable()) {
      if (count($purchase->Data(strtotime('tomorrow'))) === 0) {
        if ($pre_order_available) {
          $order_text .= '<p id="summary-pre-order">You haven\'t placed ' .
            'an order yet.<br>You can <b><a class="purchase-page-link" ' .
            'href="' . $purchase_page . '">order here</a></b> ' .
            $pre_order_text . '.</p>';
        }
      }
      else {
        $order_text .= '<p id="summary-pre-order">You have items ordered.';
        if ($pre_order_available) {
          $order_text .= '<br>You can <b><a class="purchase-page-link" ' .
            'href="' . $purchase_page . '">change them here</a></b> ' .
            $pre_order_text . '.';
        }
        $order_text .= '</p>';
      }
    }
    else if ($pre_order_available) {
      $order_text .= '<p id="summary-pre-order">You can ' .
        '<b><a class="purchase-page-link" href="' . $purchase_page .
        '">order here</a></b> ' . $pre_order_text . '.</p>';
    }
    return $order_text;
  }

}
