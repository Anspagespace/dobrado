// @source: /js/source/dobrado.usb.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.usb){dobrado.usb={};}
(function(){'use strict';$(function(){if($('.usb').length===0){return;}
$('#usb-create').button().click(create);});function create(){var address=$('#usb-input').val();if(address===''){$('#usb-info').html('');return false;}
var bookmarklet="(function(){document.querySelectorAll"+"('form').forEach(function(form){var input=form.querySelectorAll"+"('input:not([type=button]):not([type=submit]):not([type=checkbox])"+":not([type=hidden])');if(input.length===1&&/login|log.in|sign.in|"+"address/.test(form.innerHTML.toLowerCase())){input[0].value='"+
address+"';form.querySelectorAll('input[type=button],input"+"[type=submit],button').forEach(function(button){button.click();});"+"}});}());";var encoded="%28function%28%29%7Bdocument.querySelectorAll"+"%28%27form%27%29.forEach%28function%28form%29%7Bvar%20input%3D"+"form.querySelectorAll%28%27input%3Anot%28%5Btype%3Dbutton%5D%29%3A"+"not%28%5Btype%3Dsubmit%5D%29%3Anot%28%5Btype%3Dcheckbox%5D%29%3Anot"+"%28%5Btype%3Dhidden%5D%29%27%29%3Bif%28input.length%3D%3D%3D1%26%26%2F"+"login%7Clog.in%7Csign.in%7Caddress%2F.test%28form.innerHTML."+"toLowerCase%28%29%29%29%7Binput%5B0%5D.value%3D%27"+
encodeURIComponent(address)+"%27%3Bform.querySelectorAll%28%27input"+"%5Btype%3Dbutton%5D%2Cinput%5Btype%3Dsubmit%5D%2Cbutton%27%29.forEach"+"%28function%28button%29%7Bbutton.click"+"%28%29%3B%7D%29%3B%7D%7D%29%3B%7D%28%29%29%3B";$('#usb-info').html('<p>Save this link to your bookmarks: '+'<a href="javascript:'+bookmarklet+'">Sign In</a>'+'</p><p id="usb-encoded">Note that the javascript in '+'the link above has not been url encoded, as it '+'appears browsers are doing a better job managing '+'this now. If you have any trouble with it you can '+'try this encoded version: <a href="javascript:'+
encoded+'">Sign In</a></p>');return false;}}());