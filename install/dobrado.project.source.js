/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.project) {
  dobrado.project = {};
}
(function() {

  'use strict';

  var needsSaving = false;
  // Store the current project details.
  var current = {};
  // Store the id's of currently selected sections.
  var selectedID = [];
  var projectDialogScrollMax = 0;

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.project').length === 0) {
      return;
    }

    $('.project').dialog({
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Projects',
      width: 720,
      height: 500,
      create: dobrado.fixedDialog,
      beforeClose: function() {
        if (needsSaving && !confirm('Close project without saving?\n' +
                                    'Your changes will be lost.')) {
          return false;
        }
      },
      close: function() {
        if ($('#project-section-dialog').dialog('instance')) {
          $('#project-section-dialog').dialog('destroy');
        }
        $('.project').remove();
      },
      resizeStop: function() {
        projectDialogScrollMax = $('.project').prop('scrollHeight') -
          $('.project').dialog('option', 'height');
      }
    });
    // TODO: updateSelectable might be useful if we want to limit the number
    // selectable sections to make the UI more responsive. 
    //$('.project').scroll(updateSelectable);
    init();
  });

  function init() {
    $('#project-new').button().click(newProject);
    $('#project-all').button().click(allProjects);
    $('#project-save').button({ disabled: true }).click(saveProject);
    $('#project-list').accordion({ heightStyle: 'content' });
    $('.project-open').button().click(openProject);
    $('#project-name-edit').checkboxradio({ icon: false }).click(function() {
      $('#project-name').html($('#project-name-input').val()).toggle();
      $('#project-name-input').toggle();
    });
    $('#project-import-input').change(upload);
    $('#project-show-filters').checkboxradio().click(function() {
      $('#project-filters').toggle();
    });
    $('#project-add-filter').change(function() {
      $('.project-filter-description').hide();
      $('#project-filter-description-' + $(this).val()).show();
    });
    $('#project-apply-filter').button().click(applyFilter);
    $('#project-section-editor').button().click(openSectionEditor);
    $('#project-form').find(':input').change(function() {
      $('#project-save').button('enable');
      needsSaving = true;
    });
    selectedID = [];
  }

  function applyFilter() {
    var value = $('#project-add-filter').val();
    if (value === 'none') {
      return false;
    }

    dobrado.log('Applying filter.', 'info');
    $.post('/php/request.php', { request: 'project',
                                 action: 'applyFilter',
                                 filter: value,
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        $('#project-add-filter').val('none');
        if (dobrado.checkResponseError(response, 'applyFilter')) {
          return;
        }
        var result = JSON.parse(response);
        var filter = result.filter;
        var newFilter = '<div class="project-current-filter">' +
          '<button id="project-remove-filter-' + value + '">remove filter' +
          '</button>' +
          '<span "project-filter-name">' + filter.name + '</span></div>';
        if ($('.project-current-filter').length === 0) {
          if ($('#current-filters-heading').length === 0) {
            $('<p id="current-filters-heading">The current filters ' +
              'that have been applied:</p>').prependTo('#project-filters');
          }
          $(newFilter).insertAfter($('#current-filters-heading'));
        }
        else {
          $(newFilter).insertAfter($('.project-current-filter').last());
        }
        $('#project-remove-filter-' + value).button({
          icon: 'ui-icon-closethick',
          showLabel: false
        }).click(removeFilter);

        if (result.project) {
          current = result.project;
          updateProjectInfo();
        }
      });
    return false;
  }

  function allProjects() {
    if (needsSaving && !confirm('Exit project without saving?\n' +
                                'Your changes will be lost.')) {
      return false;
    }

    // Reset needsSaving since the user has confirmed it's ok.
    needsSaving = false;
    dobrado.log('Listing projects.', 'info');
    if ($('#project-section-dialog').dialog('instance')) {
      $('#project-section-dialog').dialog('destroy');
    }
    $.post('/php/request.php', { request: 'project',
                                 action: 'allProjects',
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'allProjects')) {
          return;
        }
        var project = JSON.parse(response);
        $('#project-content').html(project.content);
        init();
      });
  }

  function newProject() {
    dobrado.log('Creating new project.', 'info');
    $.post('/php/request.php', { request: 'project',
                                 action: 'newProject',
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'newProject')) {
          return;
        }
        var project = JSON.parse(response);
        $('#project-content').html(project.content);
        current = {};
        init();
      });
  }

  function openProject() {
    dobrado.log('Opening project.', 'info');
    $.post('/php/request.php', { request: 'project',
                                 action: 'openProject',
                                 id: $(this).attr('id'),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'openProject')) {
          return;
        }
        var project = JSON.parse(response);
        $('#project-content').html(project.content);
        current = { id: project.id, tags: project.tags,
                    sections: project.sections };
        init();
        showDetails();
      });
  }

  function openSectionEditor() {

    function saveTag() {
      dobrado.log('Saving tag.', 'info');
      var content = $('#project-new-tag').val();
      $.post('/php/request.php', { request: 'project',
                                   action: 'saveTag',
                                   projectID: current.id,
                                   selectedID: JSON.stringify(selectedID),
                                   content: content,
                                   url: location.href,
                                   token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'saveTag')) {
            return;
          }
          if (selectedID.length === 0) {
            current.tags.push({ 'content': content });
            showTags(current.tags);
            // The tag is added to all sections too.
            $.each(current.sections, function(i, section) {
              section.tags.push({ 'content': content });
            });
          }
          else if (selectedID.length === 1) {
            $.each(current.sections, function(i, section) {
              if (selectedID[0] === 'project-section-' + section.id) {
                section.tags.push({ 'content': content });
                showTags(section.tags);
                return false;
              }
            });
          }
          else {
            $.each(current.sections, function(i, section) {
              var sectionID = 'project-section-' + section.id;
              if ($.inArray(sectionID, selectedID) !== -1) {
                section.tags.push({ 'content': content });
              }
            });
            showTags(sharedTags());
          }
          $('#project-new-tag').val('');
        });
    }

    // Need to load the same selection dialog as individual sections use,
    // except that we also want to handle multiple selections. For instance,
    // need to register the selection editor toolbar for updates when
    // selections are selected or unselected. (ie can disable add above/below
    // when more than one selection is selected...)
    // ---> doesn't make sense to do inserts here, because we can't edit the
    //      content anyway (ie would just be inserting empty sections). might
    //      as well leave inserts for in-page edits so that content can be added
    if (!$('#project-section-dialog').dialog('instance')) {
      $('#project-section-dialog').dialog({
        position: { my: 'top', at: 'top+50', of: $(this) },
        title: 'Section Editor',
        width: 400,
        height: 400,
        create: dobrado.fixedDialog
      });
      $('#project-save-tag').button().click(saveTag);
      showTags(current.tags);
    }
    else if ($('#project-section-dialog').dialog('isOpen')) {
      $('#project-section-dialog').dialog('moveToTop');
    }
    else {
      $('#project-section-dialog').dialog('open');
    }
  }

  function removeFilter() {
    dobrado.log('Removing filter.', 'info');
    var filter = $(this).attr('id');
    $.post('/php/request.php', { request: 'project',
                                 action: 'removeFilter',
                                 filter: filter,
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'removeFilter')) {
          return;
        }
        $('#' + filter).parent().remove();
        var project = JSON.parse(response);
        if (project.sections) {
          current = project;
          updateProjectInfo();
        }
      });
    return false;
  }

  function saveProject() {
    dobrado.log('Saving project.', 'info');
    $.post('/php/request.php', { request: 'project',
                                 action: 'saveProject',
                                 name: $('#project-name-input').val(),
                                 description: $('#project-description').val(),
                                 project: JSON.stringify(current),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'saveProject')) {
          return;
        }
        var project = JSON.parse(response);
        var info = project.info;
        $.post('/php/request.php', { request: 'project',
                                     action: 'openProject',
                                     id: 'project-open-' + project.id,
                                     url: location.href,
                                     token: dobrado.token },
          function(response) {
            if (dobrado.checkResponseError(response, 'saveProject open')) {
              return;
            }
            project = JSON.parse(response);
            $('#project-content').html(project.content);
            current = { id: project.id, tags: project.tags,
                        sections: project.sections };
            init();
            showDetails();
            $('#project-info').html(info);
            $('#project-save').button('disable');
            needsSaving = false;
          });
      });
  }

  function sharedTags() {
    var oldTags = [];
    var newTags = [];
    $.each(current.sections, function(i, section) {
      var sectionID = 'project-section-' + section.id;
      if ($.inArray(sectionID, selectedID) !== -1) {
        // Start the tags array as all the tags from the first section, and
        // then remove tags that aren't shared by other selected sections.
        if (oldTags.length === 0) {
          oldTags = section.tags;
        }
        else {
          $.each(oldTags, function(j, currentTag) {
            // Look for current tag in the new section's list of tags.
            $.each(section.tags, function(k, checkTag) {
              if (checkTag.content === currentTag.content) {
                newTags.push(currentTag);
                return false;
              }
            });
          });
          // Switching oldTags and newTags arrays avoids having to remove tags
          // from a single list (which causes problems when trying to remove
          // them while iterating).
          oldTags = newTags;
          newTags = [];
        }
        // Stop processing if the shared tag list drops to zero.
        if (oldTags.length === 0) {
          return false;
        }
      }
    });
    return oldTags;
  }

  function showDetails() {
    if (current.filename && $('#project-name').html() == 'New Project') {
      // Create a project name from the file name. Note that the full path is
      // returned here, and the upload process converts spaces to underscores.
      var matches = current.filename.match(/([^/]+)\.odt$/);
      if (matches.length > 1) {
        var name = matches[1].replace(/_/g, ' ');
        $('#project-name').html(name);
        $('#project-name-input').val(name);
      }
    }
    var sectionList = '';
    $.each(current.sections, function(i, section) {
      if (section.content.indexOf('class="project-page-break"') !== -1) {
        sectionList += '<li id="project-section-' + section.id + '" ' +
          'class="project-page-break">' + section.content + '</li>';
      }
      else {
        sectionList += '<li id="project-section-' + section.id + '">' +
          section.content + '</li>';
      }
    });
    $('#project-sections').html(sectionList).selectable({
      selected: function(event, ui) {
        var id = $(ui.selected).attr('id');
        if (id && $.inArray(id, selectedID) === -1) {
          selectedID.push(id);
        }
      },
      stop: function() {
        if ($('#project-section-dialog').dialog('instance')) {
          $('#project-section-dialog').dialog('moveToTop');          
          if (selectedID.length === 0) {
            $('label[for=project-new-tag]').html('Add a <b>project</b> tag:');
            showTags(current.tags);
          }
          else if (selectedID.length === 1) {
            $('label[for=project-new-tag]').html('Add a tag to <b>1</b>' +
                                                 ' section:');
            $.each(current.sections, function(i, section) {
              if (selectedID[0] === 'project-section-' + section.id) {
                showTags(section.tags);
                return false;
              }
            });
          }
          else {
            $('label[for=project-new-tag]').html('Add a tag to <b>' +
                                                 selectedID.length +
                                                 '</b> sections:');
            showTags(sharedTags());
          }
        }
        else {
          if (selectedID.length === 0) {
            $('#project-info').html('');
          }
          else if (selectedID.length === 1) {
            $.each(current.sections, function(i, section) {
              if (selectedID[0] === 'project-section-' + section.id) {
                var info = '';
                if (section.tags.length !== 0) {
                  if (section.tags.length === 1) {
                    info = '<b>1</b> tag created for section ';
                  }
                  else {
                    info = '<b>' + section.tags.length +
                      '</b> tags created for section ';
                  }
                  info += '<b>' + section.id + '</b>: ';
                  $.each(section.tags, function(j, tag) {
                    if (j !== 0) {
                      info += ', ';
                    }
                    info += tag.content;
                  });
                }
                $('#project-info').html(info);
                return false;
              }
            });
          }
          else {
            var tags = sharedTags();
            var info = '';
            if (tags.length !== 0) {
              if (tags.length === 1) {
                info = '<b>1</b> tag created that is shared by ';
              }
              else {
                info = '<b>' + tags.length + '</b> tags created that are ' +
                  'shared by ';
              }
              info += '<b>' + selectedID.length + '</b> sections : ';
              $.each(tags, function(i, tag) {
                if (i !== 0) {
                  info += ', ';
                }
                info += tag.content;
              });
            }
            $('#project-info').html(info);
          }
        }
      },
      unselected: function(event, ui) {
        var id = $(ui.unselected).attr('id');
        if (id && $.inArray(id, selectedID) !== -1) {
          $.each(selectedID, function(i, item) {
            if (item === id) {
              selectedID.splice(i, 1);
              return false;
            }
          });
        }
      }
    });
    // Once selectable has been enabled add 'ui-widget-content' to each
    // section. (doing it before this adds the class to child elements as well)
    $('#project-sections > li').each(function() {
      $(this).addClass('ui-widget-content');
    });
  }

  function showTags(tags) {

    function removeTag() {

    }

    var content = '';
    if (tags.length !== 0) {
      if (tags.length === 1) {
        content = '<b>1</b>' + ' tag applies:<br>';
      }
      else {
        content = '<b>' + tags.length + '</b>' + ' tags apply:<br>';
      }
      $.each(tags, function(i, tag) {
        content += '<div><button class="project-remove-tag">remove' +
          '</button><span>' + tag.content + '</div>';
      });
    }
    $('#project-tags').html(content);
    $('.project-remove-tag').button({
      icon: 'ui-icon-closethick',
      showLabel: false
    }).click(removeTag);
  }

  function updateProjectInfo() {
    selectedID = [];
    if (current.sections.length === 0) {
      $('#project-info').html('<i>Processing returned no sections.</i>');
      $('#project-sections').html('');
      return;
    }

    if (current.sections.length === 1) {
      $('#project-info').html('Processing returned <b>1</b> section.');
    }
    else {
      $('#project-info').html('Processing returned <b>' +
                              current.sections.length + '</b> sections.');
    }
    showDetails();
  }

  function updateSelectable() {
    // The project dialog scroll height isn't available until it's created.
    if (projectDialogScrollMax === 0) {
      projectDialogScrollMax = $('.project').prop('scrollHeight') -
        $('.project').dialog('option', 'height');
    }
    if ($('.project').scrollTop() >= projectDialogScrollMax) {
      // TODO: When the project dialog scrolls to the bottom, update which
      // sections are selectable. Note that we can't do this when scrolling
      // to the top because we wouldn't be able to reach the project controls.
      // (Instead show info about which sections are currently displayed and
      // offer to reset.)
      console.log('reached the bottom');
    }
  }

  function upload() {
    var formData = new FormData();
    if (!formData) {
      dobrado.log("Your browser doesn't support file uploading.");
      return;
    }

    dobrado.log('Uploading file...', 'info');
    formData.append('upload', $('#project-import-input').get(0).files[0]);
    formData.append('request', 'project');
    formData.append('action', 'upload');
    formData.append('url', location.href);
    formData.append('token', dobrado.token);
    $.ajax({
      url: '/php/request.php',
      data: formData,
      contentType: false,
      processData: false,
      type: 'POST',
      success: function(response) {
        if (dobrado.checkResponseError(response, 'project upload')) {
          return;
        }
        current = JSON.parse(response);
        updateProjectInfo();
      }
    });
    return false;
  }

})();
