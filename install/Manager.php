<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Manager extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view manager'];
    }

    $action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($action === 'list') return $this->AllProducts();
    if ($action === 'search') return $this->Search();
    if ($action === 'submit') return $this->AddPurchase();
    if ($action === 'remove') return $this->RemovePurchase();
    if ($action === 'loadProducts') return $this->AvailableProducts();
    if ($action === 'savePurchase') return $this->AddToComposite();
    if ($action === 'removePurchase') return $this->RemoveFromComposite();
    if ($action === 'changeGroup') return $this->ChangeGroup();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    // Need admin privileges to add the manager module.
    if (!$this->user->canEditSite) return false;
    // Can only have one manager module on a page.
    return !$this->AlreadyOnPage('manager', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $taxable_input = '';
    $taxable = $this->Substitute('stock-taxable');
    if ($taxable !== '') {
      $not_taxable = $this->Substitute('stock-not-taxable');
      if ($not_taxable === '') $not_taxable = 'Not ' . $taxable;
      $taxable_input = '<div class="form-spacing">' .
          '<label for="manager-taxable-select">Filter:</label>' .
          '<select id="manager-taxable-select">' .
            '<option value="all">Return all results</option>' .
            '<option value="taxable">' . $taxable . '</option>' .
            '<option value="not-taxable">' . $not_taxable . '</option>' .
          '</select>' .
        '</div>';
    }
    $group_select = '';
    $default_group = $this->user->group;
    $purchase_group = isset($_SESSION['purchase-group']) ?
      $_SESSION['purchase-group'] : '';
    // Display a group select if this user has created invite groups.
    $invite = new Invite($this->user, $this->owner);
    $created = $invite->Created();
    if (count($created) > 0) {
      if (in_array($purchase_group, $created)) {
        $this->user->group = $purchase_group;
      }
      $group_select .= '<p class="manager-display-group">' .
        'Displaying purchases for ' .
        '<select id="manager-group-select">' .
          '<option value="' . $default_group . '">' .
            $this->Substitute('group-name', '', '', $default_group) .
          '</option>';
      foreach ($created as $group) {
        if ($group === $this->user->group) {
          $group_select .=
            '<option selected="selected" value="' . $group . '">' .
              $this->Substitute('group-name') . '</option>';
        }
        else {
          $group_select .= '<option value="' . $group . '">' .
            $this->Substitute('group-name', '', '', $group) . '</option>';
        }
      }
      $group_select .= '</select></p>';
    }
    $this->user->group = $default_group;

    return $group_select . '<form id="manager-form" autocomplete="off">' .
        '<button class="default-action hidden">default</button>' .
        '<a href="#" class="import-toggle">Show import form.</a>' .
        '<div class="import hidden">' .
          '<div class="form-spacing">' .
            '<label for="manager-import-file">Import File:</label>' .
            '<input id="manager-import-file" type="file">' .
          '</div>' .
          '<div class="info"></div>' .
        '</div><hr>' .
        '<div class="form-spacing">' .
          '<label for="manager-username-input">Username:</label>' .
          '<input id="manager-username-input" type="text" maxlength="50">' .
        '</div>' .
        '<hr>' .
        '<div class="form-spacing">' .
          '<label for="manager-product-input">Product:</label>' .
          '<input id="manager-product-input" type="text" maxlength="100">' .
          '<button class="view-all">View all</button>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="manager-supplier-input">Supplier:</label>' .
          '<input id="manager-supplier-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="manager-quantity-input">Quantity:</label>' .
          '<input id="manager-quantity-input" type="text" maxlength="8">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="manager-price-input">Price:</label>' .
          '<input id="manager-price-input" type="text" maxlength="50">' .
          '<span class="manager-price-info"></span>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="manager-date-input">Date:</label>' .
          '<input id="manager-date-input" type="text" maxlength="50">' .
        '</div>' .
        $taxable_input .
        '<hr>' .
        '<a class="toggle-search-options" href="#">' .
          'Search between start and end dates</a><br>' .
        '<div class="search-options hidden">' .
          '<div class="form-spacing">' .
            '<label for="manager-start-date-input">Start:</label>' .
            '<input id="manager-start-date-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="manager-end-date-input">End:</label>' .
            '<input id="manager-end-date-input" type="text" maxlength="50">' .
          '</div>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="manager-group-input" type="checkbox"> ' .
          '<label for="manager-group-input">Group results</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="manager-export-data" type="checkbox"> ' .
          '<label for="manager-export-data">Download search results</label>' .
        '</div>' .
        '<button class="submit">submit</button>' .
        '<button class="search">search</button>' .
        '<button class="back hidden">go back</button>' .
        '<button class="remove">remove</button>' .
      '</form>' .
      '<div class="search-info"></div>' .
      '<div class="manager-view-all-dialog hidden">' .
        '<div class="manager-view-all-total"></div>' .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.purchase.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.manager.js', false);
    $site_style = ['"","#manager-form","background-color","#eeeeee"',
                   '"","#manager-form","border","1px solid #aaaaaa"',
                   '"","#manager-form","border-radius","2px"',
                   '"","#manager-form","padding","5px"',
                   '"","#manager-form label","width","6em"',
                   '"","#manager-form .submit","float","right"',
                   '"","#manager-form .search","float","right"',
                   '"","#manager-form .search","margin-right","10px"',
                   '"","#manager-quantity-input","height","20px"',
                   '"","#manager-quantity-input","width","100px"',
                   '"","label[for=manager-group-input]","float","none"',
                   '"","label[for=manager-export-data]","float","none"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['banking', 'invite', 'purchase', 'stock']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.manager.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddPurchase() {
    $result = [];
    $joined = [];
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    // Check that the user being updated is in a matching system_group
    // to the current user.
    if ($organiser->MatchUser($username, $joined)) {
      $timestamp = (int)$_POST['timestamp'] / 1000;
      $product = $mysqli->escape_string($_POST['product']);
      $supplier = $mysqli->escape_string($_POST['supplier']);
      $quantity = $mysqli->escape_string($_POST['quantity']);
      $price = $mysqli->escape_string($_POST['price']);
      $base_price = $mysqli->escape_string($_POST['basePrice']);
      $purchase = new Purchase($this->user, $this->owner);
      $result = $purchase->AddPurchase($username, $timestamp, $product,
                                       $supplier, $quantity, $price,
                                       $base_price, 0, $this->user->name);
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function AddToComposite() {
    $joined = [];
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    if ($organiser->MatchUser($username, $joined)) {
      $timestamp = (int)$_POST['timestamp'] / 1000;
      $product = $mysqli->escape_string($_POST['product']);
      $supplier = $mysqli->escape_string($_POST['supplier']);
      $quantity = $mysqli->escape_string($_POST['quantity']);
      $price = $mysqli->escape_string($_POST['price']);
      $base_price = $mysqli->escape_string($_POST['basePrice']);
      $composite = $mysqli->escape_string($_POST['composite']);
      $purchase = new Purchase($this->user, $this->owner);
      if ($composite === '') {
        // Adding a normal purchase.
        $result = $purchase->AddPurchase($username, $timestamp, $product,
                                         $supplier, $quantity, $price,
                                         $base_price, 0, $this->user->name);
      }
      else {
        // Editing a composite item.
        $result = $purchase->EditComposite($username, $composite,
                                           $product, $quantity);
      }
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function ChangeGroup() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $mysqli->close();

    $invite = new Invite($this->user, $this->owner);
    if ($group === $this->user->group || in_array($group, $invite->Created())) {
      $_SESSION['purchase-group'] = $group;
      $_SESSION['purchase-group-changed'] = true;
      return ['done' => true];
    }
    return ['error' => 'Group not found.'];
  }

  private function AllProducts() {
    $result = [];
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    $invite = new Invite($this->user, $this->owner);
    $created = in_array($this->user->group, $invite->Created());
    // Lists of users and products are made to autocomplete and verify input.
    $banking = new Banking($this->user, $this->owner);
    list($result['users'], $result['buyerGroup']) =
      $banking->AllBuyers(true, true, $created);

    $stock = new Stock($this->user, $this->owner);
    $result['products'] = $stock->AllProducts(true, $this->user->group);
    // Need wholesale and retail percent to calculate base price for variably
    // priced products.
    $result['wholesalePercent'] =
      (float)$this->Substitute('stock-wholesale-percent');
    $result['retailPercent'] = (float)$this->Substitute('stock-retail-percent');
    // A default date can be used for importing orders.
    $timestamp = strtotime($this->Substitute('manager-date'));
    if ($timestamp) $result['date'] = $timestamp * 1000;
    $this->user->group = $default_group;
    return $result;
  }

  private function AvailableProducts() {
    $result = [];
    $joined = [];
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    if ($organiser->MatchUser($username, $joined)) {
      $composite = $_POST['composite'] === 'true';
      $product = $mysqli->escape_string($_POST['product']);
      $timestamp = (int)$_POST['timestamp'] / 1000;
      $stock = new Stock($this->user, $this->owner);
      $purchase = new Purchase($this->user, $this->owner);
      $result['available'] = $stock->AvailableProducts();
      $result['current'] = $purchase->CompositeData($composite, $username,
                                                    $product, $timestamp);
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function RemoveFromComposite() {
    $result = [];
    $joined = [];
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    if ($organiser->MatchUser($username, $joined)) {
      $timestamp = (int)$_POST['timestamp'] / 1000;
      $product = $mysqli->escape_string($_POST['product']);
      $supplier = $mysqli->escape_string($_POST['supplier']);
      $quantity = $mysqli->escape_string($_POST['quantity']);
      $composite = $mysqli->escape_string($_POST['composite']);
      $purchase = new Purchase($this->user, $this->owner);
      if ($composite === '') {
        // Removing a normal purchase.
        $result = $purchase->RemovePurchase($username, $timestamp, $product,
                                            $supplier, $quantity);
      }
      else {
        // Editing a composite item.
        $result = $purchase->EditComposite($username, $composite,
                                           $product, $quantity);
      }
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function RemovePurchase() {
    $result = [];
    $joined = [];
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    if ($organiser->MatchUser($username, $joined)) {
      $product = $mysqli->escape_string($_POST['product']);
      $supplier = $mysqli->escape_string($_POST['supplier']);
      $quantity = $mysqli->escape_string($_POST['quantity']);
      $timestamp = (int)$_POST['timestamp'] / 1000;
      $purchase = new Purchase($this->user, $this->owner);
      $result = $purchase->RemovePurchase($username, $timestamp, $product,
                                          $supplier, $quantity);
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function Search() {
    $result = [];
    $mysqli = connect_db();
    // Purchase->Search won't return any data if the system_group doesn't
    // match the current user, so don't need to check for matches first.
    $username = $mysqli->escape_string($_POST['username']);
    $product = $mysqli->escape_string($_POST['product']);
    $supplier = $mysqli->escape_string($_POST['supplier']);
    $quantity = $mysqli->escape_string($_POST['quantity']);
    $price = $mysqli->escape_string($_POST['price']);
    $taxable = $mysqli->escape_string($_POST['taxable']);
    $mysqli->close();

    $timestamp = (int)$_POST['timestamp'] / 1000;
    $start = (int)$_POST['start'] / 1000;
    $end = (int)$_POST['end'] / 1000;
    $group = (bool)$_POST['group'];
    $exportData = (bool)$_POST['exportData'];
    // The option to filter based on tax status is not always displayed.
    if ($taxable === '') $taxable = 'all';
    $purchase = new Purchase($this->user, $this->owner);
    $result['search'] = $purchase->Search($username, $timestamp, $product,
                                          $supplier, 0, $quantity, $price,
                                          $start, $end, $group, $taxable, true);
    if ($exportData && count($result['search']) > 0) {
      $filename = 'manager-' . date('Y-m-d') . '.csv';
      $header = NULL;
      if ($this->Substitute('purchase-export-units') === 'true' &&
          $this->Substitute('purchase-export-description') === 'true') {
        // This is a special case for Dreamcatcher's preferred export format.
        $header = ['number', 'user', 'name', 'supplier', 'quantity', 'unit',
                   'category'];
      }
      $this->CreateCSV($filename, $result['search'], true, '', $header);
      $result['filename'] = $filename;
    }
    return $result;
  }

}
