<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Project extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->loggedIn) {
      return array('error' => 'Permission denied.');
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'applyFilter') {
      $filter = (int)$_POST['filter'];
      if (in_array($filter, $_SESSION['project-filters'])) {
        return array('error' => 'Filter already applied.');
      }
      if (!isset($this->all_filters[$filter])) {
        return array('error' => 'Filter not found.');
      }
      $_SESSION['project-filters'][] = $filter;
      if (isset($_SESSION['project-filename'])) {
        $path = $_SESSION['project-filename'];
        return array('filter' => $this->all_filters[$filter],
                     'project' => $this->Odt2Html($path));
      }
      else {
        return array('filter' => $this->all_filters[$filter]);
      }
    }
    if ($us_action === 'allProjects') {
      $content = '<button id="project-new">create new project</button>'.
        '<div id="project-list">';
      foreach ($this->AllProjects() as $project) {
        $content .= $this->FormatListing($project);
      }
      // Close project-list div.
      $content .= '</div>';
      return array('content' => $content);
    }
    if ($us_action === 'newProject') {
      unset($_SESSION['project-filename']);
      $_SESSION['project-filters'] = array();
      $content = '';
      if (count($this->AllProjects()) !== 0) {
        $content .= '<button id="project-all">show all projects</button>';
      }
      $content .= $this->ShowProject();
      return array('content' => $content);
    }
    if ($us_action === 'openProject') {
      $content = '<button id="project-all">show all projects</button>';
      // This removes the 'project-open-' prefix from the id.
      $id = (int)substr($_POST['id'], 13);
      list($owner, $name, $description, $timestamp, $access) =
        $this->Access($id);
      if (!$access) {
        $content .= $this->ShowProject($id);
        return array('content' => $content, 'id' => 0, 'tags' => array(),
                     'sections' => array());
      }

      $section = new Section($this->user, $this->owner);
      $section_list = $section->ListProject($id);
      $content .= $this->ShowProject($id, count($section_list));
      return array('content' => $content, 'id' => $id,
                   'tags' => $this->Tags($id),
                   'sections' => $section_list);
    }
    if ($us_action === 'removeFilter') {
      // This removes 'project-remove-filter-' from the value.
      $filter = (int)substr($_POST['filter'], 22);
      foreach ($_SESSION['project-filters'] as $key => $value) {
        if ($filter === $value) {
          unset($_SESSION['project-filters'][$key]);
          break;
        }
      }
      if (isset($_SESSION['project-filename'])) {
        return $this->Odt2Html($_SESSION['project-filename']);
      }
      else {
        return array('done' => true);
      }
    }
    if ($us_action === 'saveProject') {
      return $this->SaveProject();
    }
    if ($us_action === 'saveTag') {
      return $this->SaveTag();
    }
    if ($us_action === 'upload') {
      return $this->UploadFile();
    }
    return array('error' => 'Unknown action.');
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    unset($_SESSION['project-filename']);
    $_SESSION['project-filters'] = array();
    $project_list = $this->AllProjects();
    // project-content wrapper is used so we can update the content of the
    // dialog from a callback after it's been created.
    $content = '<div id="project-content">';
    if (count($project_list) === 0) {
      $content .= $this->ShowProject();
    }
    else {
      $content .= '<button id="project-new">create new project</button>'.
        '<div id="project-list">';
      foreach ($project_list as $project) {
        $content .= $this->FormatListing($project);
      }
      // Close project-list div.
      $content .= '</div>';
    }
    // Close project-content div.
    return $content.'</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.project.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS project ('.
      'user VARCHAR(50) NOT NULL,'.
      'name VARCHAR(200),'.
      'filename VARCHAR(200),'.
      'description TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'project_id INT UNSIGNED NOT NULL AUTO_INCREMENT,'.
      'PRIMARY KEY(project_id)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Project->Install 1: '.$mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS project_access ('.
      'project_id INT UNSIGNED NOT NULL,'.
      'visitor VARCHAR(50),'.
      'PRIMARY KEY(project_id, visitor)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Project->Install 2: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS project_tag ('.
      'project_id INT UNSIGNED NOT NULL,'.
      'content VARCHAR(200) NOT NULL,'.
      'PRIMARY KEY(project_id, content)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Project->Install 3: '.$mysqli->error);
    }
    $mysqli->close();

    $site_style = array('"",".project","display","none"',
                        '"","#project-save","float","right"',
                        '"","#project-title","padding","10px"',
                        '"","label[for=project-name-edit]","margin-right",'.
                          '"20px"',
                        '"","label[for=project-show-filters]","float","none"',
                        '"","#project-name","font-size","1.3em"',
                        '"","#project-name-input","font-size","1.3em"',
                        '"","#project-name-input","width","400px"',
                        '"","#project-import-info","padding","10px"',
                        '"","#project-details","padding","5px"',
                        '"","#project-details","background-color","#eeeeee"',
                        '"","#project-details","border","1px solid #aaaaaa"',
                        '"","#project-details","border-radius","2px"',
                        '"","#project-details a","text-decoration","none"',
                        '"","#project-details a","color","#1f8ba6"',
                        '"","#project-details a:hover","color","#2cc6ed"',
                        '"","#project-description","width","98%"',
                        '"","#project-description","height","80px"',
                        '"","#project-sections","list-style-type","none"',
                        '"","#project-sections","padding-left","0px"',
                        '"","#project-sections","clear","both"',
                        '"","#project-sections > li","margin","5px"',
                        '"","#project-sections > li","min-height","10px"',
                        '"","#project-sections > li","padding","0 10px"',
                        '"","#project-sections > li.ui-selecting",'.
                          '"background","#efefef"',
                        '"","#project-sections > li.ui-selected",'.
                          '"background","#dedede"',
                        '"","#project-sections > li.project-page-break",'.
                          '"margin-top","70px"',
                        '"","#project-sections > li.project-page-break",'.
                          '"border-top-style","dashed"',
                        '"","#project-section-info","padding","5px"',
                        '"","#project-section-editor","float","right"',
                        '"","#project-section-editor","margin","5px"',
                        '"","#project-new-tag","width","100%"',
                        '"","#project-new-tag","height","60px"',
                        '"","#project-save-tag","float","right"',
                        '"","#project-tags","clear","both"',
                        '"",".project-navigation","text-decoration","none"',
                        '"",".project-navigation","color","#1f8ba6"',
                        '"",".project-navigation:hover","color","#2cc6ed"',
                        '"",".project-next-page","float","right"');
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(array('section'));
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.project.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  // NOTE: This private member array should be used to store all available
  // rules. When a new rule is added here, or a name is changed, a
  // corresponding entry is required in ApplyFilters that actually makes the
  // changes to sections currently being processed.
  private $all_filters = array(
    array('name' => 'Remove empty sections', 'description' =>
          'Remove empty sections when they\'re found by the converter.'),
    array('name' => 'Combine small sections', 'description' =>
          'Combine a section with the previous one when it\'s less than 50 '.
          'characters. (Does not combine headings.)'),
    array('name' => 'Heading starts new section', 'description' =>
          'Only start a new section when a heading is found.'),
    array('name' => 'Add headings as tags', 'description' =>
          'The text of the heading will be added to the section\'s tag list.'),
    array('name' => 'Page break starts new page', 'description' =>
          'When a page break is found in the document, start a new web page.')
  );

  private function Access($id) {
    $owner = '';
    $name = '';
    $description = '';
    $timestamp = 0;
    $access = true;

    $mysqli = connect_db();
    $query = 'SELECT user, name, description, timestamp FROM project WHERE '.
      'project_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($project = $result->fetch_assoc()) {
        $owner = $project['user'];
        $name = $project['name'];
        $description = $project['description'];
        $timestamp = (int)$project['timestamp'];
      }
      $result->close();
    }
    else {
      $this->Log('Project->Access 1: '.$mysqli->error);
    }
    // Check the current user has access if they're not the owner.
    if ($this->user->name !== $owner) {
      $query = 'SELECT visitor FROM project_access WHERE project_id = '.$id.
        ' AND visitor = "'.$this->user->name.'"';
      if ($result = $mysqli->query($query)) {
        $access = $result->num_rows !== 0;
        $result->close();
      }
      else {
        $this->Log('Project->Access 2: '.$mysqli->error);
      }
    }
    $mysqli->close();
    return array($owner, $name, $description, $timestamp, $access);
  }

  private function ApplyFilters(&$sections, $count) {
    foreach ($_SESSION['project-filters'] as $filter) {
      if (!isset($sections[$count - 1])) continue;

      $name = $this->all_filters[$filter]['name'];
      if ($name === 'Remove empty sections') {
        $content = $sections[$count - 1]['content'];
        if ($content === '' || $content === '<p>') {
          if (isset($sections[$count])) {
            $sections[$count - 1] = $sections[$count];
            // The previous section has been overwritten with the current one,
            // so unset the current section and decrement the counter which is
            // returned so that processing can continue at the correct index.
            unset($sections[$count--]);
          }
          else {
            // There is no new section when checking the last section created,
            // so unset the previous one instead.
            unset($sections[$count - 1]);
          }
        }
      }
      else if ($name === 'Combine small sections') {
        if (!isset($sections[$count])) continue;

        $content = $sections[$count]['content'];
        if ((strlen($content) < 50) && (strpos($content, '<h') !== 0)) {
          $sections[$count - 1]['content'] .= $content;
          $sections[$count - 1]['tags'] =
            array_merge($sections[$count - 1]['tags'],
                        $sections[$count]['tags']);
          unset($sections[$count--]);
        }
      }
      else if ($name === 'Heading starts new section') {
        if (!isset($sections[$count])) continue;

        $content = $sections[$count]['content'];
        if (strpos($content, '<h') !== 0) {
          $sections[$count - 1]['content'] .= $content;
          $sections[$count - 1]['tags'] =
            array_merge($sections[$count - 1]['tags'],
                        $sections[$count]['tags']);
          unset($sections[$count--]);
        }
      }
      else if ($name === 'Add headings as tags') {
        $content = $sections[$count - 1]['content'];
        if (strpos($content, '<h') === 0) {
          $matches = array();
          if (preg_match('/^<h[1-6]>(.+)<\/h[1-6]>$/U', $content, $matches)) {
            $content = strip_tags($matches[1]);
            $found = false;
            foreach ($sections[$count - 1]['tags'] as $tag) {
              if ($tag['content'] === $content) {
                $found = true;
                break;
              }
            }
            if (!$found) {
              $sections[$count - 1]['tags'][] = array('content' => $content,
                'quote_start' => 0, 'quote_length' => strlen($content));
            }
          }
        }
      }
      else if ($name === 'Page break starts new page') {
        // Ignore the page break if the section is already first on the page.
        if ($sections[$count - 1]['page-count'] !== 0) {
          $content = $sections[$count - 1]['content'];
          $page_break = '<span class="project-page-break"></span>';
          if (strpos($content, $page_break) !== false) {
            $sections[$count - 1]['page']++;
            $sections[$count - 1]['page-count'] = 0;
            // Make sure the section containing the page break doesn't end up
            // on it's own page due to maximum number of sections per page.
            if (isset($sections[$count])) {
              $sections[$count]['page'] = $sections[$count - 1]['page'];
              $sections[$count]['page-count'] = 1;
            }
          }
        }
      }
    }
    return $count;
  }

  private function CopyFile($source, $destination) {
    if (file_exists($destination)) {
      if (md5_file($source) === md5_file($destination)) return $destination;

      // Prevent files with the same name being overwritten.
      $i = pathinfo($destination);
      $destination =
        $i['dirname'].'/'.$i['filename'].time().'.'.$i['extension'];
    }
    return copy($source, $destination) ? $destination : false;
  }

  private function Filters($id) {
    // Filters can only be used before a project is saved.
    if ($id !== 0) return '';

    $filter_options = '<option selected="selected" value="none">'.
      'Select a filter...</option>';
    $filter_descriptions = '<div id="project-filter-description-none" '.
      'class="project-filter-description"></div>';
    for ($i = 0; $i < count($this->all_filters); $i++) {
      $filter_options .= '<option value="'.$i.'">'.
        $this->all_filters[$i]['name'].'</option>';
      $filter_descriptions .= '<div id="project-filter-description-'.$i.'" '.
        'class="project-filter-description hidden">'.
        $this->all_filters[$i]['description'].'</div>';
    }
    return '<input id="project-show-filters" type="checkbox">'.
      '<label for="project-show-filters">show filters</label>'.
      '<div id="project-filters" class="hidden">'.
        '<label for="project-add-filter">Add filter:</label>'.
        '<select id="project-add-filter">'.$filter_options.'</select>'.
        '<button id="project-apply-filter">apply</button>'.
        $filter_descriptions.
      '</div>';
  }

  private function FormatListing($project) {
    return '<h3>'.$project['name'].'</h3>'.
      '<div><p>'.$project['description'].'</p>'.
        '<button id="project-open-'.$project['id'].'" '.
          'class="project-open">open</button>'.
        'Created by: '.$project['owner'].'</div>';
  }

  private function ImportFile($id) {
    $content = '<div class="form-spacing">'.
        '<label for="project-import-input">Import (.odt file):</label>'.
        '<input id="project-import-input" type="file" size="15">'.
      '</div>';
    if ($id === 0) return $content;

    $filename = '';
    $mysqli = connect_db();
    $query = 'SELECT filename FROM project WHERE project_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($project = $result->fetch_assoc()) {
        $filename = $project['filename'];
      }
      $result->close();
    }
    $mysqli->close();
    if ($filename === '' ) return '';

    $path = '';
    $matches = array();
    if (preg_match('/^(.+)\/([^\/]+\.odt)$/', $filename, $matches)) {
      $path = $matches[1];
      $filename = $matches[2];
    }
    if ($path === '' || $filename === '') return '';

    return 'This project was created from: <a href="'.$path.'/'.$filename.'">'.
      $filename.'</a> ';
  }

  private function InsertNavigation($owner, $page, $index,
                                    $order, $last = false) {
    $mysqli = connect_db();
    $module_page = $index === 1 ? $page : $page.'-p'.$index;
    $query = 'INSERT INTO modules (user, page, label, class, box_order, '.
      'placement, deleted) VALUES ("'.$owner.'", "'.$module_page.'", '.
      '"simple", "", '.($order + 1).', "middle", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Project->InsertNavigation 1: '.$mysqli->error);
    }
    $query = 'INSERT INTO modules_history (user, page, label, class, '.
      'box_order, placement, action, modified_by, timestamp) VALUES '.
      '("'.$owner.'", "'.$module_page.'", "simple", "", '.($order + 1).', '.
      '"middle", "add", "'.$this->user->name.'", '.time().')';
    if (!$mysqli->query($query)) {
      $this->Log('Project->InsertNavigation 2: '.$mysqli->error);
    }
    $id = $mysqli->insert_id;

    $link = '';
    if ($index === 2) {
      $link = '<a class="project-navigation project-previous-page" '.
        'href="'.$this->Url('', $page, $owner).'">&lt; Page 1</a>';
    }
    else if ($index > 2) {
      $link = '<a class="project-navigation project-previous-page" '.
        'href="'.$this->Url('', $page.'-p'.($index - 1), $owner).'">&lt; Page '.
        ($index - 1).'</a>';
    }
    if (!$last) {
      $link .= '<a class="project-navigation project-next-page" '.
        'href="'.$this->Url('', $page.'-p'.($index + 1), $owner).'">Page '.
        ($index + 1).' &gt;</a>';
    }
    $mysqli->close();
    return array($id, $link);
  }

  private function IsImage($file) {
    $image_extensions = array('jpg', 'jpeg', 'png', 'gif', 'svg');
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    if (!in_array($ext, $image_extensions)) return false;

    return strpos(mime_content_type($file), 'image') === 0;
  }

  private function AllProjects() {
    $project_list = array();
    $mysqli = connect_db();
    $query = 'SELECT user, name, description, timestamp, project.project_id '.
      'FROM project LEFT JOIN project_access ON project.project_id = '.
      'project_access.project_id WHERE user = "'.$this->user->name.'" OR '.
      'visitor = "'.$this->user->name.'"';
    if ($result = $mysqli->query($query)) {
      while ($project = $result->fetch_assoc()) {
        $project_list[] = array('owner' => $project['user'],
                                'name' => $project['name'],
                                'description' => $project['description'],
                                'timestamp' => (int)$project['timestamp'],
                                'id' => (int)$project['project_id']);
      }
      $result->close();
    }
    else {
      $this->Log('Project->AllProjects: '.$mysqli->error);
    }
    $mysqli->close();
    return $project_list;
  }

  // This function has been modified from a version from the Ophir project,
  // which is released under the LGPL.
  private function Odt2Html($odt_file, $xml_string = NULL) {
    $xml = new XMLReader();

    if ($xml_string === NULL) {
      if (!$xml->open('zip://'.$odt_file.'#content.xml')) {
        return array('error' => 'Could not read file: '.$odt_file);
      }
    }
    else {
      if (!$xml->xml($xml_string)) {
        return array('error' => 'Could not read xml.');
      }
    }

    // Initialise the content of the first section as an empty string so that
    // string concatenation can be used.
    $sections = array(array('content' => '', 'id' => 0, 'tags' => array(),
                            'page' => 0, 'page-count' => 0));
    $count = 0;
    // This is the maximum number of sections on a page. It's required because
    // we create inline instances of ckeditor for each section.
    $max_page_count = 20;
    $elements = array();
    $translate = array('draw:frame' => 'div class="project-frame"',
                       'text:list' => 'ul',
                       'text:list-item' => 'li',
                       'table:table' => 'table',
                       'table:table-row' => 'tr',
                       'table:table-cell' => 'td',
                       'text:table-of-content' => 'div class="project-toc"',
                       'text:line-break' => 'br');

    while ($xml->read()) {
      $tag = '';

      if ($xml->nodeType === XMLReader::END_ELEMENT) {
        if (empty($elements)) continue;

        do {
          $element = array_pop($elements);
          if ($element['tag'] !== '') {
            $sections[$count]['content'] .= '</'.$element['tag'].'>';
          }
        } while ($element && $element['name'] !== $xml->name);
        continue;
      }

      else if (in_array($xml->nodeType,
                        array(XMLReader::ELEMENT, XMLReader::TEXT,
                              XMLReader::SIGNIFICANT_WHITESPACE))) {
        if ($xml->name === '#text') {
          $sections[$count]['content'] .= trim(htmlspecialchars($xml->value));
        }
        else if ($xml->name === 'text:h') {
          $n = $xml->getAttribute('text:outline-level');
          if ($n > 6) $n = 6;
          $tag = 'h'.$n;
          $page = $sections[$count]['page'];
          $page_count = $sections[$count++]['page-count'] + 1;
          if ($page_count % $max_page_count === 0) {
            $page++;
            $page_count = 0;
          }
          $sections[$count] = array('content' => '<h'.$n.'>', 'id' => $count,
                                    'tags' => array(), 'page' => $page,
                                    'page-count' => $page_count);
          $count = $this->ApplyFilters($sections, $count);
        }
        else if ($xml->name === 'text:soft-page-break') {
          $sections[$count]['content'] .=
            '<span class="project-page-break"></span>';
        }
        else if ($xml->name === 'text:p') {
          $tag = 'p';
          // If the previous section is just an opening paragraph tag don't
          // start a new section here.
          if ($sections[$count]['content'] !== '<p>') {
            $page = $sections[$count]['page'];
            $page_count = $sections[$count++]['page-count'] + 1;
            if ($page_count % $max_page_count === 0) {
              $page++;
              $page_count = 0;
            }
            $sections[$count] = array('content' => '<p>', 'id' => $count,
                                      'tags' => array(), 'page' => $page,
                                      'page-count' => $page_count);
            $count = $this->ApplyFilters($sections, $count);
          }
        }
        else if ($xml->name === 'text:a') {
          $href = $xml->getAttribute('xlink:href');
          $tag = 'a';
          $sections[$count]['content'] .= '<a href="'.$href.'">';
        }
        else if ($xml->name === 'draw:image') {
          $source = 'zip://'.$odt_file.'#'.$xml->getAttribute('xlink:href');
          if ($this->IsImage($source)) {
            $destination = $this->PublicDirectory(basename($source));
            if (!$image = $this->CopyFile($source, $destination)) {
              $this->Log('Project->Odt2Html: Unable to copy image: '.
                         basename($source));
              continue;
            }
          }
          else {
            $this->Log('Project->Odt2Html: Invalid image file: '.$source);
            continue;
          }
          $sections[$count]['content'] .= '<img src="'.$image.'">';
        }
        else if ($xml->name === 'text:note') {
          $note_id = $xml->getAttribute('text:id');
          $note_name = 'Note';
          $note_content = '';
          while ($xml->read() && ($xml->name !== 'text:note' ||
                                  $xml->nodeType !== XMLReader::END_ELEMENT)) {
            if ($xml->name=='text:note-citation' &&
                $xml->nodeType === XMLReader::ELEMENT) {
              $note_name = $xml->readString();
            }
            else if ($xml->name=='text:note-body' &&
                    $xml->nodeType === XMLReader::ELEMENT) {
              $html = $this->Odt2Html($odt_file, $xml->readOuterXML());
              // Just flatten returned sections here.
              if (isset($html['sections'])) {
                $note_content .= implode($html['sections']);
              }
            }
          }
          $sections[$count]['content'] .=
            '<sup><a href="#project-footnote-'.$note_id.'" '.
              'class="project-footnote-anchor" '.
              'name="project-anchor-'.$note_id.'">'.
              $note_name.'</a></sup>'.
            '<div class="project-footnote hidden" '.
              'id="project-footnote-'.$note_id.'">'.
              '<a href="#project-anchor-'.$note_id .'">'.$note_name.'</a> '.
              $note_content.'</div>';
        }
        else if (array_key_exists($xml->name, $translate)) {
          $tag = current(explode(' ', $translate[$xml->name]));
          $sections[$count]['content'] .= '<'.$translate[$xml->name].'>';
        }
      }

      if ($xml->nodeType === XMLReader::ELEMENT && !($xml->isEmptyElement)) {
        $elements[] = array('name' => $xml->name,
                            'tag' => $tag);
      }
    }
    // Apply filters to the last section created (count is incremented because
    // it always refers to the next section).
    $this->ApplyFilters($sections, $count + 1);
    return array('filename' => $odt_file, 'sections' => $sections);
  }

  private function SaveProject() {
    if (!preg_match('/^[ a-z0-9_-]{1,200}$/i', $_POST['name'])) {
      return array('error' => 'Project name does not have correct format.');
    }

    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    // Create the page name from the project name, note that an indexed suffix
    // will be added for paged projects.
    $page = preg_replace('/ /', '_', $name);
    $owner = isset($us_project['owner']) ?
      $mysqli->escape_string($us_project['owner']) : $this->user->name;
    $available = false;
    // Check if this page name is already being used.
    $query = 'SELECT box_id FROM modules WHERE user = "'.$owner.'" AND '.
      'page = "'.$page.'"';
    if ($result = $mysqli->query($query)) {
      $available = $result->num_rows === 0;
      $result->close();
    }
    else {
      $this->Log('Project->SaveProject 1: '.$mysqli->error);
    }
    if (!$available) {
      $mysqli->close();
      return array('error' => 'Project name is not available.');
    }

    // TODO: Add owner, creation time, number of pages and link to first page
    // to the description and return it at the end. (This requires having
    // ckeditor working so that description doesn't show html...)
    $description = $mysqli->escape_string($_POST['description']);
    $us_project = json_decode($_POST['project'], true);
    $project_id = isset($us_project['id']) ? (int)$us_project['id'] : 0;
    $filename = '';
    if (isset($us_project['filename'])) {
      // Note that the path was already added in UploadFile.
      $filename = $mysqli->escape_string($us_project['filename']);
    }

    if ($project_id !== 0) {
      $query = 'UPDATE project SET user = "'.$owner.'", name = "'.$name.'", '.
        'description = "'.$description.'", timestamp = '.time().
        ' WHERE project_id = '.$project_id;
      if (!$mysqli->query($query)) {
        $this->Log('Project->SaveProject 2: '.$mysqli->error);
      }
      $mysqli->close();
      return array('id' => $project_id,
                   'info' => '<i>Project details have been updated.</i>');
    }

    $query = 'INSERT INTO project (user, name, filename, description, '.
      'timestamp) VALUES ("'.$owner.'", "'.$name.'", "'.$filename.'", '.
      '"'.$description.'", '.time().')';
    if (!$mysqli->query($query)) {
      $this->Log('Project->SaveProject 3: '.$mysqli->error);
    }
    $project_id = $mysqli->insert_id;
    if (!isset($us_project['sections'])) {
      // Create a default section for the project.
      $us_project['sections'] = array(array('content' => 'Default section'));
    }
    $module_page = $page;
    $previous_index = 0;
    $index = 0;
    $order = 0;
    $count = count($us_project['sections']);
    $section = new Section($this->user, $owner);
    $simple = new Simple($this->user, $owner);
    $us_content = array('project_id' => $project_id, 'data' => '');
    for ($i = 0; $i < $count; $i++) {
      $index = (int)$us_project['sections'][$i]['page'];
      if ($previous_index !== $index) {
        // Create a simple module that contains navigation links.
        list($id, $link) =
          $this->InsertNavigation($owner, $page, $index, $order);
        $simple->SetContent($id, array('data' => $link));
        $module_page = $page.'-p'.($index + 1);
        $previous_index = $index;
      }
      $order = (int)$us_project['sections'][$i]['page-count'];
      $query = 'INSERT INTO modules (user, page, label, class, box_order, '.
        'placement, deleted) VALUES ("'.$owner.'", "'.$module_page.'", '.
        '"section", "section-group", '.$order.', "middle", 0)';
      if (!$mysqli->query($query)) {
        $this->Log('Project->SaveProject 6: '.$mysqli->error);
      }
      $query = 'INSERT INTO modules_history (user, page, label, class, '.
        'box_order, placement, action, modified_by, timestamp) VALUES '.
        '("'.$owner.'", "'.$module_page.'", "section", "section-group", '.
        $order.', "middle", "add", "'.$this->user->name.'", '.time().')';
      if (!$mysqli->query($query)) {
        $this->Log('Project->SaveProject 7: '.$mysqli->error);
      }
      $us_content['data'] = $us_project['sections'][$i]['content'];
      $section->SetContent($mysqli->insert_id, $us_content);
    }
    $mysqli->close();
    if ($index !== 0) {
      // Add a navigation link to the last page. Note that above they are
      // created for the previous page, so need to add 1 to the index here.
      list($id, $link) =
        $this->InsertNavigation($owner, $page, $index + 1, $order, true);
      $simple->SetContent($id, array('data' => $link));
    }
    return array('id' => $project_id, 'info' => '<b>Project created</b> - '.
                 'view page: <a href="'.$this->Url('', $page, $owner).'">'.
                 $page.'</a>');
  }

  private function SaveTag() {
    if ($_POST['content'] === '') {
      return array('error' => 'Tag must not be empty.');
    }
    // TODO: check that the current user has permission to edit this project...

    $mysqli = connect_db();
    $content = $mysqli->escape_string($_POST['content']);
    $project_id = (int)$_POST['projectID'];
    $us_selected_id = json_decode($_POST['selectedID'], true);
    if (count($us_selected_id) !== 1) {
      // When there's not a single section selected, store this content in
      // project tags to record that it's a multi-section tag.
      $query = 'INSERT INTO project_tag VALUES ('.$project_id.', '.
        '"'.$content.'")';
      if (!$mysqli->query($query)) {
        $this->Log('Project->SaveTag: '.$mysqli->error);
      }
    }
    $mysqli->close();

    $section = new Section($this->user, $this->owner);
    $section->SaveTag($content, $project_id, $us_selected_id);
    return array('done' => true);
  }

  private function ShowProject($id = 0, $sections = 0) {
    $owner = $this->user->name;
    $name = 'New Project';
    $description = '';
    $timestamp = time();

    if ($id !== 0) {
      list($owner, $name, $description, $timestamp, $access) =
        $this->Access($id);
      if (!$access) {
        return '<div class="ui-state-highlight ui-corner-all">'.
          'You haven\'t been granted access to this project.</div>';
      }
    }

    $section_count = $sections === 1 ? 'There is <b>1</b> section' :
      'There are <b>'.$sections.'</b> sections';
    $section_editor = $sections === 0 ? '' : $section_count.' in this project.'.
      '<button id="project-section-editor">open section editor</button>';

    // TODO: invite should first show the owner (which should default to the
    // current user for a new project, but should be able to change), but no
    // point showing a select if there's no other users to collaborate with...
    // - show who has invite to the project, invite others to work on a project.
    // - use the notification system to tell a user they have invite.
    $invite = '';

    // TODO:
    // - copy and remove project options.
    // Starting a project:
    // - projects are at the top level (ie admin pages) if the current user has
    //   permission. otherwise the pages will be created under their username.
    //   - this requires a new permission where users can create new pages at
    //     admin level.
    //   - the user in the section table needs to be the directory the
    //     sections are viewed under, which we might want to be configurable.
    //     (would be creating an account for this purpose and giving the members
    //      of the project edit permissions)
    // Section handling:
    // - a project knows about all it's sections and which pages they are on.
    //   (need to be careful with copy page dialog too? just use the section
    //   module's copy method to alert the project that it's now responsible for
    //   a new section. this may be a good way to build up a new project?)
    $advanced = '';

    return '<button id="project-save">save project</button>'.
      '<form id="project-form">'.
        '<div id="project-title">'.
          '<span id="project-name">'.$name.'</span>'.
          '<input id="project-name-input" class="hidden" value="'.$name.'">'.
          '<input type="checkbox" id="project-name-edit">'.
          '<label for="project-name-edit">'.
            '<span class="ui-icon ui-icon-pencil"></span></label>'.
        '</div>'.
        '<div id="project-details">'.$invite.
          '<div id="project-import-info">'.$this->ImportFile($id).
            $this->Filters($id).'</div>'.
          $advanced.
          '<label for="project-description">Project description:</label><br>'.
          // TODO: description can be html so load ckeditor
          '<textarea id="project-description">'.$description.'</textarea>'.
          '<div id="project-info"></div>'.
        '</div>'.
      '</form>'.
      '<div id="project-section-info"><span class="hidden"></span>'.
        $section_editor.
      '</div>'.
      '<ul id="project-sections"></ul>'.
      '<div id="project-section-dialog" class="hidden">'.
        '<label for="project-new-tag">Add a project tag:</label><br>'.
        '<textarea id="project-new-tag"></textarea><br>'.
        '<button id="project-save-tag">submit</button>'.
        '<p id="project-tags"></p>'.
      '</div>';
  }

  private function Tags($id) {
    $tags = array();
    $mysqli = connect_db();
    $query = 'SELECT content FROM project_tag WHERE project_id = '.$id;
    if ($result = $mysqli->query($query)) {
      while ($project_tag = $result->fetch_assoc()) {
        $tags[] = array('content' => $project_tag['content']);
      }
    }
    else {
      $this->Log('Project->Tags: '.$mysqli->error);
    }
    $mysqli->close();
    return $tags;
  }

  private function UploadFile() {
    // First check if the user's upload directory is at capacity.
    $handle = popen('/usr/bin/du -sm '.$this->PublicDirectory(), 'r');
    $size = fgets($handle);
    pclose($handle);
    $matches = array();
    if (!preg_match('/^([0-9]+)/', $size, $matches)) {
      return array('error' => 'Could not check upload directory.');
    }
    $size = $matches[1];
    if ((int)$size > $this->user->config->MaxUpload()) {
      return array('error' => 'Upload directory full.');
    }
    $max_file_size = $this->user->config->MaxFileSize();
    // ['upload']['size'] is given in bytes, MaxFileSize is in megabytes.
    if ($_FILES['upload']['size'] > $max_file_size * 1000000) {
      return array('error' => 'Upload file is too large. (max '.
                   $max_file_size.'M)');
    }
    // Replace spaces in the uploaded file name.
    $file = preg_replace('/ /', '_', basename($_FILES['upload']['name']));
    $regex = '/^([a-z0-9_-]{1,200})\.([a-z0-9]{1,10})$/i';
    if (!preg_match($regex, $file, $matches)) {
      return array('error' => 'Filename does not have correct format.');
    }
    $name = $matches[1];
    $type = $matches[2];
    if ($type !== 'odt') {
      return array('error' => "File type must be 'odt'.");
    }

    $path = $this->PublicDirectory($file);
    $count = 2;
    while (file_exists($path)) {
      // If this file name is not already associated with a project we don't
      // need to worry about previously uploaded copies.
      $associated = false;
      $mysqli = connect_db();
      $query = 'SELECT project_id FROM project WHERE filename = "'.$path.'"';
      if ($result = $mysqli->query($query)) {
        $associated = $result->num_rows !== 0;
        $result->close();
      }
      else {
        $this->Log('Project->UploadFile: '.$mysqli->error);
      }
      $mysqli->close();
      if (!$associated) break;

      $path = $this->PublicDirectory($name.'-'.$count.'.'.$type);
      $count++;
    }
    $tmp = $_FILES['upload']['tmp_name'];
    if (!move_uploaded_file($tmp, $path)) {
      return array('error' => 'File: '.$file.' was not uploaded.');
    }

    // Store the name of the file being processed so that processing can be
    // re-run as filters are applied.
    $_SESSION['project-filename'] = $path;
    return $this->Odt2Html($path);
  }


}
