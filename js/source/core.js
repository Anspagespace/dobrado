/*global dobrado:true, CKEDITOR: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

(function () {

  'use strict';

  dobrado.arrowBackgroundColor = '';
  dobrado.arrowBorderColor = '';
  // Check if the editor should be loaded when a simple edit area is clicked.
  dobrado.editMode = false;
  dobrado.layoutMode = false;
  // Don't allow the editor to be requested again while waiting for a previous
  // request to complete.
  dobrado.editorLoading = false;
  // Keep track of the editor on the page created using CKEDITOR.replace.
  dobrado.editor = null;
  // This object is used to keep track of editors created using CKEDITOR.inline.
  dobrado.editors = {};
  // The page to redirect to when closeEditor has customChange set.
  dobrado.permalink = '';
  // The redirect above is only done if the user was on the old permalink page
  // so need to store that too for comparison.
  dobrado.oldPermalink = '';
  // User defined settings loaded via init.php.
  dobrado.moduleSettings = {};
  // Store some css values for the item that is currently being sorted.
  var sortable = {};
  // The variable duplicate is the *return* value from this anonymous function,
  // which provides a closure to check the previous id, placement and order.
  var duplicate = function() {
    var prev_id, prev_placement, prev_order;
    return function(id, placement, order) {
      if (id === prev_id &&
          placement === prev_placement &&
          order === prev_order) {
        return true;
      }
      else {
        prev_id = id;
        prev_placement = placement;
        prev_order = order;
        return false;
      }
    };
  }();

  $(function() {
    $.post('/php/init.php',
           { url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'initialisation')) {
          return;
        }
        var attributes = JSON.parse(response);
        if (attributes.clear && dobrado.localStorage) {
          localStorage.clear();
        }
        if (attributes.reload) {
          location.reload(true);
          return;
        }
        if (!attributes.locked) {
          $.each(attributes.list, function() {
            $(this.id).data('label', this.label);
          });
        }
        if (attributes.settings) {
          // Want to store all settings so that modules can share them, but
          // some modules also need to set options once settings are provided.
          dobrado.moduleSettings = attributes.settings;
          $.each(dobrado.moduleSettings, function(module, settings) {
            if (dobrado[module] && dobrado[module].settingsCallback) {
              // Only call if the module is on the page.
              if ($('.' + module).length !== 0) {
                dobrado[module].settingsCallback(settings);
              }
            }
          });
        }
        if (attributes.tooltip) {
          dobrado.tooltip(attributes.tooltip.selector,
                          attributes.tooltip.content, attributes.tooltip.arrow);
        }
        if (dobrado.localStorage && !localStorage.indieConfig) {
          localStorage.indieConfig = JSON.stringify({
            status: attributes.status });
        }
      });
    $('#page-input').autocomplete({ minLength: 2,
                                    search: dobrado.fixAutoCompleteMemoryLeak,
                                    select: changePage, source: pageSearch });
    // Want to control when ckeditor is shown, while still using inline mode.
    CKEDITOR.disableAutoInline = true;
    // Make sure the css values can be accessed by adding a div to the page.
    var widget =
      $('<div></div>').addClass('ui-widget-content').hide().appendTo('body');
    dobrado.arrowBackgroundColor =
      $('.ui-widget-content').css('background-color');
    dobrado.arrowBorderColor =
      $('.ui-widget-content').css('border-bottom-color');
    widget.remove();
    $('.logged-in-display-none').css('display', 'none');
    // Sortables are enabled when the toolbar is in use and not on mobile.
    // (Checking for mobile gets around a scrolling bug seen on chrome mobile
    //  when sortable is initialised.)
    if (!dobrado.mobile) {
      $('.sortable').sortable({
        disabled: true,
        start: startSort,
        stop: stopSort,
        update: updateLayoutPosition,
        connectWith: '.sortable',
        placeholder: 'ui-sortable-placeholder',
        opacity: 0.6
      });
    }
  });

  function changePage(event, ui) {
    if (ui) {
      dobrado.changePage(ui.item.value);
    }
  }

  function pageSearch(request, callback) {
    $.post('/php/search.php',
           { term: request.term, token: dobrado.token },
      function(response) {
        callback(JSON.parse(response));
      });
  }

  function updateLayoutPosition(event, ui) {
    var id = '';
    var order = '';
    var placement = '';

    // Go through each of the previous items and check if they're a group of
    // modules. If they are a group, add the number in the group to the total.
    var previous = 0;
    $(ui.item).prevAll().each(function() {
      if ($(this).attr('id')) {
        previous++;
      }
      else {
        // If $(this) doesn't have an id, assume it's a group of modules.
        previous += $(this).children().length;
      }
    });

    if ($(ui.item).attr('id')) {
      id = '#' + $(ui.item).attr('id');
      placement = $(ui.item).parent().attr('class').match(/^\S+/)[0];
      order = previous;
    }
    else {
      // If ui.item doesn't have an id, assume this is a group of modules.
      var thisId = '';
      var thisOrder = 0;
      $(ui.item).children().each(function() {
        if ($(this).attr('id')) {
          thisId = '#' + $(this).attr('id');
          if (placement === '') {
            placement = $(ui.item).parent().attr('class').match(/^\S+/)[0];
          }
          if (id !== '') {
            id += ',';
          }
          id += thisId;
          // The new order is the number of modules previous to the parent
          // group, plus the previous siblings in the group.
          thisOrder = previous + $(thisId).prevAll().length;
          if (order !== '') {
            order += ',';
          }
          order += thisOrder;
        }
      });
    }

    // This event fires twice when switching groups, so check for duplicate.
    if (duplicate(id, placement, order)) {
      return;
    }
    $.post('/php/layout.php',
           { id: id, placement: placement, box_order: order,
             url: location.href, token: dobrado.token },
      function(response) {
        dobrado.checkResponseError(response, 'layout');
      });
  }

  function startSort(event, ui) {
    // Save the current values for the sortable being adjusted.
    sortable = { maxHeight: $(ui.item).css('max-height'),
                 overflow: $(ui.item).css('overflow') };
    $(ui.item).css('max-height', '50px');
    $(ui.item).css('overflow', 'hidden');
    // Force placeholder size now that the item's height has been set.
    $('.sortable').sortable('option', 'forcePlaceholderSize', true);
  }

  function stopSort(event, ui) {
    // Reset item to original settings.
    $(ui.item).css('max-height', sortable.maxHeight);
    $(ui.item).css('overflow', sortable.overflow);
    $('.sortable').sortable('option', 'forcePlaceholderSize', false);
  }

  dobrado.tooltip = function(selector, content, arrow) {
    if (!selector || !content) return;

    // Delay calculating position for tooltip until the rest of the js
    // on the page has run.
    setTimeout(function() {
      if (!arrow) arrow = '10px';

      var background = { top: '-17px', left: arrow,
                         borderColor: 'transparent transparent ' +
                           dobrado.arrowBackgroundColor + ' transparent' };
      var border = { top: '-20px', position: 'absolute', marginLeft: '0',
                     left: arrow, borderColor: 'transparent transparent ' +
                       dobrado.arrowBorderColor + ' transparent' };

      $(selector).tooltip({ position:
        { my: 'left top+5',
          at: 'left bottom',
          using: function(position, feedback) {
            $(this).css(position);
            $('<div>').addClass('arrow').css(background).prependTo(this);
            $('<div>').addClass('arrow-border').css(border).prependTo(this);
            // Close the tooltip if the user clicks on it.
            $(this).click(function() { $(selector).tooltip('close'); });
          }
        }
      });
      $(selector).attr('title', content).tooltip('open');
      // When a user starts typing into an input field, the tooltip obscures
      // the autocomplete so close the tooltip.
      $(selector).keydown(function() { $(selector).tooltip('close'); });
    }, 5000);
  };

  dobrado.changePage = function(page) {
    var user = dobrado.readCookie('user');
    if (!user) {
      dobrado.log('Error: User unknown', 'error');
      return;
    }

    if (/^[a-z0-9\/_-]+$/i.test(page)) {
      dobrado.log('Changing page...', 'info');
      // Look for an optional username before the page name.
      var fields = page.match(/^([^\/]+)\/?(.*)$/);
      if (fields && fields.length === 3) {
        if (fields[2] === '') {
          // Only the page name, must be the user's own page.
          if (user === 'admin') {
            location.href = '/index.php?page=' + fields[1];
          }
          else {
            location.href = '/' + user + '/index.php?page=' + fields[1];
          }
        }
        else {
          // Otherwise redirect to another user's page.
          // Also special case for the 'admin' user.
          if (fields[1] === 'admin') {
            location.href = '/index.php?page=' + fields[2];
          }
          else {
            location.href = '/' + fields[1] + '/index.php?page=' + fields[2];
          }
        }
      }
    }
    else {
      // TODO: Assuming search module exists, if there was no page match and
      // dobrado.moduleSettings.search.page is set, change page as above but
      // with: &q= + encodeURIComponent(page)
      dobrado.log('Invalid page name.', 'error');
    }
  };

  dobrado.ckeditor = function(response) {
    if (dobrado.checkResponseError(response, 'ckeditor')) {
      return;
    }
    var editor = JSON.parse(response);
    dobrado.createModule('browser', 'browser', editor.callback);
  };

  dobrado.createModule = function(callbackModule, label, context) {

    function style(rules) {
      for (var selector in rules) {
        for (var property in rules[selector]) {
          var value = rules[selector][property];
          $(selector).css(property, value);
        }
      }
    }

    if (dobrado.more) {
      // Close the more dialog automatically when other modules are added.
      $('.more').dialog('close');
    }

    if (label !== 'post') {
      dobrado.log('Adding module...', 'info');
    }
    $.post('/php/add.php',
           { label: label, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'add')) {
          return;
        }

        var module = JSON.parse(response);
        var placement = '.main .' + module.placement;
        if (module.placement === 'outside') {
          placement = 'body';
        }
        // The module may also need to be placed inside a parent div, check
        // if it already exists on the page otherwise create it.
        if (module.group) {
          // If the module should be hidden, hide the parent div too.
          var wrapperClass = module.group;
          if (module.content === false) {
            wrapperClass += ' hidden';
          }
          if ($(placement + ' .' + module.group).length === 0) {
            $('<div></div>').addClass(wrapperClass).appendTo(placement);
          }
          placement += ' .' + module.group;
        }
        // Hide the module if content === false.
        var className = module.label;
        if (module.content === false) {
          className += ' hidden';
        }
        var selector = '#' + module.id;
        // Unique id's are not assigned for modules with 'outside' placement.
        if (module.id === 'dobrado-0') {
          $('<div>' + module.content +
            '</div>').addClass(className).prependTo(placement);
          selector = '.' + module.label;
        }
        else {
          $('<div>' + module.content + '</div>').attr({
            id: module.id }).addClass(className).prependTo(placement);
          $(selector).data('label', module.label);
        }
        style(module.style);
        if (dobrado.editMode) {
          var element = $(selector + ' .dobrado-editable').get(0);
          if (element) {
            dobrado.inlineEditor(element);
          }
        }
        if (dobrado.layoutMode) {
          $('.sortable > *').css('cursor', 'move');
        }
        // Get a javascript file for the module if required.
        if (module.script) {
          $.getScript('/js/' + module.script, function() {
              if (dobrado[callbackModule] &&
                  dobrado[callbackModule].newModuleCallback) {
                dobrado[callbackModule].newModuleCallback(selector, context);
              }
            });
        }
        // callbackModule can have a script when the added module doesn't.
        else if (dobrado[callbackModule] &&
                 dobrado[callbackModule].newModuleCallback) {
          dobrado[callbackModule].newModuleCallback(selector, context);
        }
    });
  };

  dobrado.addModule = function(event) {
    event.preventDefault();
    dobrado.createModule('', $(this).attr('id'), '');
  };

  dobrado.inlineEditor = function(element) {

    function setCurrent(event) {
      // Call closeEditor before switching dobrado.current in case a module has
      // set dobrado.editor using CKEDITOR.replace. 
      dobrado.closeEditor();
      dobrado.current = dobrado.editors[event.editor.id];
      // The module's content might have been changed from what was originally
      // loaded, so request the original content from the server.
      $.post('/php/request.php',
             { id: dobrado.current, request: $(dobrado.current).data('label'),
               mode: 'box', url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'setCurrent')) {
            return;
          }
          var module = JSON.parse(response);
          event.editor.setData(module.source);
          event.editor.resetDirty();
        });
    }

    var id = '';
    var label = '';
    $(element).parents('div').each(function() {
      if (/^dobrado-/.test($(this).attr('id'))) {
        id = '#' + $(this).attr('id');
        label = $(this).data('label');
        return false;
      }
    });
    // Some modules use 'dobrado-editable' class to save content, but don't
    // want to use inline editor. They need to provide a showEditor function.
    if (dobrado[label] && dobrado[label].showEditor) {
      return;
    }

    // Show a special border around editable areas when active,
    // and set contenteditable to true for inline editing.
    $(element).addClass('border').attr('contenteditable', true);
    var editor = CKEDITOR.inline(element,
      { allowedContent: true,
        disableNativeSpellChecker: false,
        enterMode: CKEDITOR.ENTER_BR,
        extraPlugins: 'extended',
        filebrowserBrowseUrl: '/php/browse.php',
        on: { blur: dobrado.save, focus: setCurrent },
        removePlugins:
          'elementspath,tableselection,tabletools,contextmenu,liststyle',
        toolbar: [[ 'Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'Link',
                    'Unlink', '-', 'EmojiPanel', 'Image', 'Extended' ]]
      });
    // Map the editor's id to the module's id to set dobrado.current.
    dobrado.editors[editor.id] = id;
  };

  dobrado.select = function() {
    $(this).select();
    // Cancel event propagation.
    return false;
  };

  dobrado.clear = function() {
    $(this).val('');
    // Cancel event propagation.
    return false;
  };

  dobrado.closeEditor = function(saveContent, customChange) {
    var current = dobrado.current;
    var content = {};
    var editorChange = false;

    if (saveContent) {
      content = saveContent;
    }
    if (dobrado.editor) {
      // Post module needs content even if it hasn't changed.
      content.data = dobrado.editor.getData();
      if (current && dobrado.editor.checkDirty()) {
        editorChange = true;
        var editable = $(current + ' .dobrado-editable');
        if (editable.prop('tagName') === 'TEXTAREA') {
          editable.val(content.data);
        }
        else {
          editable.html(content.data);
        }
      }
      dobrado.editor.destroy();
      dobrado.editor = null;
      dobrado.current = '';
    }
    var label = $(current).data('label');
    // Check if this module provides a showEditor function, as it must have
    // it's own way to save content. Can only restore the editor here when
    // not in edit mode, because all editors currently share dobrado.current
    // so that they can access the extended editor.
    if (dobrado[label] && dobrado[label].showEditor) {
      if (!dobrado.editMode) {
        dobrado[label].showEditor();
      }
      return;
    }

    if (label === 'post' && content.data) {
      // Add an exception for the post module, which does it's own check to
      // see if content has changed but may have suppressed notifications.
      editorChange = true;
    }

    if (editorChange || customChange) {
      dobrado.log('Saving content...', 'info');
      $.post('/php/content.php',
             { id: current, label: label, content: JSON.stringify(content),
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'content')) {
            return;
          }
          if (label === 'commenteditor') {
            // Don't want to show new content for the dialog, or reload.
            return;
          }

          var formatted = JSON.parse(response);
          // If there's no content initially then post modules are hidden,
          // so show it now if content is returned.
          if (formatted.html) {
            $(current).html(formatted.html);
            $(current).show();
            if (dobrado.editMode) {
              var element = $(current + ' .dobrado-editable').get(0);
              if (element) {
                dobrado.inlineEditor(element);
              }
            }
          }
          // Changing custom settings can't be reflected on the page without
          // a reload or redirect. 
          if (customChange && label !== 'post') {
            location.reload();
            return;
          }

          if (label === 'post') {
            // Update notifications are done once the post has been saved.
            $.post('/php/request.php',
                   { request: 'post', action: 'updated',
                     url: location.href, token: dobrado.token },
              function(response) {
                if (dobrado.checkResponseError(response, 'post updated')) {
                  return;
                }
                var post = JSON.parse(response);
                if (post.status) {
                  // Need to wait for the 'Saving content' notification to be
                  // removed before displaying the post status notification.
                  setTimeout(function() {
                    dobrado.log(post.status, 'info');
                    setTimeout(function() {
                      $('.control .info').hide();
                    }, 10000);
                  }, 2000);
                }
              });
            if (customChange) {
              // The page changes based on title for the post module, which is
              // set in dobrado.permalink. Only redirect if the permalink has
              // changed, and the user is on the old permalink page. Note that
              // if the title is empty, the permalink is automatically generated
              // on the server so dobrado.permalink will be empty here.
              var exp = new RegExp(dobrado.oldPermalink + '$');
              if (dobrado.permalink !== '' && dobrado.oldPermalink !== '' &&
                  dobrado.permalink !== dobrado.oldPermalink &&
                  exp.test(location.href)) {
                // When redirecting need to find the current page owner. This
                // is done using the current url, if a username is not found
                // then the page owner is 'admin'.
                let owner = 'admin';
                let regex = /\/\/[^\/]+\/([^\/]+)\/?(.*)$/;
                let fields = location.href.match(regex);
                if (fields && fields.length === 3) {
                  if (fields[2] !== '') {
                    owner = fields[1];
                  }
                }
                // TODO: If the new permalink has been used previously then the
                // server will increment a suffix, which means these redirects
                // won't work.
                if (owner === 'admin') {
                  location.href = '/index.php?page=' + dobrado.permalink;
                }
                else {
                  location.href = '/' + owner + '/index.php?page=' +
                    dobrado.permalink;
                }
              }
            }
            if (dobrado.writer) {
              dobrado.writer.showEditor();
            }
          }
        });
    }
    else {
      // Also make sure inline editors get a chance to save their content,
      // which is done automatically when they lose focus.
      $.each(CKEDITOR.instances, function() {
        this.fire('blur');
      });
    }
  };

  dobrado.removeModule = function(id, done) {
    var label = $(id).data('label');
    $.post('/php/remove.php',
           { id: id, label: label, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'remove')) {
          return;
        }
        var remove = JSON.parse(response);
        if (remove.done) {
          $(id).remove();
          if (done) {
            done();
          }
          if (label === 'post') {
            $.post('/php/request.php',
                   { request: 'post', action: 'updated',
                     url: location.href, token: dobrado.token },
              function(response) {
                dobrado.checkResponseError(response, 'remove post update');
              });
          }
        }
      });
  };

  dobrado.readCookie = function(name) {
    var exp = new RegExp(name + '=([^;]+)');
    var fields = document.cookie.match(exp);
    if (fields && fields.length === 2) {
      return fields[1];
    }
    return null;
  };

  dobrado.home = function() {
    var user = dobrado.readCookie('user');
    if (user) {
      if (user === 'admin') {
        location.href = '/';
      }
      else {
        location.href = '/' + user;
      }
    }
    else {
      dobrado.log('Error: User unknown', 'error');
    }
  };

  dobrado.toggleMenu = function(name) {
    $('.' + name).toggle();
  };

  dobrado.hideOtherMenu = function(name) {
    // hideOtherMenu is called with a menu that should be left open.
    $('.menu-wrapper').each(function() {
      if (!$(this).hasClass(name)) {
        $(this).hide();
      }
    });
  };

  dobrado.formatDate = function(year, month, day) {
    if (year && month && day) {
      return $.datepicker.formatDate(dobrado.dateFormat,
                                     new Date(year, month - 1, day));
    }
    if (year) {
      // If only one parameter is given, it's not the year but a timestamp.
      return $.datepicker.formatDate(dobrado.dateFormat,
                                     new Date(parseInt(year, 10)));
    }
    return $.datepicker.formatDate(dobrado.dateFormat, new Date());
  };

  // TODO: Remove this once jQuery UI 12.2 is out, which fixes the leak.
  dobrado.fixAutoCompleteMemoryLeak = function(event, ui) {
    $(this).data('ui-autocomplete').menu.bindings = $();
  };

}());
