<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class ControlTest extends TestCase {

  private $control = NULL;

  protected function setUp() {
    $user = new User();
    $this->control = new Control($user, 'admin');
  }

  public function testCanAdd() {
    $this->assertFalse($this->control->CanAdd(''));
  }

  public function testCanEdit() {
    $this->assertFalse($this->control->CanEdit(0));
  }

  public function testCanRemove() {
    $this->assertFalse($this->control->CanRemove(0));
  }

  public function testIncludeScript() {
    $this->assertTrue($this->control->IncludeScript());
  }

  public function testPlacement() {
    $this->assertEquals($this->control->Placement(), 'outside');
  }

}
