<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function helper_all_modules() {
  return ['account' => ['title' => 'Account Menu', 'display' => 0],
          'autoupdate' => ['title' => 'Automatic Updates','display' => 1],
          'analytics' => ['title' => 'Analytics', 'display' => 0],
          'banking' => ['title' => 'Banking', 'display' => 1],
          'browser' => ['title' => 'File Browser', 'display' => 1],
          'cart' => ['title' => 'Shopping Cart', 'display' => 1],
          'comment' => ['title' => 'Comment', 'display' => 0],
          'commenteditor' => ['title' => 'Comment Editor','display' => 1],
          'contact' => ['title' => 'Contact Form', 'display' => 1],
          'control' => ['title' => 'Control Bar', 'display' => 0],
          'detail' => ['title' => 'User Details', 'display' => 1],
          'domaincheck' => ['title' => 'Domain Check', 'display' => 1],
          'escrow' => ['title' => 'Escrow', 'display' => 1],
          'extended' => ['title' => 'Extended Editor', 'display' => 0],
          'gift' => ['title' => 'Gift Registry', 'display' => 1],
          'graph' => ['title' => 'Graph', 'display' => 1],
          'grid' => ['title' => 'Grid', 'display' => 1],
          'groupwizard' => ['title' => 'Group Settings', 'display' => 1],
          'hosting' => ['title' => 'Hosting', 'display' => 1],
          'indieauth' => ['title' => 'Indieauth', 'display' => 1],
          'invite' => ['title' => 'Invite', 'display' => 1],
          'invoice' => ['title' => 'Invoice', 'display' => 1],
          'login' => ['title' => 'Login Form', 'display' => 1],
          'manager' => ['title' => 'Manager', 'display' => 1],
          'mapper' => ['title' => 'Mapper', 'display' => 1],
          'members' => ['title' => 'Accounts', 'display' => 1],
          'more' => ['title' => '<b>more...</b>', 'display' => 0],
          'notification' => ['title' => 'Notifications', 'display' => 0],
          'organiser' => ['title' => 'Organiser', 'display' => 1],
          'pager' => ['title' => 'Pager', 'display' => 1],
          'payment' => ['title' => 'Payment', 'display' => 1],
          'player' => ['title' => 'Audio Player', 'display' => 1],
          'post' => ['title' => 'Post', 'display' => 0],
          'project' => ['title' => 'Project Manager', 'display' => 1],
          'purchase' => ['title' => 'Purchase', 'display' => 1],
          'reader' => ['title' => 'Reader', 'display' => 1],
          'registration' => ['title' => 'Registration Form','display'=>1],
          'report' => ['title' => 'Report', 'display' => 1],
          'roster' => ['title' => 'Roster', 'display' => 1],
          'section' => ['title' => 'Project Section', 'display' => 1],
          'sell' => ['title' => 'Point Of Sale', 'display' => 1],
          'settings' => ['title' => 'Settings', 'display' => 1],
          'simple' => ['title' => 'Text Area', 'display' => 1],
          'slider' => ['title' => 'Image Slider', 'display' => 1],
          'spark' => ['title' => 'Spark', 'display' => 1],
          'start' => ['title' => 'Help', 'display' => 1],
          'stock' => ['title' => 'Stock', 'display' => 1],
          'subscribe' => ['title' => 'Subscribers', 'display' => 1],
          'summary' => ['title' => 'Summary', 'display' => 1],
          'turner' => ['title' => 'Page Turner', 'display' => 1],
          'usb' => ['title' => 'Universal Sign-In Button','display' => 1],
          'viewanalytics' => ['title' => 'Analytics', 'display' => 1],
          'workgroup' => ['title' => 'Workgroup', 'display' => 1],
          'writer' => ['title' => 'Writer', 'display' => 1],
          'xero' => ['title' => 'Xero', 'display' => 1]];
}

function helper_create_test_data() {
  $mysqli = connect_db();
  $password = $mysqli->escape_string(password_hash('admin', PASSWORD_DEFAULT));
  $query = 'INSERT INTO users VALUES ("admin", "' . $password . '", "", ' .
    '"", 1, "", ' . time() . ', "", 1)';
  if (!$mysqli->query($query)) {
    log_db('helper insert user: ' . $mysqli->error);
  }
  // Add the 'admin' user to their own 'admin' group so that new accounts
  // can be created, and give them permission to edit the group.
  $query = 'INSERT INTO group_names VALUES ("admin", "admin", "admin", 1)';
  if (!$mysqli->query($query)) {
    log_db('helper insert group_names: ' . $mysqli->error);
  }
  $query = 'INSERT INTO template VALUES ("invoice-day", "", "today")';
  if (!$mysqli->query($query)) {
    log_db('helper insert template invoice-day: ' . $mysqli->error);
  }
  $query = 'INSERT INTO template VALUES ("invoice-day-time", "", ' .
    '"' . date('H:i:00') . '")';
  if (!$mysqli->query($query)) {
    log_db('helper insert template invoice-day-time: ' . $mysqli->error);
  }
  $mysqli->close();
}

function helper_drop_tables() {
  $mysqli = connect_db();
  $drop_table_list = [];
  if ($mysqli_result = $mysqli->query('SHOW TABLES')) {
    while ($show_tables = $mysqli_result->fetch_row()) {
      $drop_table_list[] = $show_tables[0];
    }
    $mysqli_result->close();
  }
  else {
    error_log('show tables: ' . $mysqli->error);
  }
  foreach ($drop_table_list as $drop_table) {
    if (!$mysqli->query('DROP TABLE ' . $drop_table)) {
      error_log('drop table: ' . $mysqli->error);
    }
  }
  $mysqli->close();
}
