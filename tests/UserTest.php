<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {

  private $user = NULL;

  protected function setUp() {
    $this->user = new User('admin');
    $this->user->SetPermission('index');
  }

  public function testName() {
    $this->assertEquals($this->user->name, 'admin');
  }

  public function testPage() {
    $this->assertEquals($this->user->page, 'index');
  }

  public function testGroup() {
    $this->assertEquals($this->user->group, '');
  }

  public function testActive() {
    $this->assertTrue($this->user->active);
  }

  public function testLoggedIn() {
    $this->assertFalse($this->user->loggedIn);
  }

  public function testLoginFailed() {
    $this->assertFalse($this->user->loginFailed);
  }

  public function testEmailReset() {
    $this->assertFalse($this->user->emailReset);
  }

  public function testDefaultPage() {
    $this->assertFalse($this->user->defaultPage);
  }

  // The following permissions all return false because a session hasn't been
  // started, and permission checks also rely on $_SESSION['user'] being set in
  // page.php when the user logs in.
  public function testCanEditSite() {
    $this->assertFalse($this->user->canEditSite);
  }

  public function testCanEditPage() {
    $this->assertFalse($this->user->canEditPage);
  }

  public function testCanCopyPage() {
    $this->assertFalse($this->user->canCopyPage);
  }

  public function testCanViewPage() {
    $this->assertFalse($this->user->canViewPage);
  }

}
