<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'instance.php';

class Module {

  private $instance;

  public function __construct($user, $owner, $label) {
    $this->instance = instance($user, $owner, $label);
  }

  // The IsInstalled function is provided by this Module class.
  // Otherwise all other functions below are proxies for modules.
  public function IsInstalled() {
    return $this->instance !== NULL;
  }

  public function Add($id) {
    if ($this->instance === NULL) return;
    return $this->instance->Add($id);
  }

  public function Callback() {
    if ($this->instance === NULL) return;
    return $this->instance->Callback();
  }

  public function CanAdd($page) {
    if ($this->instance === NULL) return false;
    return $this->instance->CanAdd($page);
  }

  public function CanEdit($id) {
    if ($this->instance === NULL) return false;
    return $this->instance->CanEdit($id);
  }

  public function CanRemove($id) {
    if ($this->instance === NULL) return false;
    return $this->instance->CanRemove($id);
  }

  public function Content($id) {
    if ($this->instance === NULL) return;
    return $this->instance->Content($id);
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    if ($this->instance === NULL) return;
    return $this->instance->Copy($id, $new_page, $old_owner, $old_id);
  }

  public function Cron() {
    if ($this->instance === NULL) return;
    return $this->instance->Cron();
  }

  public function Factory($fn, $p = NULL) {
    if ($this->instance === NULL) return;
    return $this->instance->Factory($fn, $p);
  }

  public function Group() {
    if ($this->instance === NULL) return;
    return $this->instance->Group();
  }

  public function IncludeScript() {
    if ($this->instance === NULL) return false;
    return $this->instance->IncludeScript();
  }

  public function Install($path) {
    if ($this->instance === NULL) return;
    return $this->instance->Install($path);
  }

  public function Placement() {
    if ($this->instance === NULL) return;
    return $this->instance->Placement();
  }

  public function Publish($id, $update) {
    if ($this->instance === NULL) return;
    return $this->instance->Publish($id, $update);
  }

  public function Remove($id) {
    if ($this->instance === NULL) return;
    return $this->instance->Remove($id);
  }

  // Note that $us_content passed in here is sql UnSafe.
  public function SetContent($id, $us_content) {
    if ($this->instance === NULL) return;
    return $this->instance->SetContent($id, $us_content);
  }

  public function Update() {
    if ($this->instance === NULL) return;
    return $this->instance->Update();
  }

  public function UpdateScript($path) {
    if ($this->instance === NULL) return;
    return $this->instance->UpdateScript($path);
  }

  // Below here are public functions of the Base class.

  public function AlreadyOnPage($label, $page) {
    if ($this->instance === NULL) return;
    return $this->instance->AlreadyOnPage($label, $page);
  }

  public function Media($id) {
    if ($this->instance === NULL) return;
    return $this->instance->Media($id);
  }

  public function Style($id, $media = '') {
    if ($this->instance === NULL) return;
    return $this->instance->Style($id, $media);
  }

  public function Substitute($label, $patterns = '',
                             $replacements = '', $group = NULL) {
    if ($this->instance === NULL) return;
    return $this->instance->Substitute($label, $patterns,
                                       $replacements, $group);
  }

}
