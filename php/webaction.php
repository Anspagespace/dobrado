<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

$url = isset($_POST['url']) ? $_POST['url'] : '';

// If url is not set, then a remote request is looking up the config for a
// user on this site. Also want to look up the config for local users when
// a user provides a url that matches the current domain.
if ($url === '' || strpos($url, $_SERVER['SERVER_NAME']) !== false) {

  include 'functions/db.php';
  include 'functions/page_owner.php';
  include 'functions/permission.php';

  include 'config.php';
  include 'module.php';
  include 'user.php';

  header('Content-Type: application/json');

  $username = isset($_GET['username']) ? $_GET['username'] : '';
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($username === '' || $page === '') {
    list($page, $username) = page_owner($url);
    $user = new User($username);
    $module = new Module($user, $username, 'detail');
    $page = $module->Factory('Action');
    if ($page === '') {
      echo json_encode('config not found.');
      exit;
    }
  }

  $url = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  $url .= $_SERVER['SERVER_NAME'];
  // status is a special case as it returns data via CORS rather than sending
  // the user to the page they've set in the Detail module.
  $status = $url.'/php/status.php';
  if ($username !== 'admin') $url .= '/'.$username;
  $url .= '/index.php?page='.$page;
  echo json_encode(['follow' => $url.'&follow={url}',
                    'like' => $url.'&like={url}',
                    'repost' => $url.'&repost={url}',
                    'reply' => $url.'&reply={url}',
                    'status' => $status.'?{url}']);
  exit;
}

include 'functions/session.php';

if (session_expired()) exit;

include 'functions/microformats.php';

header('Content-Type: application/json');
echo discover_endpoint($url, 'webaction');
