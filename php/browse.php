<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

// This is called because it's linked to the 'filebrowserBrowseUrl' config
// value when a ckeditor instance was created. Nothing happens here however
// (except returning 'CKEditorFuncNum') because ckeditor's filebrowser plugin
// has been modified so that the callback goes to 'dobrado.ckeditor'. From
// there dobrado's browser module can be opened, and ckeditor's filebrowser
// callback is triggered from there.
if (isset($_POST['CKEditorFuncNum'])) {
  echo json_encode(['callback' => $_POST['CKEditorFuncNum']]);
}
