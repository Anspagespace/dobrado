<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/db.php';
include 'functions/new_module.php';
include 'functions/permission.php';
include 'functions/write_style.php';
include 'functions/create.php';

// Check for required writeable directories here, and exit if necessary,
// otherwise the next section will not complete properly.
if (!is_writeable('..')) {
  log_db('The parent directory is not writeable.');
  exit;
}
if (!is_writeable('.')) {
  log_db('The current directory is not writeable.');
  exit;
}
if (!is_dir('../install')) mkdir('../install');
if (!is_writeable('../install')) {
  log_db('The \'install\' directory is not writeable.');
  exit;
}
if (!is_dir('../public')) {
  mkdir('../public');
}
if (!is_writeable('../public')) {
  log_db('The \'public\' directory is not writeable.');
  exit;
}
if (!is_writeable('../js')) {
  log_db('The \'js\' directory is not writeable.');
  exit;
}
if (!is_writeable('../js/source')) {
  log_db('The \'js/source\' directory is not writeable.');
  exit;
}
if (file_exists('../site.css') && !is_writeable('../site.css')) {
  log_db('The file \'site.css\' is not writeable.');
  exit;
}
if (!is_writeable('modules')) {
  log_db('The \'modules\' directory is not writeable.');
  exit;
}
if (file_exists('instance.php') && !is_writeable('instance.php')) {
  log_db('The file \'instance.php\' is not writeable.');
  exit;
}
// Create a cache directory for SimplePie.
if (!is_dir('cache')) mkdir('cache');
if (!is_writeable('cache')) {
  log_db('The \'cache\' directory is not writeable.');
  exit;
}

$mysqli = connect_db();

if ($mysqli_result = $mysqli->query('SHOW TABLES')) {
  if ($mysqli_result->num_rows !== 0) {
    $mysqli_result->close();
    $mysqli->close();
    log_db('start.php has already been run.');
    exit;
  }
}
else {
  log_db('start show tables:' . $mysqli->error);
  exit;
}

create_tables();

$password = $mysqli->escape_string(password_hash('admin', PASSWORD_DEFAULT));
// The empty string is used for the system_group here so that any template
// changes become the default and will be used for logged out users.
$query = 'INSERT INTO users VALUES ("admin", "' . $password . '", "", "", ' .
  '1, "", ' . time() . ', "", 1)';
if (!$mysqli->query($query)) {
  log_db('start insert user: ' . $mysqli->error);
}
// Add the 'admin' user to their own 'admin' group so that new accounts
// can be created, and give them permission to edit the group.
$query = 'INSERT INTO group_names VALUES ("admin", "admin", "admin", 1)';
if (!$mysqli->query($query)) {
  log_db('start insert group_names: ' . $mysqli->error);
}

$mysqli->close();

$default_modules =
  ['account' => ['title' => 'Account Menu', 'display' => 0],
   'control' => ['title' => 'Control Bar', 'display' => 0],
   'extended' => ['title' => 'Extended Editor', 'display' => 0],
   'login' => ['title' => 'Login Form', 'display' => 1],
   'more' => ['title' => '<b>more...</b>', 'display' => 0],
   'notification' => ['title' => 'Notifications','display' => 0],
   'organiser' => ['title' => 'Organiser', 'display' => 1],
   'simple' => ['title' => 'Text Area', 'display' => 1]];
create_default('../', '../', '', $default_modules);

// Add default site styles to the site_style table.
create_site_style();
// Write out site style information once the modules are installed.
write_site_style();

$scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
  'https://' : 'http://';
header('Location: ' . $scheme . $_SERVER['SERVER_NAME']);
