<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

if (!isset($_POST['label'])) {
  echo json_encode(['error' => 'label not provided']);
  exit;
}
if (!isset($_POST['url'])) {
  echo json_encode(['error' => 'url not provided']);
  exit;
}

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
$label = $mysqli->escape_string($_POST['label']);
$mysqli->close();

list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
$module = new Module($user, $owner, $label);
// Don't need to worry about permission for the current page if the module's
// placement is 'outside'.
$placement = $module->Placement();
if (!$module->IsInstalled()) {
  echo json_encode(['error' => $label . ' module is not available']);
  exit;
}

if ($module->CanAdd($page) && ($placement === 'outside' ||
                               $user->canEditPage ||
                               ($label === 'post' && $user->canViewPage))) {
  $id = 0;
  $group = $module->Group();
  // Modules that can be added here but have placement set to 'outside' are
  // only loaded for the current user. This means they do not get added to the
  // modules table, or are allowed to set per module settings via Add or do
  // any style updates via write_box_style (ie they are completely transient).
  // Also indieauth users need to add a post module, but it's not stored.
  if ($placement !== 'outside' &&
      !isset($user->settings['micropub']['endpoint'])) {
    // If adding a post module, the writer module can designate a different page
    // to add posts to for different actions, or so that a list of posts is
    // visible when used with a reader module on the same page.
    if ($label === 'post') {
      // Since a post is being added, can assume the Writer module is installed.
      $writer = new Writer($user, $owner);
      $page = $writer->Designate();
      $user->SetPermission($page, $owner);
    }
    // TODO: Return box_order from new_module and store it in result so that
    // the module can be added in the correct order on the page.
    $id = new_module($user, $owner, $label, $page, $group, $placement);
    $module->Add($id);
    // Write out a new style sheet after the module has been added.
    if ($owner === 'admin') write_box_style($owner, '../style.css');
    else write_box_style($owner, '../' . $owner . '/style.css');
  }

  // Doesn't matter if this module has an existing copy of this script
  // on the page, this is still an easy way to re-run the script.
  $script_prefix = $user->config->DevelopmentMode() ? 'source/' : '';
  $result = ['script' => $module->IncludeScript() ?
               $script_prefix . 'dobrado.' . $label . '.js' : false,
             'id' => 'dobrado-' . $id,
             'label' => $label,
             'content' => $module->Content($id),
             'style' => $module->Style($id),
             'group' => $group,
             'placement' => $placement];
  echo json_encode($result);
}
else {
  echo json_encode(['error' => 'could not add module']);
}
