<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function create_tables() {
  $mysqli = connect_db();

  $query = 'CREATE TABLE IF NOT EXISTS users (' .
    'user VARCHAR(50) NOT NULL,' .
    'password VARCHAR(123) NOT NULL,' .
    'email VARCHAR(100),' .
    'system_group VARCHAR(50),' .
    'confirmed TINYINT(1),' .
    'confirm_id VARCHAR(32),' .
    'registration_time INT(10) UNSIGNED,' .
    'from_guest VARCHAR(20),' .
    'active TINYINT(1),' .
    'PRIMARY KEY(user)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create users: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS modules (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200) NOT NULL,' .
    'box_id INT UNSIGNED NOT NULL AUTO_INCREMENT,' .
    'label VARCHAR(50) NOT NULL,' .
    'class TEXT,' .
    'box_order INT UNSIGNED,' .
    'placement ENUM("header", "left", "middle", ' .
      '"right", "footer", "outside") NOT NULL,' .
    'deleted TINYINT(1),' .
    'PRIMARY KEY(user, box_id, page(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create modules: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS modules_history (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200) NOT NULL,' .
    'box_id INT UNSIGNED NOT NULL AUTO_INCREMENT,' .
    'label VARCHAR(50) NOT NULL,' .
    'class TEXT,' .
    'box_order INT UNSIGNED,' .
    'placement ENUM("header", "left", "middle", ' .
      '"right", "footer", "outside") NOT NULL,' .
    'action ENUM("add", "delete", "move") NOT NULL,' .
    'modified_by VARCHAR(50),' .
    'timestamp INT(10) UNSIGNED NOT NULL,' .
    'PRIMARY KEY(user, box_id, page(50), timestamp)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create modules_history: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS box_style (' .
    'user VARCHAR(50) NOT NULL,' .
    'media VARCHAR(200) NOT NULL,' .
    'selector VARCHAR(200) NOT NULL,' .
    'property VARCHAR(200) NOT NULL,' .
    'value TEXT,' .
    'PRIMARY KEY(user, media(50), selector(50), property(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create box_style: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS page_style (' .
    'user VARCHAR(50) NOT NULL,' .
    'name VARCHAR(200) NOT NULL,' .
    'media VARCHAR(200) NOT NULL,' .
    'selector VARCHAR(200) NOT NULL,' .
    'property VARCHAR(200) NOT NULL,' .
    'value TEXT,' .
    'PRIMARY KEY(user, name(50), media(50), selector(50), property(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create page_style: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS site_style (' .
    'user VARCHAR(50) NOT NULL,' .
    'media VARCHAR(200) NOT NULL,' .
    'selector VARCHAR(200) NOT NULL,' .
    'property VARCHAR(200) NOT NULL,' .
    'value TEXT,' .
    'PRIMARY KEY(media(50), selector(100), property(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create site_style: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS installed_modules (' .
    'user VARCHAR(50) NOT NULL,' .
    'label VARCHAR(50) NOT NULL,' .
    'version VARCHAR(50),' .
    'title VARCHAR(50) NOT NULL,' .
    'description TEXT,' .
    'display TINYINT(1),' .
    'PRIMARY KEY(label)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create installed_modules: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS user_permission (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200) NOT NULL,' .
    'visitor VARCHAR(50) NOT NULL,' .
    'edit TINYINT(1),' .
    'copy TINYINT(1),' .
    'view TINYINT(1),' .
    'PRIMARY KEY(user, page(50), visitor)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create user_permission: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS group_permission (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200) NOT NULL,' .
    'name VARCHAR(250) NOT NULL,' .
    'edit TINYINT(1),' .
    'copy TINYINT(1),' .
    'view TINYINT(1),' .
    'PRIMARY KEY(user, page(50), name(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create group_permission: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS group_names (' .
    'user VARCHAR(50) NOT NULL,' .
    'name VARCHAR(250) NOT NULL,' .
    'visitor VARCHAR(50) NOT NULL,' .
    'edit_group TINYINT(1),' .
    'PRIMARY KEY(user, name(100), visitor)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create group_names: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS published (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200) NOT NULL,' .
    'published TINYINT(1) NOT NULL,' .
    'PRIMARY KEY(user, page)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create published: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS api (' .
    'user VARCHAR(50) NOT NULL,' .
    'box_id INT UNSIGNED NOT NULL,' .
    'label VARCHAR(50) NOT NULL,' .
    'feed_id INT UNSIGNED NOT NULL,' .
    'api_key INT UNSIGNED NOT NULL,' .
    'PRIMARY KEY(user, box_id)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create api: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS notify (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200),' .
    'box_id INT UNSIGNED NOT NULL,' .
    'label VARCHAR(50) NOT NULL,' .
    'action ENUM("add", "comment", "delete", "edit", "feed", "invite", ' .
      '"message", "move", "share", "star", "system", "spark", "tag") ' .
      'NOT NULL,' .
    'timestamp INT(10) UNSIGNED NOT NULL,' .
    'public TINYINT(1),' .
    'PRIMARY KEY(user, box_id, page(50), action)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create notify: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS page_updates (' .
    'user VARCHAR(50) NOT NULL,' .
    'page VARCHAR(200),' .
    'action ENUM("add", "comment", "delete", "edit", "feed", "invite", ' .
      '"message", "move", "share", "star", "system", "spark", "tag") ' .
      'NOT NULL,' .
    'timestamp INT(10) UNSIGNED NOT NULL,' .
    'PRIMARY KEY(user, page(50), action)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create page_updates: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS config (' .
    'user VARCHAR(50) NOT NULL,' .
    'analytics TEXT,' .
    'cron INT UNSIGNED,' .
    'development_mode TINYINT(1),' .
    'fancy_url TINYINT(1),' .
    'guest_allowed TINYINT(1),' .
    'login_page VARCHAR(200),' .
    'max_file_size INT UNSIGNED,' .
    'max_upload INT UNSIGNED,' .
    'permalink_default VARCHAR(200),' .
    'permalink_order INT UNSIGNED,' .
    'private_path VARCHAR(200),' .
    'secure TINYINT(1),' .
    'server_name VARCHAR(200),' .
    'theme VARCHAR(200),' .
    'timezone VARCHAR(50),' .
    'title VARCHAR(200),' .
    'title_includes_page TINYINT(1),' .
    'unavailable VARCHAR(200),' .
    'PRIMARY KEY(user)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create config: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS log (' .
    'user VARCHAR(50),' .
    'visitor VARCHAR(50),' .
    'page VARCHAR(200),' .
    'timestamp INT(10) UNSIGNED NOT NULL,' .
    'message TEXT,' .
    'PRIMARY KEY(user, visitor, timestamp)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create log: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS template (' .
    'label VARCHAR(200) NOT NULL,' .
    'system_group VARCHAR(50),' .
    'content TEXT,' .
    'PRIMARY KEY(label, system_group)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create template: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS template_description (' .
    'label VARCHAR(200) NOT NULL,' .
    'description TEXT,' .
    'PRIMARY KEY(label)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create template_description: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS script_version (' .
    'ckeditor INT UNSIGNED,' .
    '3rdparty INT UNSIGNED,' .
    'dobrado INT UNSIGNED' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create script_version: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS session (' .
    'user VARCHAR(50) NOT NULL,' .
    'series VARCHAR(32) NOT NULL,' .
    'token VARCHAR(32) NOT NULL,' .
    'PRIMARY KEY(user, series, token)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create session: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS nickname (' .
    'name VARCHAR(200) NOT NULL,' .
    'url VARCHAR(200) NOT NULL,' .
    'photo VARCHAR(200),' .
    'cache VARCHAR(200),' .
    'nickname VARCHAR(200),' .
    'reachable TINYINT(1),' .
    'PRIMARY KEY(url)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create nickname: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS settings (' .
    'user VARCHAR(50) NOT NULL,' .
    'label VARCHAR(50) NOT NULL,' .
    'name VARCHAR(200) NOT NULL,' .
    'value TEXT,' .
    'PRIMARY KEY(user, label, name(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create settings: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS setting_types (' .
    'label VARCHAR(50) NOT NULL,' .
    'name VARCHAR(200) NOT NULL,' .
    'value TEXT,' .
    'input_type ENUM("checkbox", "radio", "date", "password", "select", ' .
      '"text", "textarea") NOT NULL,' .
    'input_info TEXT,' .
    'PRIMARY KEY(label, name(50))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create setting_types: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS auth_codes (' .
    'me VARCHAR(200) NOT NULL,' .
    'code VARCHAR(64) NOT NULL,' .
    'client_id VARCHAR(200) NOT NULL,' .
    'redirect_uri VARCHAR(200) NOT NULL,' .
    'scope VARCHAR(100) NOT NULL,' .
    'response_type ENUM("id", "code") NOT NULL,' .
    'timestamp INT(10) UNSIGNED NOT NULL,' .
    'PRIMARY KEY(client_id(100), redirect_uri(100))' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create auth_codes: ' . $mysqli->error);
  }

  $query = 'CREATE TABLE IF NOT EXISTS access_tokens (' .
    'me VARCHAR(200) NOT NULL,' .
    'token VARCHAR(64) NOT NULL,' .
    'client_id VARCHAR(200) NOT NULL,' .
    'scope VARCHAR(100) NOT NULL,' .
    'PRIMARY KEY(token)' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('create access_tokens: ' . $mysqli->error);
  }

  $mysqli->close();
}

function create_site_style() {
  $mysqli = connect_db();

  // This is a default media query for mobile devices. Note '.dobrado-mobile'
  // visibility is used by javascript to do some dynamic configuration.
  $media = '@media screen and (max-device-width: 480px)';

  // Add some site wide style rules that aren't set by modules.
  $site_style = ['"","body","margin","0"',
                 '"","body","padding","0"',
                 '"","body","color","#222222"',
                 '"","body","font-family","Arial, sans-serif"',
                 '"","audio","max-width","100%"',
                 '"","iframe","max-width","100%"',
                 '"","video","max-width","100%"',
                 '"","video","max-height","500px"',
                 '"","img","max-width","100%"',
                 '"","img","height","auto"',
                 '"","img","border-style","none"',
                 '"","input[type=text]","padding","4px"',
                 '"","input[type=text]","border","1px solid #aaaaaa"',
                 '"","input[type=text]","border-radius","3px"',
                 '"","input[type=text]:focus","border-color","#32a3cf"',
                 '"","input[type=password]","padding","4px"',
                 '"","input[type=password]","border","1px solid #aaaaaa"',
                 '"","input[type=password]","border-radius","3px"',
                 '"","input[type=password]:focus","border-color","#32a3cf"',
                 '"","textarea","padding","4px"',
                 '"","textarea","border","1px solid #aaaaaa"',
                 '"","textarea","border-radius","3px"',
                 '"","textarea:focus","border-color","#32a3cf"',
                 '"",".form-spacing","margin-top","4px"',
                 '"",".form-spacing","margin-bottom","4px"',
                 '"",".form-spacing","clear","both"',
                 '"",".form-spacing > input[type=text]","height","25px"',
                 '"",".form-spacing > input[type=text]","width","300px"',
                 '"",".form-spacing > input[type=password]","height","25px"',
                 '"",".form-spacing > input[type=password]","width","300px"',
                 '"",".form-spacing > input[type=checkbox]","height","20px"',
                 '"",".form-spacing > input","max-width","100%"',
                 '"",".form-spacing > textarea","height","75px"',
                 '"",".form-spacing > textarea","width","300px"',
                 '"",".form-spacing > textarea","max-width","100%"',
                 '"",".form-spacing > select","height","30px"',
                 '"",".form-spacing > label","float","left"',
                 '"",".form-spacing > label","margin-top","0.3em"',
                 '"",".form-spacing > label","margin-right","0.3em"',
                 '"",".form-spacing > label","text-align","right"',
                 '"",".hidden","display","none"',
                 '"",".show","display","block"',
                 '"",".main > .header","min-height","100px"',
                 '"",".main > .left","position","relative"',
                 '"",".main > .left","left","0"',
                 '"",".main > .left","top","0"',
                 '"",".main > .left","width","0"',
                 '"",".main > .left","float","left"',
                 '"",".main > .right","position","relative"',
                 '"",".main > .right","right","0"',
                 '"",".main > .right","top","0"',
                 '"",".main > .right","width","0"',
                 '"",".main > .right","float","right"',
                 '"",".main > .middle","position","relative"',
                 '"",".main > .middle","width","760px"',
                 '"",".main > .middle","margin-left","auto"',
                 '"",".main > .middle","margin-right","auto"',
                 '"",".main > .middle","min-height","350px"',
                 '"",".main > .footer","clear","both"',
                 '"",".main > .footer","min-height","100px"',
                 '"",".dobrado-editable","min-height","100px"',
                 '"",".dobrado-editable.border","border","1px dashed #828282"',
                 '"",".cke_skin_kama a.cke_button_extended .cke_icon",' .
                   '"background-image","url(\"/images/extended.png\")"',
                 '"",".cke_skin_kama a.cke_button_extended .cke_icon",' .
                   '"background-position","center"',
                 '"",".photo-hidden img","display","none"',
                 '"",".photo-list img","width","45%"',
                 '"",".photo-list img","margin-right","10px"',
                 '"",".ui-sortable-placeholder","height","50px"',
                 '"",".ui-sortable-placeholder","border",' .
                   '"4px dashed #828282"',
                 '"",".ui-layout-resizer","background-color","#dddddd"',
                 '"",".ui-dialog","box-shadow",' .
                   '"20px 20px 10px rgba(0, 0, 0, 0.3)"',
                 '"",".ui-dialog","max-width","100%"',
                 '"",".no-close .ui-dialog-titlebar-close","display","none"',
                 '"",".ui-button-text-only .ui-button-text",' .
                   '"padding","0.1em 0.6em"',
                 '"",".ui-autocomplete","max-height","300px"',
                 '"",".ui-autocomplete","max-width","200px"',
                 '"",".ui-autocomplete","overflow-y","auto"',
                 '"",".ui-autocomplete","overflow-x","hidden"',
                 '"",".control .ui-button-icon-only","height","28px"',
                 '"",".control .ui-button-icon-only","width","28px"',
                 '"",".ui-tabs","padding","0"',
                 '"",".ui-tabs-nav li a","padding","0.3em 0.6em"',
                 '"",".ui-accordion-header a","padding","0.3em"',
                 '"",".ui-dialog-content","padding","0.5em 0"',
                 '"",".ui-spinner input[type=text]","border","none"',
                 '"",".ui-selectmenu-menu","position","fixed"',
                 '"",".ui-menu .ui-state-active","font-weight","normal"',
                 '"",".horizontal-menu:after","clear","both"',
                 '"",".horizontal-menu:after","content","\".\""',
                 '"",".horizontal-menu:after","display","block"',
                 '"",".horizontal-menu:after","height","0"',
                 '"",".horizontal-menu:after","line-height","0"',
                 '"",".horizontal-menu:after","visibility","hidden"',
                 '"",".horizontal-menu > .ui-menu-item","display",' .
                   '"inline-block"',
                 '"",".horizontal-menu > .ui-menu-item","float","left"',
                 '"",".horizontal-menu > .ui-menu-item","margin","0 15px"',
                 '"",".horizontal-menu > .ui-menu-item","padding","0"',
                 '"",".horizontal-menu > .ui-menu-item","width","auto"',
                 '"",".horizontal-menu > .ui-menu-item .ui-icon",' .
                   '"margin-top","0.3em"',
                 '"","indie-action","display","none"',
                 '"","indie-action","white-space","nowrap"',
                 '"","indie-action .ui-icon","display","inline-block"',
                 '"","indie-action .ui-icon","height","14px"',
                 '"","#indie-action-handler","width","220px"',
                 '"","#indie-action-submit","margin-left","10px"',
                 '"","#indie-action-twitter","margin-left","10px"',
                 '"","#indie-action-facebook","margin-left","10px"',
                 '"",".indie-config .ui-icon","display","inline-block"',
                 '"",".indie-config .ui-icon","height","14px"',
                 '"",".indie-config","white-space","nowrap"',
                 '"",".indie-config","font-size","0.7em"',
                 '"",".indie-config","margin-top","15px"',
                 '"",".indie-config-info","padding","5px"',
                 '"",".indie-config-settings","margin-left","10px"',
                 '"","#indie-config-ok","float","right"',
                 '"",".dobrado-mobile","display","none"',
                 '"' . $media . '",".dobrado-mobile","display","block"'];
  $values = '';
  foreach ($site_style as $style) {
    if ($values !== '') $values .= ',';
    $values .= '("admin",' . $style . ')';
  }
  $query = 'INSERT INTO site_style VALUES ' . $values;
  if (!$mysqli->query($query)) {
    log_db('create insert site_style: ' . $mysqli->error);
  }
  $mysqli->close();
}

function create_default($get_prefix, $put_prefix, $php_prefix, $modules) {
  $mysqli = connect_db();
  // Add default config settings. Can set server_name here, but don't allow
  // 'www' prefix because it's used to create email addresses too.
  $server_name = isset($_SERVER['SERVER_NAME']) ?
    preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']) : 'localhost';
  $unavailable = 'unavailable';
  $query = 'INSERT INTO config VALUES ("", "", 3600, 1, 0, 1, "index", 8, ' .
    '100, "default", 0, "/tmp", 0, "' . $server_name . '", "base-svg", ' .
    '"UTC", "dobrado", 1, "' . $unavailable . '")';
  if (!$mysqli->query($query)) {
    log_db('create insert config: ' . $mysqli->error);
  }
  // Add system template values. ('indieauth-group' is set here so that it
  // doesn't match the default group which is set to the empty string above.)
  $query = 'INSERT INTO template VALUES ' .
    '("system-sender", "", "noreply@!host"), ' .
    '("new-user-sender", "", "noreply@!host"), ' .
    '("new-user-subject", "", "New account"), ' .
    '("new-user-message", "", "Hi !username<p>Your account has been created ' .
      'on !server, you can now log in using this link: ' .
      '<a href=\"!server?code=!code\">!server?code=!code</a></p>"), ' .
    '("group-default-page", "", "default"), ' .
    '("indieauth-group", "", "indieauth")';
  if (!$mysqli->query($query)) {
    log_db('create insert template: ' . $mysqli->error);
  }
  // Add template descriptions for templates that aren't created by modules.
  $template_description =
    ['group-default-page' => 'The \'admin\' user\'s page to copy to the ' .
                             '\'index\' page for a new user.',
     'new-user-info' => 'A message to add to the new user email, substitutes ' .
                        '!reference.',
     'new-user-sender' => 'The \'from\' address for the email sent to new ' .
                          'users, must be on the domain.',
     'new-user-sender-name' => 'A descriptive name for the \'from\' address ' .
                               'for the email sent to new users.',
     'new-user-sender-cc' => 'The cc address to use when an email is sent ' .
                             'from the new-user-sender.',
     'new-user-sender-bcc' => 'The bcc address to use when an email is sent ' .
                              'from the new-user-sender.',
     'new-user-subject' => 'The subject for the email sent to new users.',
     'new-user-message' => 'The content of the email sent to new users, ' .
                           'substitutes !username, !server and !code.',
     'new-user-moderator' => 'An email address to send the new user ' .
                             'confirmation link to, rather than the new user.',
     'system-sender' => 'The default \'from\' address used to send email, ' .
                        'must be on the domain.',
     'system-sender-name' => 'The descriptive name used with the default ' .
                             '\'from\' address used to send email.',
     'system-sender-cc' => 'The cc address to use when an email is sent from ' .
                           'the system-sender.',
     'system-sender-bcc' => 'The bcc address to use when an email is sent ' .
                            'from the system-sender.'];
  $description_query = '';
  foreach ($template_description as $label => $description) {
    if ($description_query !== '') $description_query .= ',';
    $description_query .= '("' . $label . '", "' . $description . '")';
  }
  $query = 'INSERT INTO template_description VALUES ' . $description_query;
  if (!$mysqli->query($query)) {
    log_db('create insert template_description: ' . $mysqli->error);
  }
  $values = '';
  foreach ($modules as $label => $module_info) {
    if ($values !== '') $values .= ',';
    $values .= '("admin", "' . $label . '", "", ' .
      '"' . $module_info['title'] . '", "", ' . $module_info['display'] . ')';
  }
  $query = 'INSERT INTO installed_modules VALUES ' . $values;
  if (!$mysqli->query($query)) {
    log_db('create insert installed_modules: ' . $mysqli->error);
  }
  // Set the default 'unavailable' page to published, as well as the index page.
  $query = 'INSERT INTO published VALUES ("admin", "' . $unavailable . '", ' .
    '"1"), ("admin", "index", "1")';
  if (!$mysqli->query($query)) {
    log_db('create insert published: ' . $mysqli->error);
  }
  // Set script version numbers to zero so they can be updated later.
  $query = 'INSERT INTO script_version VALUES (0, 0, 0)';
  if (!$mysqli->query($query)) {
    log_db('create insert script_version: ' . $mysqli->error);
  }
  $mysqli->close();

  date_default_timezone_set('UTC');
  $date = date(DATE_COOKIE);
  // Javascript files for individual modules are concatenated into single
  // 'dobrado.js' and 'dobrado.pub.js' files. Start here with the core files.
  $content = "\n// File: core.pub.js\n// Added by: create.php\n// Date: " .
    $date . "\n";
  $content .= file_get_contents($get_prefix . 'js/core.pub.js');
  file_put_contents($put_prefix . 'js/dobrado.pub.js', $content);
  // Note that both core.js and core.pub.js are included in dobrado.js.
  $content .= "\n// File: core.js\n// Added by: create.php\n// Date: " . $date .
    "\n" . file_get_contents($get_prefix . 'js/core.js');
  file_put_contents($put_prefix . 'js/dobrado.js', $content);

  // Do the same for the 'source' files.
  $content = "\n// File: core.pub.js\n// Added by: create.php\n// Date: " .
    $date . "\n" . file_get_contents($get_prefix . 'js/source/core.pub.js');
  file_put_contents($put_prefix . 'js/source/dobrado.pub.js', $content);
  $content .= "\n// File: core.js\n// Added by: create.php\n// Date: " . $date .
    "\n" . file_get_contents($get_prefix . 'js/source/core.js');
  file_put_contents($put_prefix . 'js/source/dobrado.js', $content);

  // This function is called when running tests, so don't overwrite some files
  // when they already exist.
  if (!file_exists($get_prefix . '3rdparty.css')) {
    $handle = fopen($get_prefix . '3rdparty.css', 'w');
    foreach (scandir($get_prefix . 'css') as $file) {
      if (preg_match('/.*\.css$/', $file)) {
        $content = "\n/* File: " . $file . "\n Added by: create.php\n Date: " .
          $date . "*/\n";
        $content .= file_get_contents($get_prefix . 'css/' . $file);
        fwrite($handle, $content);
      }
    }
    fclose($handle);
  }

  if (!file_exists($php_prefix . 'instance.php')) {
    include $php_prefix . 'write_instance.php';
    write_instance();
    // instance.php gets written to the directory that bootstrap.php is called
    // from (the parent directory) which breaks some path assumptions, so move
    // it to the php directory.
    if ($php_prefix !== '') {
      rename('instance.php', $php_prefix . 'instance.php');
    }
  }
  // module.php is included after so that it includes the new instance.php.
  include $php_prefix . 'config.php';
  include $php_prefix . 'module.php';
  include $php_prefix . 'user.php';

  $owner = 'admin';
  $user = new User($owner);
  $user->SetPermission('index');

  foreach ($modules as $label => $module_info) {
    $module = new Module($user, $owner, $label);
    if (!$module->IsInstalled()) {
      // If IsInstalled returns false here it's because the module wasn't found
      // in instance.php. This can happen because a development version exists
      // and was not overwritten when running tests. In this case it's ok to
      // remove the file and ask the user to run the tests again.
      unlink($php_prefix . 'instance.php');
      echo 'An incomplete instance.php file was found. Please run tests again.';
      echo "\n";
      exit;
    }

    // This function is called by bootstrap.php to run tests, which looks for
    // javascript files in the tests directory. If a module includes javascript,
    // prepare for calling Install by copying the files. Prefer files in the js
    // directory but use the install directory if not found.
    if ($put_prefix === 'tests/' && $module->IncludeScript()) {
      if (file_exists('js/dobrado.' . $label . '.js')) {
        copy('js/dobrado.' . $label . '.js',
             'tests/js/dobrado.' . $label . '.js');
      }
      else {
        copy('install/dobrado.' . $label . '.js',
             'tests/js/dobrado.' . $label . '.js');
      }
      if (file_exists('js/source/dobrado.' . $label . '.js')) {
        copy('js/source/dobrado.' . $label . '.js',
             'tests/js/source/dobrado.' . $label . '.js');
      }
      else {
        copy('install/dobrado.' . $label . '.source.js',
             'tests/js/source/dobrado.' . $label . '.js');
      }
    }
    $module->Install($put_prefix . 'js');
    if ($label === 'login') {
      // Add a login module to the index page to get started.
      $id = new_module($user, $owner, $label, 'index', $module->Group(),
                       $module->Placement());
      $module->Add($id);
      // Write out a new style sheet after the module has been added.
      if (!file_exists($get_prefix . 'style.css')) {
        write_box_style($owner, $get_prefix . 'style.css');
      }
    }
  }
}