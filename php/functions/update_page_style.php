<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function all_pages() {
  $pages = array();
  // Don't select index page here because it's added by default even when not
  // found for the user in the modules table. (ie only empty page is listed).
  $mysqli = connect_db();
  $query = 'SELECT DISTINCT user, page FROM modules WHERE deleted = 0 AND '.
    'page != "index" ORDER BY user';
  if ($result = $mysqli->query($query)) {
    while ($modules = $result->fetch_assoc()) {
      $user = $modules['user'];
      if (!isset($pages[$user])) {
        $pages[$user] = array();
      }
      if ($modules['page'] === '') {
        $pages[$user][] = 'index';
      }
      else {
        $pages[$user][] = $modules['page'];
      }
    }
    $result->close();
  }
  else {
    log_db('all_pages: '.$mysqli->error);
  }
  $mysqli->close();
  return $pages;
}

function page_style_values($all_pages, $media, $selector, $property, $value) {
  $page_style_values = '';
  foreach ($all_pages as $user => $page) {
    for ($i = 0; $i < count($page); $i++) {
      if ($page_style_values !== '') {
        $page_style_values .= ', ';
      }
      $page_style_values .= '("'.$user.'", "'.$page[$i].'", '.
        '"'.$media.'", "'.$selector.'", "'.$property.'", "'.$value.'") ';
    }
  }
  return $page_style_values;
}

function update_page_style($all_pages) {
  foreach ($all_pages as $user => $page) {
    for ($i = 0; $i < count($page); $i++) {
      if ($user === 'admin') write_page_style($user, '../'.$page[$i].'.css');
      else write_page_style($user, '../'.$user.'/'.$page[$i].'.css');
    }
  }
}
