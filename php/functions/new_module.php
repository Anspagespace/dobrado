<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function new_module($user, $owner, $label, $page, $group, $placement) {
  $check = '';
  $box_order = 0;
  // If adding a 'post' or 'reader' module, check if there's a writer module
  // on the page and put the new module beneath it.
  if ($label === 'post' || $label === 'reader') {
    $box_order = 1;
    $check = 'writer';
  }
  // If adding a 'comment' module, put it above the 'commenteditor' module.
  // This will also put the new comment below older comments.
  else if ($label === 'comment') {
    $check = 'commenteditor';
  }
  // And if adding a 'commenteditor' module then it needs to go under a post
  // module if there's already one on the page.
  else if ($label === 'commenteditor') {
    $box_order = 1;
    $check = 'post';
  }

  $mysqli = connect_db();
  if ($check !== '') {
    $query = 'SELECT box_order FROM modules WHERE user = "' . $owner . '" ' .
      'AND label = "' . $check . '" AND page = "' . $page . '" AND ' .
      'placement = "' . $placement . '" AND deleted = 0';
    if ($result = $mysqli->query($query)) {
      if ($modules = $result->fetch_assoc()) {
        $box_order += (int)$modules['box_order'];
      }
      $result->close();
    }
    else {
      log_db('new_module 1: ' . $mysqli->error);
    }
  }
  if ($box_order === 0 && $label === 'comment') {
    // If there's no commenteditor module on the page, don't let the comment get
    // placed above the post as this affects what markup gets added to the page.
    $box_order = 2;
  }
  if ($placement !== 'outside') {
    // Need to update the box_order of boxes currently in the required column.
    $query = 'REPLACE INTO modules (user, page, box_id, label, class, ' .
      'box_order, placement, deleted) SELECT user, page, box_id, label, ' .
      'class, box_order + 1, placement, deleted FROM modules WHERE ' .
      'user = "' . $owner . '" AND page = "' . $page . '" AND ' .
      'box_order >= ' . $box_order . ' AND placement = "' . $placement . '" ' .
      'AND deleted = 0';
    if (!$mysqli->query($query)) {
      log_db('new_module 2: ' . $mysqli->error);
    }
  }
  $query = 'INSERT INTO modules (user, page, label, class, box_order, ' .
    'placement, deleted) VALUES ("' . $owner . '", "' . $page . '", ' .
    '"' . $label . '", "' . $group . '", ' . $box_order . ', ' .
    '"' . $placement . '", 0)';
  if (!$mysqli->query($query)) {
    log_db('new_module 3: ' . $mysqli->error);
  }
  $query = 'INSERT INTO modules_history (user, page, label, class, ' .
    'box_order, placement, action, modified_by, timestamp) VALUES (' .
    '"' . $owner . '", "' . $page . '", "' . $label . '", "' . $group . '", ' .
    $box_order . ', "' . $placement . '", "add", "' . $user->name . '", ' .
    time() . ')';
  if (!$mysqli->query($query)) {
    log_db('new_module 4: ' . $mysqli->error);
  }
  $id = $mysqli->insert_id;
  $mysqli->close();
  return $id;
}
