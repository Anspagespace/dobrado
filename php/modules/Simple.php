<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Simple extends Base {

  public function Add($id) {

  }

  public function Callback() {
    // mode is used by the Extended module, which calls this function.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
      // TODO: Use the custom area to search for multiple id's to update
      // content. (like site editor but just for the current user)
      return ['source' => $this->PlainContent($id), 'editor' => true];
    }
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<div class="dobrado-editable">'.$this->PlainContent($id).'</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $this->Insert($id, $this->PlainContent($old_id, $old_owner, true));
    $this->CopyStyle($id, $old_owner, $old_id);
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS simple_module ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'content TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'PRIMARY KEY(user, box_id)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->Install 1: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS simple_module_history ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'content TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'modified_by VARCHAR(50) NOT NULL,'.
      'PRIMARY KEY(user, box_id, timestamp)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->Install 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM simple_module WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Simple->Remove 1: '.$mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM simple_module WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Simple->Remove 2: '.$mysqli->error);
      }
      $query = 'DELETE FROM simple_module_history WHERE '.
        'user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Simple->Remove 3: '.$mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    if ($us_content['data'] === $this->PlainContent($id)) return;

    $us_data = '';
    if ($this->owner === 'admin') {
      $us_data = $us_content['data'];
    }
    else {
      include_once 'library/HTMLPurifier.auto.php';
      $config = HTMLPurifier_Config::createDefault();
      $config->set('Attr.AllowedRel', ['me']);
      $config->set('Attr.EnableID', true);
      $config->set('Attr.IDPrefix', 'anchor-');
      $config->set('Attr.AllowedFrameTargets', ['_blank']);
      // Allow iframes from youtube and vimeo.
      $config->set('HTML.SafeIframe', true);
      $config->set('URI.SafeIframeRegexp',
                   '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|'.
                   'player\.vimeo\.com/video/)%');
      $config->set('HTML.DefinitionID', 'dobrado-simple');
      $config->set('HTML.DefinitionRev', 1);
      if ($def = $config->maybeGetRawHTMLDefinition()) {
        // These attributes are both for lightbox markup.
        $def->addAttribute('a', 'data-lightbox', 'Text');
        $def->addAttribute('a', 'data-title', 'Text');
      }
      $purifier = new HTMLPurifier($config);
      $us_data = $purifier->purify($us_content['data']);
    }

    $time = time();
    $mysqli = connect_db();
    $data = $mysqli->escape_string($us_data);
    $query= 'INSERT INTO simple_module VALUES ("'.$this->owner.'", '.$id.', '.
      '"'.$data.'", '.$time.') ON DUPLICATE KEY UPDATE content = "'.$data.'", '.
      'timestamp = '.$time;
    if (!$mysqli->query($query)) {
      $this->Log("Simple->SetContent 1: ".$mysqli->error);
    }

    $query = 'INSERT INTO simple_module_history VALUES ("'.$this->owner.'", '.
      $id.', "'.$data.'", '.$time.', "'.$this->user->name.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->SetContent 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

  // Public functions that aren't part of interface here /////////////////////

  public function Search($search, $group) {
    // $data is used to preserve the original content for use with update
    // queries. The content shown to the user can't be used for this as it
    // may be modified by the browser.
    $data = [];
    $content = '';
    $current = '';
    $id = 0;
    $count = 0;
    $total = 0;
    $mysqli = connect_db();
    $query = 'SELECT content FROM simple_module LEFT JOIN users ON '.
      'simple_module.user = users.user WHERE content LIKE "%'.$search.'%" '.
      'AND users.system_group = "'.$group.'" ORDER BY content';
    if ($result = $mysqli->query($query)) {
      while ($simple_module = $result->fetch_assoc()) {
        if ($current !== $simple_module['content']) {
          if ($current !== '') {
            $matches = $count === 1 ? 'match' : 'matches';
            $content .= '<div class="search-match">'.
                '<b>'.$count.'</b> '.$matches.' found with content: '.
                '<button class="edit-content">edit content</button> '.
                '<button class="edit-style">edit style</button>'.
                '<div class="content" id="search-match-'.$id.'">'.$current.
                '</div>'.
              '</div>';
            $data[$id++] = $current;
          }
          $current = $simple_module['content'];
          $count = 0;
        }
        $count++;
        $total++;
      }
      $result->close();
    }
    else {
      $this->Log('Simple->Search: '.$mysqli->error);
    }
    // Add the final match to content.
    if ($current !== '') {
      $matches = $count === 1 ? 'match' : 'matches';
      $content .= '<div class="search-match">'.
          '<b>'.$count.'</b> '.$matches.' found with content: '.
          '<button class="edit-content">edit content</button> '.
          '<button class="edit-style">edit style</button>'.
          '<div class="content" id="search-match-'.$id.'">'.$current.'</div>'.
        '</div>';
      $data[$id] = $current;
    }
    $mysqli->close();
    if ($total === 0) {
      return ['content' => '<i>No results found.</i>'];
    }
    if ($total !== $count) {
      $matches = $total === 1 ? 'match' : 'matches';
      return ['content' => '<b>'.$total.'</b> '.$matches.' were found in '.
              'total.<br>'.$content, 'data' => $data];
    }
    return ['content' => $content, 'data' => $data];
  }

  public function UpdateContent($old, $new, $group) {
    $mysqli = connect_db();
    // Add the new content to history first, so that it can be matched against
    // the existing content.
    $query = 'INSERT INTO simple_module_history (user, box_id, content, '.
      'timestamp, modified_by) SELECT simple_module.user, box_id, "'.$new.'", '.
      time().', "'.$this->user->name.'" FROM simple_module LEFT JOIN users ON '.
      'simple_module.user = users.user WHERE content = "'.$old.'" AND '.
      'users.system_group = "'.$group.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->UpdateContent 1: '.$mysqli->error);
    }
    $query = 'UPDATE simple_module LEFT JOIN users ON '.
      'simple_module.user = users.user SET content = "'.$new.'" WHERE '.
      'content = "'.$old.'" AND users.system_group = "'.$group.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->UpdateContent 2: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function UpdateStyle($media, $selector,
                              $property, $value, $content, $group) {
    $mysqli = connect_db();
    // box_style doesn't store module id's because it's always part of the
    // selector. Can use the selector though to create the join query, where
    // the selector passed in is the generic section after the box_id.
    $query = 'INSERT INTO box_style (user, media, selector, property, value) '.
      'SELECT simple_module.user, "'.$media.'", CONCAT("#dobrado-", box_id, '.
      '" '.$selector.'"), "'.$property.'", "'.$value.'" FROM simple_module '.
      'LEFT JOIN users ON simple_module.user = users.user WHERE '.
      'content LIKE "%'.$content.'%" AND system_group = "'.$group.'" '.
      'ON DUPLICATE KEY UPDATE value = "'.$value.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->UpdateStyle 1: '.$mysqli->error);
    }
    // Don't really need to do the above query if value is empty, but it's an
    // easy way to work out what needs updating and makes for a simpler delete.
    if ($value === '') {
      $query = 'DELETE FROM box_style WHERE value = ""';
      if (!$mysqli->query($query)) {
        $this->Log('Simple->UpdateStyle 2: '.$mysqli->error);
      }
    }
    $mysqli->close();
  }

  // Private functions below here ////////////////////////////////////////////

  private function Insert($id, $content='') {
    $time = time();
    $mysqli = connect_db();
    $query = 'INSERT INTO simple_module VALUES '.
      '("'.$this->owner.'","'.$id.'","'.$content.'","'.$time.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->Insert 1: '.$mysqli->error);
    }

    $query = 'INSERT INTO simple_module_history VALUES ("'.$this->owner.'","'.
      $id.'","'.$content.'","'.$time.'","'.$this->user->name.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Simple->Insert 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  private function PlainContent($id, $user = '', $escape = false) {
    if ($user === '') {
      $user = $this->owner;
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT content FROM simple_module WHERE user = "'.$user.'" '.
      'AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($simple_module = $result->fetch_assoc()) {
        $content = $escape ? $mysqli->escape_string($simple_module['content']) :
          $simple_module['content'];
      }
      $result->close();
    }
    else {
      $this->Log('Simple->PlainContent: '.$mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

}
