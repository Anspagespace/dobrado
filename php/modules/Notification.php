<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Notification extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'exists') {
      return ['exists' => $this->Exists()];
    }
    if ($us_action === 'remove') {
      $this->DeleteNotifications();
      return ['done' => true];
    }
    if ($us_action === 'new') {
      return ['content' => $this->NewNotifications()];
    }
    if ($us_action === 'history') {
      $object = $this->History();
      // Also send back the current notification settings.
      $object['settings'] = $this->Settings();
      return $object;
    }
    if ($us_action === 'addToGroup') {
      return $this->AddToGroup();
    }
    if ($us_action === 'removeFromGroup') {
      $this->RemoveFromGroup();
      return ['done' => true];
    }
    if ($us_action === 'addDefault') {
      return $this->AddDefault();
    }
    if ($us_action === 'removeDefault') {
      return $this->RemoveDefault();
    }
  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    // Only the owner can see their notifications.
    if ($this->user->name !== $this->owner) {
      return false;
    }

    $title = '';
    $description = '';
    $author = '';
    $category = '';
    $enclosure = '';
    $permalink = '';
    $timestamp = 0;
    $mysqli = connect_db();
    $query = 'SELECT title, description, author, author_photo, author_url, ' .
      'category, enclosure, permalink, timestamp FROM notification WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id;
    if ($result = $mysqli->query($query)) {
      if ($notification = $result->fetch_assoc()) {
        $title = $notification['title'];
        $description = $notification['description'];
        $author = '';
        $author_name = $notification['author'];
        $author_photo = $notification['author_photo'];
        $author_url = $notification['author_url'];
        if ($author_photo !== '') {
          $author = $author_url !== '' ?
            '<a href="' . $author_url . '">' . $author_photo . '</a>' :
            $author_photo;
        }
        if ($author_name === '') {
          if (preg_match('/^https?:\/\/([^\/]+)/i', $author_url, $match)) {
            $author_name = $match[1];
          }
          else {
            $author_name = 'someone';
          }
          $author .= $author_url !== '' ?
            '<a class="u-author h-card" href="' . $author_url . '">' .
              $author_name . '</a>' : $author_name;
        }
        else {
          $author .= $author_url !== '' ?
            '<a class="u-author h-card" href="' . $author_url . '">' .
              $author_name . '</a>' : $author_name;
        }
        $category = $notification['category'];
        $enclosure = $notification['enclosure'];
        $permalink = $notification['permalink'];
        $timestamp = (int)$notification['timestamp'];
      }
      $result->close();
    }
    else {
      $this->Log('Notification->Content: ' . $mysqli->error);
    }
    $mysqli->close();

    // Allow different substitutions depending on category and also url,
    // since permalink could actually be the user's home page.
    if ($category === 'tag') {
      if ($permalink === $this->Url('', 'index')) $category = 'tag-me';
    }
    else if ($category === 'follow') {
      if ($permalink === $this->Url('', 'index')) $category = 'follow-me';
    }
    else if ($category === 'tag-comment') {
      // This category is used so that entries marked as person tags can still
      // be added as a comment, so it's ok to change it back here.
      $category = 'tag-person';
    }
    return $this->Substitute('notification-' . $category,
                             ['/!title/', '/!description/', '/!author/',
                              '/!category/', '/!enclosure/',
                              '/!permalink/', '/!timestamp/'],
                             [$title, $description, $author, $category,
                              $enclosure, $permalink,
                              date('g:i:sa \o\n D F j, Y', $timestamp)]);
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {
    return 'notifications';
  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS notification (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'title VARCHAR(180),' .
      'description TEXT,' .
      'author VARCHAR(50),' .
      'author_photo VARCHAR(200),' .
      'author_url VARCHAR(200),' .
      'category VARCHAR(200),' .
      'enclosure TEXT,' .
      'permalink VARCHAR(200),' .
      'feed VARCHAR(200),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Notification->Install 1: ' . $mysqli->error);
    }
    // This table is used when a new user is created. If they're in a group that
    // is listed here, they will start receiving notifications for that page.
    $query = 'CREATE TABLE IF NOT EXISTS notification_defaults (' .
      'user VARCHAR(50) NOT NULL,' .
      'page VARCHAR(200) NOT NULL,' .
      'group_name VARCHAR(250) NOT NULL,' .
      'PRIMARY KEY(user, page(50), group_name(50))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Notification->Install 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $template = ['"notification-system","","System message from ' .
                   '<b>!author</b>:<br>!description' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-feed","","!author added a new post to ' .
                   '<a href=\"!permalink\">!description</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-comment","","!author commented on ' .
                   '<a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-invite","","!author has invited your ' .
                   'group to <a href=\"!permalink\">join ' .
                   '<b>!description</b></a>"',
                 '"notification-tag","","!author <a href=\"!description\">' .
                   'mentioned</a> <a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-tag-me","","!author mentioned you in ' .
                   '<a href=\"!description\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-tag-person","","!author tagged you in ' .
                   '<a href=\"!description\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-follow","","!author <a href=\"!description\">' .
                   'started following</a> <a href=\"!permalink\">your feed</a>'.
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-follow-me","","!author ' .
                   '<a href=\"!description\">started following</a> you' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-star","","!author ' .
                   '<a href=\"!description\">liked</a> ' .
                   '<a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-share","","!author ' .
                   '<a href=\"!description\">shared</a> ' .
                   '<a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">' .
                   'At !timestamp</div>"',
                 '"notification-comment-mention","","!author ' .
                   '<a href=\"!description\">replied to a comment</a> that ' .
                   'mentions <a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-like-mention","","!author ' .
                   '<a href=\"!description\">likes a comment</a> that ' .
                   'mentions <a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"',
                 '"notification-share-mention","","!author ' .
                   '<a href=\"!description\">shared a comment</a> that ' .
                   'mentions <a href=\"!permalink\">this post</a>' .
                   '<div class=\"notification-date\">At !timestamp</div>"'];
    $this->AddTemplate($template);
    $description = ['notification-system' => 'The format used for system ' .
                      'notifications, substitutes: !author, !description, ' .
                      '!timestamp.',
                    'notification-feed' => 'The format used for new post ' .
                      'notifications, substitutes: !author, !permalink, ' .
                      '!description, !timestamp.',
                    'notification-comment' => 'The format used for new ' .
                      'comment notifications, substitutes: !author, ' .
                      '!permalink, !description, !timestamp.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"",".notifications","display","none"',
                   '"",".notification-date","white-space","nowrap"',
                   '"",".notification-group","padding-bottom","5px"',
                   '"",".old-notification","background-color","#eeeeee"',
                   '"",".old-notification","border","1px solid #aaaaaa"',
                   '"",".old-notification","border-radius","2px"',
                   '"",".old-notification","margin-bottom","4px"',
                   '"",".old-notification","padding","4px"',
                   '"",".old-notification .thumb","width","20px"',
                   '"",".old-notification .thumb","border-radius","2px"',
                   '"",".old-notification a","text-decoration","none"',
                   '"",".old-notification a","color","#aaaaaa"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM notification WHERE user = "' . $this->owner . '" ' .
        'AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Notification->Remove 1: ' . $mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM notification WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Notification->Remove 2: ' . $mysqli->error);
      }
      $query = 'DELETE FROM notification_defaults WHERE ' .
        'user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Notification->Remove 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    $title = '';
    $description = '';
    $author = '';
    $author_photo = '';
    $author_url = '';
    $category = '';
    $enclosure = '';
    $permalink = '';
    $feed = '';
    $public = 0;

    $mysqli = connect_db();
    if (isset($us_content['title'])) {
      $title = $mysqli->escape_string($us_content['title']);
    }
    if (isset($us_content['description'])) {
      $description = $mysqli->escape_string($us_content['description']);
    }
    if (isset($us_content['author'])) {
      $author = $mysqli->escape_string($us_content['author']);
    }
    if (isset($us_content['author_photo'])) {
      $author_photo = $mysqli->escape_string($us_content['author_photo']);
    }
    if (isset($us_content['author_url'])) {
      $author_url = $mysqli->escape_string($us_content['author_url']);
    }
    if (isset($us_content['category'])) {
      $category = $mysqli->escape_string($us_content['category']);
    }
    if (isset($us_content['enclosure'])) {
      $enclosure = $mysqli->escape_string($us_content['enclosure']);
    }
    if (isset($us_content['permalink'])) {
      $permalink = $mysqli->escape_string($us_content['permalink']);
    }
    if (isset($us_content['feed'])) {
      $feed = $mysqli->escape_string($us_content['feed']);
    }
    if (isset($us_content['public'])) {
      $public = $mysqli->escape_string($us_content['public']);
    }

    $query = 'INSERT INTO notification VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $title . '", "' . $description . '", "' . $author . '", ' .
      '"' . $author_photo . '", "' . $author_url . '", "' . $category . '", ' .
      '"' . $enclosure . '", "' . $permalink . '", "' . $feed . '", ' . time() .
      ') ON DUPLICATE KEY UPDATE title = "' . $title . '", ' .
      'description = "' . $description . '", author = "' . $author . '", ' .
      'author_photo = "' . $author_photo . '", ' .
      'author_url = "' . $author_url . '", category = "' . $category . '", ' .
      'enclosure = "' . $enclosure . '", permalink = "' . $permalink . '", ' .
      'feed = "' . $feed . '", timestamp = ' . time();
    if (!$mysqli->query($query)) {
      $this->Log('Notification->SetContent: ' . $mysqli->error);
    }
    $mysqli->close();

    // Create the system notification once the item has been added to the
    // notification table. Note 'tag' can be extended as a category type but
    // the Notify system only has a limited set of labels to use.
    if ($category === 'follow' || strpos($category, 'tag') === 0 ||
        strpos($category, 'mention') !== false) {
      $category = 'tag';
    }
    $this->Notify($id, 'notification', $category, $feed, $public);
  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

  // Public functions that aren't part of interface here /////////////////////

  public function AddMember() {
    $mysqli = connect_db();
    $query = 'INSERT IGNORE INTO group_names (user, name, visitor, ' .
      'edit_group) SELECT user, CONCAT("post-notifications-", page), ' .
      '"' . $this->user->name . '", 0 FROM notification_defaults WHERE ' .
      'group_name = "' . $this->user->group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Notification->AddMember: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function ExistingUrl($us_url) {
    $exists = false;
    $mysqli = connect_db();
    $url = $mysqli->escape_string($us_url);
    // Notifications that need to check for existing urls also store the
    // source url in the description.
    $query = 'SELECT box_id FROM notification WHERE ' .
      'description = "' . $url . '" AND user = "' . $this->owner . '" ' .
      'AND permalink = "' . $this->user->page . '"';
    if ($result = $mysqli->query($query)) {
      $exists = $result->num_rows !== 0;
    }
    else {
      $this->Log('Notification->ExistingUrl: ' . $mysqli->error);
    }
    $mysqli->close();
    return $exists;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddDefault() {
    // Need permission to edit notifications for other users, but since the
    // Writer module only needs view permission to add new posts, make the
    // permissions the same to update notifications.
    if (!$this->user->canViewPage) {
      return ['error' => 'Permission denied adding default.'];
    }

    // A group name is only passed in when the user has site permission,
    // or there's more than one group in the organisation. In the latter
    // case make sure the group passed in is in the organisation.
    $group = $this->user->group;
    $organiser = new Organiser($this->user, $this->owner);
    $siblings = $organiser->Siblings();

    $mysqli = connect_db();
    if ($this->user->canEditSite) {
      $group = $mysqli->escape_string($_POST['group']);
    }
    else if (count($siblings) > 1) {
      $group = $mysqli->escape_string($_POST['group']);
      if (!in_array($group, $siblings)) {
        $mysqli->close();
        return ['error' => 'Unknown default group.'];
      }
    }

    // If the page isn't published, the group must have permission to access
    // the page to allow notifications. Notifications are added for all members
    // of system_group = $group below (so that they can remove notifications
    // individually if they want to), but the permission group containing all
    // the members of this system group is actually 'all-' . $group.
    $permission = false;
    if ($this->Published($this->user->page, $this->owner) === 1) {
      $permission = true;
    }
    else {
      $query = 'SELECT name FROM group_permission WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '" '.
        'AND name = "all-' . $group .'" AND (edit = 1 OR copy = 1 OR view = 1)';
      if ($result = $mysqli->query($query)) {
        $permission = $result->num_rows === 1;
      }
      else {
        $this->Log('Notification->AddDefault 1: ' . $mysqli->error);
      }
    }

    $object = [];
    if (!$permission) {
      $object['error'] = 'Group doesn\'t have permission to access page.';
    }
    else {
      $query = 'SELECT group_name FROM notification_defaults WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '" '.
        'AND group_name = "' . $group . '"';
      if ($result = $mysqli->query($query)) {
        $notification_exists = $result->num_rows !== 0;
        $result->close();
      }
      else {
        $this->Log('Notification->AddDefault 2: ' . $mysqli->error);
      }
      if ($notification_exists) {
        $object['error'] = 'Notification already exists.';
      }
      else {
        $query = 'INSERT INTO notification_defaults VALUES ' .
          '("' . $this->owner . '", "' . $this->user->page . '", "'.$group.'")';
        if (!$mysqli->query($query)) {
          $this->Log('Notification->AddDefault 3: ' . $mysqli->error);
        }
        $query = 'INSERT IGNORE INTO group_names (user, name, visitor, ' .
          'edit_group) SELECT "' . $this->owner . '", ' .
          '"post-notifications-' . $this->user->page . '", users.user, 0 FROM '.
          'users WHERE users.system_group = "' . $group . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Notification->AddDefault 4: ' . $mysqli->error);
        }
        if (!$this->user->canEditSite && count($siblings) === 1) {
          $group = 'Users are notified about new posts.';
          $object['hideOptions'] = true;
        }
        $object['content'] = '<div class="notification-group">' .
            '<button class="remove-default-group">remove</button> ' .
            '<span class="default">' . $group . '</span>' .
          '</div>';
      }
    }
    $mysqli->close();
    return $object;
  }

  private function AddToGroup() {
    $mysqli = connect_db();
    $object = [];
    // First check that the user has permission to access the page.
    $permission = $this->owner === $this->user->name;
    // If not the owner of the page check if it's published.
    if (!$permission) {
      $permission = $this->Published($this->user->page, $this->owner) === 1;
    }
    // If not published check if this user is in a permission group.
    if (!$permission) {
      $query = 'SELECT visitor FROM user_permission WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '" '.
        'AND (visitor = "' . $this->user->name . '" OR visitor = "") AND ' .
        '(edit = 1 OR copy = 1 OR view = 1)';
      if ($result = $mysqli->query($query)) {
        $permission = $result->num_rows === 1;
        $result->close();
      }
      else {
        $this->Log('Notification->AddToGroup 1: ' . $mysqli->error);
      }
    }
    if (!$permission) {
      $query = 'SELECT DISTINCT visitor FROM group_names INNER JOIN ' .
        'group_permission ON group_names.name = group_permission.name AND ' .
        'group_names.user = group_permission.user WHERE ' .
        'group_names.user = "' . $this->owner . '" AND ' .
        'group_permission.page = "' . $this->user->page . '" ' .
        'AND visitor = "' . $this->user->name . '" AND ' .
        '(edit = 1 OR copy = 1 OR view = 1)';
      if ($result = $mysqli->query($query)) {
        $permission = $result->num_rows === 1;
        $result->close();
      }
      else {
        $this->Log('Notification->AddToGroup 2: ' . $mysqli->error);
      }
    }
    if ($permission) {
      // type is either comment or post, if there's a commenteditor module on
      // the page make the type comment.
      $type = 'post';
      $query = 'SELECT label FROM modules WHERE user = "' . $this->owner . '" '.
        'AND page = "' . $this->user->page . '" AND label = "commenteditor" ' .
        'AND deleted = 0';
      if ($result = $mysqli->query($query)) {
        if ($result->num_rows !== 0) {
          $type = 'comment';
          $result->close();
        }
      }
      else {
        $this->Log('Notification->AddToGroup 3: ' . $mysqli->error);
      }
      if ($this->owner !== '' && $this->user->page !== '') {
        $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
          '"' . $type . '-notifications-' . $this->user->page . '", ' .
          '"' . $this->user->name . '", 0) ON DUPLICATE KEY UPDATE ' .
          'edit_group = 0';
        if (!$mysqli->query($query)) {
          $this->Log('Notification->AddToGroup 3: ' . $mysqli->error);
        }
      }
      $link = $this->owner === $this->user->name ?
        $this->user->page : $this->owner . '/' . $this->user->page;
      $group = '<div class="notification-group">' .
          '<button class="remove-notification-group">remove</button> ' .
          '<a class="' . $type . '-notification" ' .
            'href="' . $this->Url('', $this->user->page, $this->owner) . '">' .
            $link . '</a>' .
        '</div>';
      $object = ['type' => $type, 'group' => $group];
    }
    else {
      $object = ['error' => 'Permission denied adding notification.'];
    }
    $mysqli->close();
    return $object;
  }

  private function DeleteNotifications() {
    $mysqli = connect_db();
    $query = 'UPDATE modules SET deleted = 1 WHERE label = "notification" ' .
      'AND user = "' . $this->user->name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Notification->DeleteNotifications: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function Exists() {
    $exists = false;
    $mysqli = connect_db();
    // Check if a post or comment notification exists for the current page.
    $query = 'SELECT DISTINCT visitor FROM group_names WHERE ' .
      '(name = "post-notifications-' . $this->user->page . '" OR ' .
      'name = "comment-notifications-' . $this->user->page . '") AND ' .
      'user = "' . $this->owner . '" AND visitor = "' . $this->user->name . '"';
    if ($result = $mysqli->query($query)) {
      $exists = $result->num_rows === 1;
      $result->close();
    }
    else {
      $this->Log('Notification->Exists: ' . $mysqli->error);
    }
    $mysqli->close();
    return $exists;
  }

  private function History() {
    $object = [];
    $history = '';
    // Content function needs owner to be current user to show notifications.
    $owner = $this->owner;
    $this->owner = $this->user->name;

    $mysqli = connect_db();
    $count = (int)$_POST['count'];
    $query = 'SELECT box_id FROM modules WHERE label = "notification" AND ' .
      'user = "' . $this->user->name . '" ORDER BY box_id DESC ' .
      'LIMIT ' . $count . ', ' . ($count + 10);
    if ($result = $mysqli->query($query)) {
      while ($modules = $result->fetch_assoc()) {
        $content = $this->Content($modules['box_id']);
        $history .= '<div class="old-notification">' . $content . '</div>';
        $count++;
      }
      $result->close();
    }
    else {
      $this->Log('Notification->History: ' . $mysqli->error);
    }
    $mysqli->close();

    $this->owner = $owner;
    $object['history'] = $history;
    $object['count'] = $count;
    return $object;
  }

  private function NewNotifications() {
    // Content function needs owner to be current user to show notifications.
    $owner = $this->owner;
    $this->owner = $this->user->name;
    $notifications = '';
    $mysqli = connect_db();
    $query = 'SELECT box_id FROM modules WHERE label = "notification" AND ' .
      'user = "' . $this->user->name . '" AND deleted = 0 ORDER BY box_id DESC';
    if ($result = $mysqli->query($query)) {
      while ($modules = $result->fetch_assoc()) {
        $content = $this->Content($modules['box_id']);
        $notifications .= '<div class="notification">' . $content . '</div>';
      }
      $result->close();
    }
    else {
      $this->Log('Notification->NewNotifications: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->owner = $owner;
    return $notifications;
  }

  private function RemoveDefault() {
    // Need permission to edit notifications for other users, but since the
    // Writer module only needs view permission to add new posts, make the
    // permissions the same to update notifications.
    if (!$this->user->canViewPage) {
      return ['error' => 'Permission denied removing default.'];
    }

    // A group name is only passed in when the user has site permission,
    // or there's more than one group in the organisation. In the latter
    // case make sure the group passed in is in the organisation.
    $group = $this->user->group;
    $organiser = new Organiser($this->user, $this->owner);
    $siblings = $organiser->Siblings();

    $mysqli = connect_db();
    if ($this->user->canEditSite) {
      $group = $mysqli->escape_string($_POST['group']);
    }
    else if (count($siblings) > 1) {
      $group = $mysqli->escape_string($_POST['group']);
      if (!in_array($group, $siblings)) {
        $mysqli->close();
        return ['error' => 'Unknown default group.'];
      }
    }

    $query = 'DELETE FROM notification_defaults WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $this->user->page . '" ' .
      'AND group_name = "' . $group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Notification->RemoveDefault: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function RemoveFromGroup() {
    $mysqli = connect_db();
    $type = $mysqli->escape_string($_POST['type']);
    $group = $mysqli->escape_string($_POST['group']);
    $owner = '';
    $page = '';
    preg_match('/^([a-z0-9_-]+)\/?([a-z0-9_-]*)$/i', $group, $matches);
    if (count($matches) === 3) {
      // Note that count will always be 3 if there's a match because the second
      // match is allowed to be empty.
      if ($matches[2] === '') {
        $owner = $this->user->name;
        $page = $matches[1];
      }
      else {
        $owner = $matches[1];
        $page = $matches[2];
      }
    }
    if ($owner !== '' && $page !== '') {
      $query = 'DELETE FROM group_names WHERE user = "' . $owner . '" AND ' .
        'name = "' . $type . '-notifications-' . $page . '" AND ' .
        'visitor = "' . $this->user->name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Notification->RemoveFromGroup: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  private function Settings() {
    $settings = '';
    $writer = false;
    $mysqli = connect_db();
    // If there's a writer module on the page allow the user to edit the
    // default notification groups for new posts.
    $query = 'SELECT label FROM modules WHERE user = "' . $this->owner . '" ' .
      'AND page = "' . $this->user->page . '" AND label = "writer"';
    if ($result = $mysqli->query($query)) {
      $writer = $result->num_rows !== 0;
      $result->close();
    }
    else {
      $this->Log('Notification->Settings 1: ' . $mysqli->error);
    }
    if ($writer) $settings .= $this->WriterSettings();

    // Get the current post notifications for this user.
    $post_notifications = '';
    $name = 'post-notifications-';
    $query = 'SELECT user, name FROM group_names WHERE name LIKE ' .
      '"' . $name . '%" AND visitor = "' . $this->user->name . '"';
    if ($result = $mysqli->query($query)) {
      while ($group_names = $result->fetch_assoc()) {
        $regex = '/^' . $name . '(.+)$/';
        if (preg_match($regex, $group_names['name'], $matches)) {
          $page = $matches[1];
          $user = $group_names['user'];
          // Just show the page name if it belongs to the current user.
          $link = $user === $this->user->name ? $page : $user . '/' . $page;
          $post_notifications .= '<div class="notification-group">' .
              '<button class="remove-notification-group">remove</button> ' .
              '<a class="post-notification" ' .
                'href="' . $this->Url('', $page, $user) . '">' . $link . '</a>'.
            '</div>';
        }
      }
      $result->close();
    }
    else {
      $this->Log('Notification->Settings 2: ' . $mysqli->error);
    }

    // Next get the current comment notifications for this user.
    $comment_notifications = '';
    $name = 'comment-notifications-';
    $query = 'SELECT user, name FROM group_names WHERE name ' .
      'LIKE "' . $name . '%" AND visitor = "' . $this->user->name . '"';
    if ($result = $mysqli->query($query)) {
      while ($group_names = $result->fetch_assoc()) {
        $regex = '/^' . $name . '(.+)$/';
        if (preg_match($regex, $group_names['name'], $matches)) {
          $page = $matches[1];
          $user = $group_names['user'];
          // Just show the page name if it belongs to the current user.
          $link = $user === $this->user->name ? $page : $user . '/' . $page;
          $comment_notifications .= '<div class="notification-group">' .
              '<button class="remove-notification-group">remove</button> ' .
              '<a class="comment-notification" ' .
                'href="' . $this->Url('', $page, $user) . '">' . $link . '</a>'.
            '</div>';
        }
      }
      $result->close();
    }
    else {
      $this->Log('Notification->Settings 3: ' . $mysqli->error);
    }
    $mysqli->close();

    // Only show the group if there are notifications.
    $hidden = $post_notifications !== '' ? '' : ' hidden';
    $settings .= '<div class="notifications-post-group' . $hidden . '">' .
        '<h4>Your notifications for new posts:</h4>' .
        $post_notifications .
      '</div>';
    $hidden = $comment_notifications !== '' ? '' : ' hidden';
    $settings .= '<div class="notifications-comment-group' . $hidden . '">' .
        '<h4>Your notifications for comments:</h4>' .
        $comment_notifications .
      '</div>';
    return $settings;
  }

  function WriterSettings() {
    // Need permission to edit notifications for other users, but since the
    // Writer module only needs view permission to add new posts, make the
    // permissions the same to update notifications.
    if (!$this->user->canViewPage) return '';

    $defaults = '';
    $group_content = '';
    $hidden = '';
    $current = [];
    $organiser = new Organiser($this->user, $this->owner);
    $siblings = $organiser->Siblings();
    // Only show groups if there is more than one, otherwise just ask if
    // all users should be notified about new posts.
    $mysqli = connect_db();
    $query = 'SELECT group_name FROM notification_defaults WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"';
    if ($result = $mysqli->query($query)) {
      while ($notification_defaults = $result->fetch_assoc()) {
        $group = $notification_defaults['group_name'];
        if ($this->user->canEditSite || in_array($group, $siblings)) {
          $current[] = $group;
          // Change the text when there is only one group.
          if (!$this->user->canEditSite && count($siblings) === 1) {
            $group = 'Users are notified about new posts.';
          }
          $defaults .= '<div class="notification-group">' .
            '<button class="remove-default-group">remove</button> ' .
            '<span class="default">' . $group . '</span>' .
            '</div>';
        }
      }
      $result->close();
    }
    else {
      $this->Log('Notification->WriterSettings: ' . $mysqli->error);
    }
    $mysqli->close();

    // Show an input for groups rather than a select if user has permission.
    if ($this->user->canEditSite) {
      $group_content = '<div class="form-spacing">' .
          '<label for="notification-group-input">Add a group:</label>' .
          '<input id="notification-group-input" maxlength="50">' .
          '<button class="submit">submit</button>' .
        '</div>';
    }
    else if (count($siblings) === 1) {
      // Only show content when a notification isn't already been set.
      if (count($current) === 1) {
        $hidden = ' hidden';
      }
      $group_content = '<div class="form-spacing">' .
          'Notify other users about new posts? ' .
          '<button class="submit">submit</button>' .
        '</div>';
    }
    else {
      // Only show content when all notifications aren't already been set.
      if (count($siblings) === count($current)) {
        $hidden = ' hidden';
      }
      $options = '';
      for ($i = 0; $i < count($siblings); $i++) {
        $group = $siblings[$i];
        $options .= '<option value="' . $group . '">' . ucfirst($group) .
          '</option>';
      }
      $group_content = '<div class="form-spacing">' .
          '<label for="notification-group-input">Add a group:</label>' .
          '<select id="notification-group-input">' . $options . '</select> ' .
          '<button class="submit">submit</button>' .
        '</div>';
    }
    return '<div class="notification-default-group">' .
        '<h4>Groups notifications for this page:</h4>' .
        '<div class="list">' . $defaults . '</div>' .
        '<div class="options' . $hidden . '">' . $group_content . '</div>' .
      '</div>';
  }

}
