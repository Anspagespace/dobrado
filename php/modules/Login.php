<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Login extends Base {

  public function Add($id) {
    // Add some default style rules for each Login module.
    $selector = '#dobrado-' . $id;
    $box_style = ['"","' . $selector . '","background-color","#999999"',
                  '"","' . $selector . '","border","2px solid #444444"',
                  '"","' . $selector . '","border-radius","0.2em"',
                  '"","' . $selector . '","margin","0.6em"',
                  '"","' . $selector . '","padding","0.4em"',
                  '"","' . $selector . '","width","500px"',
                  '"","' . $selector . '","color","#ffffff"',
                  '"","' . $selector . ' a","color","#222222"',
                  '"","' . $selector . ' a","text-decoration","none"',
                  '"","' . $selector . ' a:hover","color","#dddddd"',
                  '"","' . $selector . ' label","width","8.2em"',
                  '"","' . $selector . ' label","margin-top","0.3em"',
                  '"","' . $selector . ' .submit","margin-left","8em"',
                  '"","' . $selector . ' .login-title","font-weight","bold"',
                  '"","' . $selector . ' .email-reset","display","none"',
                  '"","' . $selector . ' .login-main","overflow","hidden"'];
    $this->AddBoxStyle($box_style);
    $url = $this->Url();
    $us_login_form = '<a href="#" class="login-title">Log in</a>' .
      '<div class="login-main">' .
      '<form action="' . $url . '" method="post" class="login-form">' .
        '<div class="form-spacing">' .
          '<label id="user-input-label" for="user-input">Username:</label>' .
          '<input id="user-input" type="text" name="user" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label id="password-input-label" for="password-input">Password:' .
          '</label>' .
          '<input id="password-input" type="password" name="password" ' .
            'maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="remember-input">Remember me:</label>' .
          '<input id="remember-input" type="checkbox" name="remember" ' .
            'value="true" checked="checked">' .
        '</div>' .
        '<input class="submit" type="submit" value="submit">' .
      '</form>' .
      '<a href="#" class="login-help">Having trouble logging in?</a>' .
      '<form action="' . $url . '" method="post" class="email-reset">' .
        'Please use the form below to send a new password to your ' .
        'registered email address.<br>' .
        '<div class="form-spacing">' .
          '<label id="user-email-label" for="user-email-input">Username:' .
          '</label>' .
          '<input id="user-email-input" type="text" name="user" ' .
            'maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label id="email-reset-label" for="email-reset">Email:</label>' .
          '<input id="email-reset" type="text" name="email" maxlength="100">' .
        '</div>' .
      '<input class="submit" type="submit" value="submit"></form></div>';

    $mysqli = connect_db();
    $login_form = $mysqli->escape_string($us_login_form);
    $mysqli->close();
    $this->Insert($id, $login_form);
  }

  public function Callback() {
    // mode is used by the Extended module, which also calls this function.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      $id = (int)substr($_POST['id'], 9);
      return ['source' => $this->PlainContent($id), 'editor' => true];
    }
  }

  public function CanAdd($page) {
    // Must have admin access to add the login module.
    if (!$this->user->canEditSite) return false;
    // Also can only have one login module on a page.
    return !$this->AlreadyOnPage('login', $page);
  }

  public function CanEdit($id) {
    // Must have admin access to edit the login module.
    return $this->user->canEditSite;
  }

  public function CanRemove($id) {
    // Must have admin access to remove the login module.
    return $this->user->canEditSite;
  }

  public function Content($id) {
    $message = '';
    if ($this->user->loginFailed) {
      if ($this->user->loginStatus === 'unconfirmed') {
        $message = 'Your account has not yet been confirmed.<br>If you have ' .
          'not received a confirmation email please contact an administrator.';
      }
      else if ($this->user->loginStatus === 'password failed') {
        $message = 'Please check your password and try again.';
      }
      else if ($this->user->loginStatus === 'user not found') {
        $message = 'Please check your username and try again.';
      }
      else if ($this->user->loginStatus === 'code expired') {
        $message = 'The login code has expired and can no longer be used.';
      }
      else if ($this->user->loginStatus === 'code failed') {
        $message = 'The login code provided does not match.';
      }
      else if ($this->user->loginStatus === 'verification not set') {
        $txt_ttl = 0;
        $server = $this->user->config->ServerName();
        $check_verification = $this->Substitute('domaincheck-verification');
        if ($result_list = dns_get_record($server, DNS_TXT)) {
          foreach ($result_list as $result) {
            if ($result['type'] === 'TXT' &&
                strpos($result['txt'], $check_verification) !== false) {
              $txt_ttl = ceil((int)$result['ttl'] / 3600);
              break;
            }
          }
        }
        $message = 'Your email address has not been verified.<br>Please ' .
          'make sure the verification string sent to your email address has ' .
          'been added to your DNS settings and then try logging in again.';
        if ($txt_ttl > 1) {
          $message .= ' (The current settings are cached for ' . $txt_ttl .
            ' hours.)';
        }
        else if ($txt_ttl === 1) {
          $message .= ' (The current settings are cached for 1 hour.)';
        }
      }
    }
    else if ($this->user->emailReset) {
      $mysqli = connect_db();
      $email = $mysqli->escape_string($_POST['email']);
      $query = 'SELECT user FROM users WHERE ' .
        'user = "' . $this->user->name . '" AND email = "' . $email . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows !== 1) {
          $message = 'Email doesn\'t match username.';
        }
        else if ($email !== '') {
          $this->NewPassword($email);
          $message = 'Password changed. Please check your email.';
        }
        else {
          $message = 'No email address set.';
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Login->Content: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    return '<div class="dobrado-editable">' . $this->PlainContent($id) .
      '</div><p class="login-message">' . $message . '</p>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $this->Insert($id, $this->PlainContent($old_id, $old_owner, true));
    $this->CopyStyle($id, $old_owner, $old_id);
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS login (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'content TEXT,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Login->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS login_history (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'content TEXT,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'modified_by VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, box_id, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Login->Install 2: ' . $mysqli->error);
    }
    $mysqli->close();

    // Append dobrado.login.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.login.js', true);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM login WHERE user = "' . $this->owner . '" ' .
        'AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Login->Remove 1: ' . $mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM login WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Login->Remove 2: ' . $mysqli->error);
      }
      $query = 'DELETE FROM login_history WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Login->Remove 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    if ($us_content['data'] === $this->PlainContent($id)) return;

    $time = time();
    $mysqli = connect_db();
    $data = $mysqli->escape_string($us_content['data']);
    $query = 'UPDATE login SET content = "' . $data . '", timestamp = ' . $time.
      ' WHERE user = "' . $this->owner . '" AND box_id = ' . $id;
    if (!$mysqli->query($query)) {
      $this->Log('Login->SetContent 1: ' . $mysqli->error);
    }

    $query = 'INSERT INTO login_history VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $data . '", ' . $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Login->SetContent 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.login.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.login.js', true);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Insert($id, $content) {
    $time = time();
    $mysqli = connect_db();
    $query = 'INSERT INTO login VALUES ' .
      '("' . $this->owner . '", ' . $id . ', "' . $content . '", ' . $time .')';
    if (!$mysqli->query($query)) {
      $this->Log('Login->Insert 1: ' . $mysqli->error);
    }

    $query = 'INSERT INTO login_history VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $content . '", ' . $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Login->Insert 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function NewPassword($email) {
    // Generate a new password.
    $chars = 'bcdfghjklmnpqrstvwxyz1234567890';
    $length = strlen($chars) - 1;
    $new_password = '';
    for ($i = 0; $i < 8; $i++) {
      $new_password .= substr($chars, mt_rand(0, $length), 1);
    }

    $mysqli = connect_db();
    $password_hash =
      $mysqli->escape_string(password_hash($new_password, PASSWORD_DEFAULT));
    $query = 'UPDATE users SET password = "' . $password_hash . '" WHERE ' .
      'user = "' . $this->user->name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Login->NewPassword: ' . $mysqli->error);
    }
    $mysqli->close();

    $server = $this->user->config->ServerName();
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    // Send the user an email.
    $subject = 'Password reset';
    $message = '<html><head><title>' . $subject . '</title></head>' .
      '<body><p>Hello ' . $this->user->name . '</p>' .
      '<p>This is an automated email from <a href="' . $scheme . $server . '">'.
        $server . '</a></p>' .
      '<p>You have requested a new password for your account, ' .
        'which has been reset to: ' . $new_password . '</p>' .
      '<p>Please log in and change your password again, by selecting the ' .
        '\'Preferences\' option in the account menu.</p>' .
      '</body></html>';
    $message = wordwrap($message);
    $sender = $this->Substitute('system-sender', '/!host/', $server);
    $sender_name = $this->Substitute('system-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    dobrado_mail($email, $subject, $message, $headers, '',
                 $sender, $this->user->name, 'login', 'newPasswordEmail');
  }

  private function PlainContent($id, $user = '', $escape = false) {
    if ($user === '') {
      $user = $this->owner;
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT content FROM login WHERE user = "' . $user . '" ' .
      'AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($login = $mysqli_result->fetch_assoc()) {
        $content = $escape ? $mysqli->escape_string($login['content']) :
          $login['content'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Login->PlainContent: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

}
