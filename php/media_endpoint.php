<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function header_value($headers, $name) {
  foreach ($headers as $key => $value) {
    if (strtolower($key) === strtolower($name)) return $value;
  }
  return '';
}

$us_token = '';
$headers = apache_request_headers();
$authorization = header_value($headers, 'Authorization');
if ($authorization !== '') {
  // Remove the prefix 'Bearer ' from the Authorization header.
  $us_token = substr($authorization, 7);
}
else if (isset($_POST['access_token'])) {
  $us_token = urldecode($_POST['access_token']);
}
if ($us_token === '') {
  header('HTTP/1.1 401 Unauthorised');
  exit;
}

include 'functions/db.php';

$me = '';
$mysqli = connect_db();
$token = $mysqli->escape_string($us_token);
$query = 'SELECT me FROM access_tokens WHERE token = "' . $token . '"';
if ($result = $mysqli->query($query)) {
  if ($access_tokens = $result->fetch_assoc()) {
    $me = $access_tokens['me'];
  }
  $result->close();
}
else {
  log_db('media_endpoint.php 1: ' . $mysqli->error);
}
$mysqli->close();

if (!preg_match('/^https?:\/\/' . $_SERVER['SERVER_NAME'] . '/', $me)) {
  log_db('media_endpoint.php 2: Couldn\'t match ' . $_SERVER['SERVER_NAME'] .
         ' in: ' . $me);
  header('HTTP/1.1 403 Forbidden');
  exit;
}

if (!isset($_FILES['file'])) {
  log_db('media_endpoint.php 3: File not found.');
  header('HTTP/1.1 400 Bad Request');
  exit;
}

include 'functions/page_owner.php';
include 'functions/permission.php';

include 'config.php';
include 'module.php';
include 'user.php';

list($page, $owner) = page_owner($me);
$user = new User($owner);
$user->page = $page;

$browser = new Module($user, $owner, 'browser');
if (!$browser->IsInstalled()) {
  header('HTTP/1.1 500 Internal Server Error');
  log_db('media_endpoint.php 4: Browser module is not installed.');
  exit;
}

$result = $browser->Factory('Upload', 'file');
if (isset($result['error'])) {
  log_db('media_endpoint.php 5: ' . $result['error']);
  header('HTTP/1.1 400 Bad Request');
  header('Content-Type: application/json');
  echo json_encode(['error' => 'Bad Request',
                    'error_description' => $result['error']]);
  exit;
}

$location = $user->config->Secure() ? 'https://' : 'http://';
$location .= $user->config->ServerName() . $result;
header('HTTP/1.1 201 Created');
header('Location: ' . $location);
