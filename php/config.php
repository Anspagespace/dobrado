<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Config {

  private $user = '';

  public function __construct() {
    // This only sets a default timezone, as the user hasn't been set yet.
    date_default_timezone_set($this->Get('timezone'));
  }

  public function SetUser($user, $timezone = '') {
    $this->user = $user;
    if ($timezone === '') {
      date_default_timezone_set($this->Get('timezone'));
    }
    else {
      date_default_timezone_set($timezone);
    }
  }

  // Allow tracking code to be added per install, added to every page.
  public function Analytics() {
    return $this->Get('analytics');
  }

  // The frequency with which cron.php is called, in seconds.
  public function Cron() {
    return $this->Get('cron');
  }

  // Set to true to use individual javascript files for each module during
  // development. Otherwise a concatenated version of all the files is used.
  public function DevelopmentMode() {
    return $this->Get('development_mode') === '1' ? true : false;
  }

  // Use fancy urls (ie 'name' instead of 'index.php?page=name')
  public function FancyUrl() {
    return $this->Get('fancy_url') === '1' ? true : false;
  }

  // Set to true to allow special 'guest' accounts to be created when
  // logging in with the username 'guest'.
  public function GuestAllowed() {
    return $this->Get('guest_allowed') === '1' ? true : false;
  }

  // The page a user is shown when they first log in.
  public function LoginPage() {
    return $this->Get('login_page');
  }
  
  // The maximum upload file size in megabytes.
  public function MaxFileSize() {
    // The config value is returned, but there are other factors that affect
    // the maximum file size that aren't checked here, such as php's ini values.
    return $this->Get('max_file_size');
  }

  // The maximum capacity for uploads for the user in megabytes.
  public function MaxUpload() {
    return (int)$this->Get('max_upload');
  }

  // The default permalink page to copy for the user.
  public function PermalinkDefault() {
    return $this->Get('permalink_default');
  }

  // The box_order for the new post module on the permalink page.
  public function PermalinkOrder() {
    return $this->Get('permalink_order');
  }

  // A path to store private files outside the web server. Make sure it's
  // writable by the user that runs the web server though.
  public function PrivatePath() {
    return $this->Get('private_path');
  }

  public function Secure() {
    return $this->Get('secure') === '1' ? true : false;
  }

  public function ServerName() {
    return $this->Get('server_name');
  }

  public function Theme() {
    return $this->Get('theme');
  }

  // Set the page title for the site. (Optionally prefixed by the page name)
  public function Title() {
    return $this->Get('title');
  }

  // Set whether the page name is prefixed into the title of the page.
  public function TitleIncludesPage() {
    return $this->Get('title_includes_page') === '1' ? true : false;
  }

  // Unavailable page name. (see: page.php)
  public function Unavailable() {
    return $this->Get('unavailable');
  }

  // Private functions below here ////////////////////////////////////////////

  private function Get($name) {
    $value = '';
    $mysqli = connect_db();
    $query = 'SELECT '.$name.' FROM config WHERE user = "'.$this->user.'" '.
      'OR user = "" ORDER BY user DESC';
    if ($result = $mysqli->query($query)) {
      while ($config = $result->fetch_assoc()) {
        $value = $config[$name];
        // If a value is set for the user don't use the default.
        if ($value !== NULL && $value !== '') break;
      }
      $result->close();
    }
    else {
      log_db('Config->Get: '.$mysqli->error, $this->user);
    }
    $mysqli->close();
    return $value;
  }

}
