<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/db.php';
include 'functions/new_user.php';

include 'config.php';
include 'module.php';
include 'user.php';

function redirect($label) {
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  $config = new Config();
  $page = $config->FancyUrl() ? '/' : '/index.php?page=';
  $page .= substitute($label);
  // Not using config ServerName here in case CNAME is being used.
  header('Location: ' . $scheme . $_SERVER['SERVER_NAME'] . $page);
}

$username = '';
$confirm_id = '';
$email = '';
$moderator = false;

$mysqli = connect_db();

if (isset($_GET['username'])) {
  $username = $mysqli->escape_string($_GET['username']);
}
if (isset($_GET['confirm_id'])) {
  $confirm_id = $mysqli->escape_string($_GET['confirm_id']);
}
if (isset($_GET['moderator'])) {
  $moderator = $_GET['moderator'] === 'true';
}

$query = 'SELECT email, confirmed, from_guest FROM users WHERE ' .
  'user = "' . $username . '" AND confirm_id = "' . $confirm_id . '"';
if ($result = $mysqli->query($query)) {
  if ($result->num_rows !== 1) {
    $result->close();
    $mysqli->close();
    redirect('confirm-unknown');
    exit;
  }

  if ($users = $result->fetch_assoc()) {
    if ($users['confirmed'] === '1') {
      $result->close();
      $mysqli->close();
      redirect('already-confirmed');
      exit;
    }
    $email = $users['email'];
  }

  $query = 'UPDATE users SET confirmed = 1 WHERE user = "' . $username . '"';
  if (!$mysqli->query($query)) {
    log_db('confirm.php 1: ' . $mysqli->error);
    $result->close();
    $mysqli->close();
    redirect('confirm-failed');
    exit;
  }

  $result->close();
}
else {
  log_db('confirm.php 2: ' . $mysqli->error);
  $mysqli->close();
  redirect('confirm-failed');
  exit;
}

$mysqli->close();

// If new accounts aren't moderated we're done. 
if (!$moderator) {
  redirect('confirm-done');
  exit;
}

// TODO: from_guest can be used to set permission so the user can copy over
//       their old pages if they want to.

$name = '';
$user = new User($username);
$detail = new Module($user, $username, 'detail');
if ($detail->IsInstalled()) {
  $user_detail = $detail->Factory('User', $username);
  $name = $user_detail['first'];
}
if ($name === '') {
  $name = $username;
}
$subject = 'Account confirmed';
$server = $user->config->ServerName();
$scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
  'https://' : 'http://';
$message = '<html><head><title>Account confirmed</title></head>' .
  '<body>Hello ' . $name . '<p>Thanks for waiting! Your account has been ' .
  'confirmed.<br>Please go to <a href="' . $scheme . $server . '">' . $server .
  '</a> to log in, using the username <b>' . $username . '</b> and the ' .
  'password you gave when creating the account.</p>';
$banking = new Module($user, $username, 'banking');
if ($banking->IsInstalled()) {
  $settings = $banking->Factory('Settings', $username);
  $message .= substitute('new-user-info', $user->group, '/!reference/',
                         $settings['reference']);
}
$message .= '</body></html>';
$message = wordwrap($message);
$sender = substitute('new-user-sender', $user->group, '/!host/', $server);
$sender_name = substitute('new-user-sender-name', $user->group);
if ($sender_name === '') {
  $sender_name = $sender;
}
else {
  $sender_name .= ' <' . $sender . '>';
}
$cc = substitute('new-user-sender-cc', $user->group);
$bcc = substitute('new-user-sender-bcc', $user->group);

$headers = "MIME-Version: 1.0\r\n" .
  "Content-type: text/html; charset=utf-8\r\n" .
  'From: ' . $sender_name . "\r\n";
if ($cc !== '') {
  $headers .= 'Cc: ' . $cc . "\r\n";
}
if ($bcc !== '') {
  $headers .= 'Bcc: ' . $bcc . "\r\n";
}
mail($email, $subject, $message, $headers, '-f ' . $sender);
redirect('moderator-done');
